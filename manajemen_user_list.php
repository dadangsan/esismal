<?php
require_once("config/database-connect.php");
?>
<div class="panel" id="formMenu" style="display: none">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Pengguna Baru
        </div>
        <div class="panel-body">
            <form id="userform" method="post">
         <table cellpadding="0" cellspacing="0" border="0" class="tblInput" style="width: 80%">
         <tbody>
            <tr>
                <th>Username</th>
                <th>: <input type="text" name="username" id="username" size="20"></th>
            </tr>
            <tr>
                <th>Nama Lengkap</th>
                <th>: <input type="text" name="nama" id="nama" size="40"></th>
            </tr>
            <tr>
                <th>Propinsi</th>
                <th>: <select name="propinsi" id="propinsi" onchange="getComboKab('kabupaten',this)">
                <option value="">-- Pilih --</option>
                <?php
                $str =mysqli_query($dbconn,"SELECT id_propinsi,nama_propinsi from mst_propinsi");
                while($mn=mysqli_fetch_array($str)){?>
                <option value="<?php echo $mn['id_propinsi'];?>"><?php echo $mn['nama_propinsi'];?></option>
                
                <?php } ?>
                </select></th>
            </tr>
            
             <tr>
                <th>Kabupaten</th>
                <th>: <select name="kabupaten" id="kabupaten" onchange="getComboFaskes('fasyankes',this)">
                <option value="">-- Pilih --</option>
                </select></th>
            </tr>
             
              <tr>
                <th>Fasyankes</th>
                <th>: <select name="fasyankes" id="fasyankes">
                <option value="">-- Pilih --</option>
                </select></th>
            </tr>
              
             <tr>
                <th>Group</th>
                <th>: <select name="usergroup" id="usergroup">
                <option value="">-- Pilih --</option>
                <?php
                $strGroup =mysqli_query($dbconn,"SELECT id_group,`group` from `group1`");
                while($mn=mysqli_fetch_array($strGroup)){?>
                <option value="<?php echo $mn['id_group'];?>"><?php echo $mn['group'];?></option>
                
                <?php } ?>
                </select></th>
            </tr>
            
            <tr>
                <th>Password</th>
                <th>: <input type="password" name="userpassword" id="userpassword" size="20"></th>
            </tr>
             <tr>
                <th>Ulangi Password</th>
                <th>: <input type="password" name="userpassword2" id="userpassword2" size="20"></th>
            </tr>
            <input type="hidden" id="uid" value="">
            <tr>
                <th colspan="2" style="text-align: center">
                    <button type="button" class="btn btn-primary btn-xs" id="eduser" style="display: none" onclick="updateUser()">Edit</button>
                    <button type="button" class="btn btn-primary btn-xs" id="aduser" onclick="addNewUser()">Tambahkan</button>
                   
                </th>
            </tr>
         </tbody>
         </table>
            
            </form>
          
            
        </div>
        <div class="panel-footer">&nbsp;</div>
    </div>
</div>
<table class="tblListData">
    <thead>
        <tr>
            <th colspan="7" style="text-align: left">
                
                <button type="btn" class="btn btn-default btn-xs" onclick="showFormMenu();userFormValidation('#userForm');" id="simpan"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i>
 Tambah User</button>
                
               
  <span><i class="fa fa-search fa-fw"></i></span>
  <input type="text" id="search" placeholder="Pencarian" onkeyup="search(this)">

            </th>
        </tr>
        <tr>
        <th  style="width: 5%">No.</th>
        <th   style="width: 10%">Username</th>
        <th   style="width: 25%">Nama Lengkap</th>
        <th   style="width: 20%">Group</th>
        <th   style="width: 25%">Propinsi</th>
		<th   style="width: 25%">Kabupaten</th>		
        <th   style="width: 15%">Ubah/Hapus</th>
        </tr>
    </thead>
    
    <tbody id="listdata">
    </tbody>
    <tfoot>
        <th colspan="6" style="text-align: left">
            <?php
            $genSTR = "SELECT a.`username`,a.`nama`,a.`propinsi`,b.`id_group`,b.`group`,c.`nama_propinsi` FROM user_account a
INNER JOIN `group1` b ON(a.`id_group`=b.`id_group`)
LEFT JOIN mst_propinsi c ON(a.`propinsi`=c.`id_propinsi`)";
$sql = mysqli_query($dbconn,$genSTR);
$rows = mysqli_num_rows($sql);
$limit='50';
$totalPage = ceil($rows/$limit);
            ?>
            Halaman :
            <select name="page" id="page" onchange="userPaging(this)">
                <?php for($i=0;$i<$totalPage;$i++){?>
                <option value="<?php echo $i;?>"><?php echo $i+1;?></option>
                <?php } ?>
            </select>
            <!--  <ul class="pagination pagination-sm">
                <?php for($i=1;$i<=10;$i++) { ?> <li><a href="#"><?php echo $i; ?></a></li><?php } ?>
</ul>-->
            
        </th>
    </tfoot>
</table>
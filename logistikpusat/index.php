<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- <title>Basic DataGrid - jQuery EasyUI Demo</title> -->
    
    	<!-- <script type="text/javascript" src="logistikpusat/js/jquery-1.8.0.min.js"></script> 
	<script type="text/javascript" src="logistikpusat/js/jquery.easyui.min.js"></script>   
	<link rel="stylesheet" type="text/css" href="logistikpusat/css/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="logistikpusat/css/icon.css"> -->


<script type="text/javascript">
var url;  
function create(){
	$('#dialog-form').dialog('open').dialog('setTitle','Tambah Data');
	$('#form').form('clear');
}
function save(){
	var kode = $("#kode").val();
	var string = $("#form").serialize();
	if(kode.length==0){
		$.messager.show({
			title:'Info',
			msg:'Maaf, kode tidak boleh kosong',
			timeout:2000,
			showType:'slide'
		});
		$("#kode").focus();
		return false();
	}
	
	$.ajax({
		type	: "POST",
		url		: "simpan.php",
		data	: string,
		success	: function(data){
			$.messager.show({
				title:'Info',
				msg:data, //'Password Tidak Boleh Kosong.',
				timeout:2000,
				showType:'slide'
			});
			$('#datagrid-crud').datagrid('reload');
		}
	});
}
function hapus(){  
	var row = $('#datagrid-crud').datagrid('getSelected');  
	if (row){  
		$.messager.confirm('Confirm','Apakah Anda akan menghapus data ini ?',function(r){  
			if (r){  
				$.ajax({
					type	: "POST",
					url		: "hapus.php",
					data	: 'id='+row.kodebarang,
					success	: function(data){
						$.messager.show({
							title:'Info',
							msg:data, //'Password Tidak Boleh Kosong.',
							timeout:2000,
							showType:'slide'
						});
						$('#datagrid-crud').datagrid('reload');
					}
				});  
			}  
		});  
	}  
} 
function update(){
	var row = $('#datagrid-crud').datagrid('getSelected');
	if(row){
		$('#dialog-form').dialog('open').dialog('setTitle','Edit Data');
		$('#form').form('load',row);
	}
}

function fresh(){  
	$('#datagrid-crud').datagrid('reload');
}  
function doSearch(value){  
	$('#datagrid-crud').datagrid('load',{    
        cari: value
    });  
}  
</script>
</head>
<body>
	<!-- <h2>CRUD DataGrid EsayUI</h2> -->
	<div style="margin:10px 0;"></div>
	
	<table id="datagrid-crud" title="Logistik Malaria - Level Pusat" class="easyui-datagrid" style="width:1000px; height: auto;" url="logistikpusat/json.php" toolbar="#tb" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" collapsible="true">
    <thead>
        <tr>
            <th data-options="field:'kodebarang',width:80" sortable="true">Kode Barang</th>
            <th data-options="field:'namabarang',width:200" sortable="true">Nama Barang</th>
            <th data-options="field:'hargabeli',width:80,align:'right'" sortable="true">Harga Beli</th>
            <th data-options="field:'hargajual',width:80,align:'right'" sortable="true">Harga Jual</th>
            <th data-options="field:'stokawal',width:80,align:'center'">Stok</th>
        </tr>
    </thead>
	</table>
    <div id="tb" style="padding:2px;height:30px;">
		<div style="float:left;">
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="create()">Tambah</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="update()">Edit</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="hapus()">Hapus</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="fresh()">Refresh</a>
		</div>
		<div style="float:right;">
        	Pencarian <input id="cari" class="easyui-searchbox" data-options="prompt:'Cari Kode Barang..',searcher:doSearch" style="width:200px"></input> 
		</div>
	</div>
    
<!-- Dialog Form -->
<div id="dialog-form" class="easyui-dialog" style="width:400px; height:300px; padding: 10px 20px" closed="true" buttons="#dialog-buttons">
	<form id="form" method="post" novalidate>
		<div class="form-item">
			
            <tr>
        <th>Tahun *</th>
        <th>:
        <select name="tahun" id="tahun">
            
            <option value="">-- Pilih --</option>
            <?php       
		//for($i=date('Y');$i>date('Y')-2;$i--){ $tahun[$i] = $i; 
		for($i=date('Y');$i>2015;$i--){ 
		//$tahun[$i] = $i; 
		 $sel = ($i == date('Y')) ? 'selected' : '';
		 //$sel = ($i == date('Y')+1) ? '' : '';
		 //echo "<option value=".$i." ".$i.">".date("Y", mktime(0,0,0,0,1,$i))."</option>";			    
		?>
    <OPTION VALUE="<?php echo $i;?>"><?php echo $i;?></OPTION>
    <?php }  ?>	
		
        </select> 
        </th>
    </tr>
            <br/><label for="type">Kode Barang2</label>
			<br />
			<input type="text" name="kodebarang" id="kode" class="easyui-validatebox" required="true" size="20" maxlength="20" />
		</div>
		<div class="form-item">
			<label for="nama">Nama Barang2</label>
			<br />
			<input type="text" name="namabarang" id="nama" class="easyui-validatebox" required="true" size="53" maxlength="50" />
		</div>
		<div class="form-item">
			<label for="hargabeli">Harga Beli2</label>
			<br />
			<input class="easyui-numberbox" name="hargabeli" id="hargabeli" data-options="precision:2,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. '" class="easyui-validatebox" required="true" />
		</div>
        <div class="form-item">
			<label for="hargajual">Harga Jual2</label>
			
			<br />
			<input class="easyui-numberbox" name="hargajual" id="hargajual" data-options="precision:2,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. '" class="easyui-validatebox" required="true" />
		</div>
        <div class="form-item">
			<label for="stok">Stok Awal2</label>
			<br />
			<input class="easyui-numberbox" name="stokawal" id="stok" class="easyui-validatebox" required="true" />
		</div>
	</form>
</div>

<!-- Dialog Button -->
<div id="dialog-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Simpan</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#dialog-form').dialog('close')">Batal</a>
</div>
</body>
</html>
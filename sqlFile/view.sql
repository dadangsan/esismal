DELIMITER $$

USE `esismalv2`$$

DROP VIEW IF EXISTS `view_lap_bulanan_perprop`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_perprop` AS 
SELECT
  `imp_lap_jan_des`.`kdfaskes`    AS `kdfaskes`,
  `imp_lap_jan_des`.`idpropinsi`  AS `idpropinsi`,
  `imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,
  `imp_lap_jan_des`.`tahun`       AS `tahun`,
  `imp_lap_jan_des`.`bulan`       AS `bulan`,
  `imp_lap_jan_des`.`kd_desa_kel` AS `kd_desa_kel`,
  SUM(`imp_lap_jan_des`.`juml_pddk`) AS `jmlpddk`,
  SUM(`imp_lap_jan_des`.`mikroskopis`) AS `mikroskopis`,
  SUM(`imp_lap_jan_des`.`rdt`)    AS `rdt`,
  SUM(`imp_lap_jan_des`.`lainnya`) AS `lainnya`,
  SUM(`imp_lap_jan_des`.`total`)  AS `total`,
  SUM(`imp_lap_jan_des`.`0_11bln_l`) AS `l1`,
  SUM(`imp_lap_jan_des`.`0_11bln_p`) AS `p1`,
  SUM(`imp_lap_jan_des`.`1_4_l`)  AS `l2`,
  SUM(`imp_lap_jan_des`.`1_4_p`)  AS `p2`,
  SUM(`imp_lap_jan_des`.`5_9_l`)  AS `l3`,
  SUM(`imp_lap_jan_des`.`5_9_p`)  AS `p3`,
  SUM(`imp_lap_jan_des`.`10_14_l`) AS `l4`,
  SUM(`imp_lap_jan_des`.`10_14_p`) AS `p4`,
  SUM(`imp_lap_jan_des`.`15_64_l`) AS `l5`,
  SUM(`imp_lap_jan_des`.`15_64_p`) AS `p5`,
  SUM(`imp_lap_jan_des`.`65_l`)   AS `l6`,
  SUM(`imp_lap_jan_des`.`65_p`)   AS `p6`,
  SUM(`imp_lap_jan_des`.`tot_l`)  AS `ltotal`,
  SUM(`imp_lap_jan_des`.`tot_p`)  AS `ptotal`,
  SUM(`imp_lap_jan_des`.`tot_pl`) AS `lptotal`,
  SUM(`imp_lap_jan_des`.`kematian_mal`) AS `kematian`,
  SUM(`imp_lap_jan_des`.`bumil_pos_mal`) AS `bumil`,
  SUM(`imp_lap_jan_des`.`pf`)     AS `pf`,
  SUM(`imp_lap_jan_des`.`pv`)     AS `pv`,
  SUM(`imp_lap_jan_des`.`po`)     AS `po`,
   SUM(`imp_lap_jan_des`.`pm`)     AS `pm`,
  SUM(`imp_lap_jan_des`.`pk`)     AS `pk`,
  SUM(`imp_lap_jan_des`.`mix`)    AS `mix`,
  SUM(`imp_lap_jan_des`.`peng_act`) AS `peng_act`,
  SUM(`imp_lap_jan_des`.`peng_nonact`) AS `peng_nonact`,
  SUM(`imp_lap_jan_des`.`peng_primaquin`) AS `peng_primaquin`,
  SUM(`imp_lap_jan_des`.`kasus_pe`) AS `kasus_pe`,
  SUM(`imp_lap_jan_des`.`indigenus`) AS `indigenus`,
  SUM(`imp_lap_jan_des`.`impor`)  AS `impor`,
  SUM(`imp_lap_jan_des`.`relaps`) AS `relap`,
  SUM(`imp_lap_jan_des`.`induced`) AS `induced`,
  SUM(`imp_lap_jan_des`.`indik_act`) AS `indik_act`,
  SUM(`imp_lap_jan_des`.`indik_primaq`) AS `indik_primaq`,
  SUM(`imp_lap_jan_des`.`indik_kasus_pe`) AS `indik_kasus_pe`,
  SUM(`imp_lap_jan_des`.`api`)    AS `api`
FROM `imp_lap_jan_des`
GROUP BY `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`bulan`,`imp_lap_jan_des`.`idpropinsi`,`imp_lap_jan_des`.`kd_desa_kel`$$

DELIMITER ;



DELIMITER $$

USE `esismalv2`$$

DROP VIEW IF EXISTS `view_lap_bulanan_perkab`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_perkab` AS 
SELECT
  `imp_lap_jan_des`.`kdfaskes`    AS `kdfaskes`,
  `imp_lap_jan_des`.`idpropinsi`  AS `idpropinsi`,
  `imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,
  `imp_lap_jan_des`.`tahun`       AS `tahun`,
  `imp_lap_jan_des`.`bulan`       AS `bulan`,
  `imp_lap_jan_des`.`kd_desa_kel` AS `kd_desa_kel`,
  SUM(`imp_lap_jan_des`.`juml_pddk`) AS `jmlpddk`,
  SUM(`imp_lap_jan_des`.`mikroskopis`) AS `mikroskopis`,
  SUM(`imp_lap_jan_des`.`rdt`)    AS `rdt`,
  SUM(`imp_lap_jan_des`.`lainnya`) AS `lainnya`,
  SUM(`imp_lap_jan_des`.`total`)  AS `total`,
  SUM(`imp_lap_jan_des`.`0_11bln_l`) AS `l1`,
  SUM(`imp_lap_jan_des`.`0_11bln_p`) AS `p1`,
  SUM(`imp_lap_jan_des`.`1_4_l`)  AS `l2`,
  SUM(`imp_lap_jan_des`.`1_4_p`)  AS `p2`,
  SUM(`imp_lap_jan_des`.`5_9_l`)  AS `l3`,
  SUM(`imp_lap_jan_des`.`5_9_p`)  AS `p3`,
  SUM(`imp_lap_jan_des`.`10_14_l`) AS `l4`,
  SUM(`imp_lap_jan_des`.`10_14_p`) AS `p4`,
  SUM(`imp_lap_jan_des`.`15_64_l`) AS `l5`,
  SUM(`imp_lap_jan_des`.`15_64_p`) AS `p5`,
  SUM(`imp_lap_jan_des`.`65_l`)   AS `l6`,
  SUM(`imp_lap_jan_des`.`65_p`)   AS `p6`,
  SUM(`imp_lap_jan_des`.`tot_l`)  AS `ltotal`,
  SUM(`imp_lap_jan_des`.`tot_p`)  AS `ptotal`,
  SUM(`imp_lap_jan_des`.`tot_pl`) AS `lptotal`,
  SUM(`imp_lap_jan_des`.`kematian_mal`) AS `kematian`,
  SUM(`imp_lap_jan_des`.`bumil_pos_mal`) AS `bumil`,
  SUM(`imp_lap_jan_des`.`pf`)     AS `pf`,
  SUM(`imp_lap_jan_des`.`pv`)     AS `pv`,
  SUM(`imp_lap_jan_des`.`po`)     AS `po`,
   SUM(`imp_lap_jan_des`.`pm`)     AS `pm`,
  SUM(`imp_lap_jan_des`.`pk`)     AS `pk`,
  SUM(`imp_lap_jan_des`.`mix`)    AS `mix`,
  SUM(`imp_lap_jan_des`.`peng_act`) AS `peng_act`,
  SUM(`imp_lap_jan_des`.`peng_nonact`) AS `peng_nonact`,
  SUM(`imp_lap_jan_des`.`peng_primaquin`) AS `peng_primaquin`,
  SUM(`imp_lap_jan_des`.`kasus_pe`) AS `kasus_pe`,
  SUM(`imp_lap_jan_des`.`indigenus`) AS `indigenus`,
  SUM(`imp_lap_jan_des`.`impor`)  AS `impor`,
  SUM(`imp_lap_jan_des`.`relaps`) AS `relap`,
  SUM(`imp_lap_jan_des`.`induced`) AS `induced`,
  SUM(`imp_lap_jan_des`.`indik_act`) AS `indik_act`,
  SUM(`imp_lap_jan_des`.`indik_primaq`) AS `indik_primaq`,
  SUM(`imp_lap_jan_des`.`indik_kasus_pe`) AS `indik_kasus_pe`,
  SUM(`imp_lap_jan_des`.`api`)    AS `api`
FROM `imp_lap_jan_des`
GROUP BY `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`bulan`,`imp_lap_jan_des`.`idpropinsi`,`imp_lap_jan_des`.`idkabupaten`,`imp_lap_jan_des`.`kd_desa_kel`$$

DELIMITER ;


DELIMITER $$

USE `esismalv2`$$

DROP VIEW IF EXISTS `view_lap_bulanan_perfaskes`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_perfaskes` AS 
SELECT
  `imp_lap_jan_des`.`kdfaskes`    AS `kdfaskes`,
  `imp_lap_jan_des`.`idpropinsi`  AS `idpropinsi`,
  `imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,
  `imp_lap_jan_des`.`tahun`       AS `tahun`,
  `imp_lap_jan_des`.`bulan`       AS `bulan`,
  `imp_lap_jan_des`.`kd_desa_kel` AS `kd_desa_kel`,
  SUM(`imp_lap_jan_des`.`juml_pddk`) AS `jmlpddk`,
  SUM(`imp_lap_jan_des`.`mikroskopis`) AS `mikroskopis`,
  SUM(`imp_lap_jan_des`.`rdt`)    AS `rdt`,
  SUM(`imp_lap_jan_des`.`lainnya`) AS `lainnya`,
  SUM(`imp_lap_jan_des`.`total`)  AS `total`,
  SUM(`imp_lap_jan_des`.`0_11bln_l`) AS `l1`,
  SUM(`imp_lap_jan_des`.`0_11bln_p`) AS `p1`,
  SUM(`imp_lap_jan_des`.`1_4_l`)  AS `l2`,
  SUM(`imp_lap_jan_des`.`1_4_p`)  AS `p2`,
  SUM(`imp_lap_jan_des`.`5_9_l`)  AS `l3`,
  SUM(`imp_lap_jan_des`.`5_9_p`)  AS `p3`,
  SUM(`imp_lap_jan_des`.`10_14_l`) AS `l4`,
  SUM(`imp_lap_jan_des`.`10_14_p`) AS `p4`,
  SUM(`imp_lap_jan_des`.`15_64_l`) AS `l5`,
  SUM(`imp_lap_jan_des`.`15_64_p`) AS `p5`,
  SUM(`imp_lap_jan_des`.`65_l`)   AS `l6`,
  SUM(`imp_lap_jan_des`.`65_p`)   AS `p6`,
  SUM(`imp_lap_jan_des`.`tot_l`)  AS `ltotal`,
  SUM(`imp_lap_jan_des`.`tot_p`)  AS `ptotal`,
  SUM(`imp_lap_jan_des`.`tot_pl`) AS `lptotal`,
  SUM(`imp_lap_jan_des`.`kematian_mal`) AS `kematian`,
  SUM(`imp_lap_jan_des`.`bumil_pos_mal`) AS `bumil`,
  SUM(`imp_lap_jan_des`.`pf`)     AS `pf`,
  SUM(`imp_lap_jan_des`.`pv`)     AS `pv`,
  SUM(`imp_lap_jan_des`.`po`)     AS `po`,
   SUM(`imp_lap_jan_des`.`pm`)     AS `pm`,
  SUM(`imp_lap_jan_des`.`pk`)     AS `pk`,
  SUM(`imp_lap_jan_des`.`mix`)    AS `mix`,
  SUM(`imp_lap_jan_des`.`peng_act`) AS `peng_act`,
  SUM(`imp_lap_jan_des`.`peng_nonact`) AS `peng_nonact`,
  SUM(`imp_lap_jan_des`.`peng_primaquin`) AS `peng_primaquin`,
  SUM(`imp_lap_jan_des`.`kasus_pe`) AS `kasus_pe`,
  SUM(`imp_lap_jan_des`.`indigenus`) AS `indigenus`,
  SUM(`imp_lap_jan_des`.`impor`)  AS `impor`,
  SUM(`imp_lap_jan_des`.`relaps`) AS `relap`,
  SUM(`imp_lap_jan_des`.`induced`) AS `induced`,
  SUM(`imp_lap_jan_des`.`indik_act`) AS `indik_act`,
  SUM(`imp_lap_jan_des`.`indik_primaq`) AS `indik_primaq`,
  SUM(`imp_lap_jan_des`.`indik_kasus_pe`) AS `indik_kasus_pe`,
  SUM(`imp_lap_jan_des`.`api`)    AS `api`
FROM `imp_lap_jan_des`
GROUP BY `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`bulan`,`imp_lap_jan_des`.`idpropinsi`,`imp_lap_jan_des`.`idkabupaten`,`imp_lap_jan_des`.`kdfaskes`,`imp_lap_jan_des`.`kd_desa_kel`$$

DELIMITER ;


DELIMITER $$

USE `esismalv2`$$

DROP VIEW IF EXISTS `view_lap_tahunan_perfaskes`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_perfaskes` AS 
SELECT
  `imp_lap_jan_des`.`kdfaskes`    AS `kdfaskes`,
  `imp_lap_jan_des`.`idpropinsi`  AS `idpropinsi`,
  `imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,
  `imp_lap_jan_des`.`tahun`       AS `tahun`,
  `imp_lap_jan_des`.`bulan`       AS `bulan`,
  `imp_lap_jan_des`.`kd_desa_kel` AS `kd_desa_kel`,
  SUM(`imp_lap_jan_des`.`juml_pddk`) AS `jmlpddk`,
  SUM(`imp_lap_jan_des`.`mikroskopis`) AS `mikroskopis`,
  SUM(`imp_lap_jan_des`.`rdt`)    AS `rdt`,
  SUM(`imp_lap_jan_des`.`lainnya`) AS `lainnya`,
  SUM(`imp_lap_jan_des`.`total`)  AS `total`,
  SUM(`imp_lap_jan_des`.`0_11bln_l`) AS `l1`,
  SUM(`imp_lap_jan_des`.`0_11bln_p`) AS `p1`,
  SUM(`imp_lap_jan_des`.`1_4_l`)  AS `l2`,
  SUM(`imp_lap_jan_des`.`1_4_p`)  AS `p2`,
  SUM(`imp_lap_jan_des`.`5_9_l`)  AS `l3`,
  SUM(`imp_lap_jan_des`.`5_9_p`)  AS `p3`,
  SUM(`imp_lap_jan_des`.`10_14_l`) AS `l4`,
  SUM(`imp_lap_jan_des`.`10_14_p`) AS `p4`,
  SUM(`imp_lap_jan_des`.`15_64_l`) AS `l5`,
  SUM(`imp_lap_jan_des`.`15_64_p`) AS `p5`,
  SUM(`imp_lap_jan_des`.`65_l`)   AS `l6`,
  SUM(`imp_lap_jan_des`.`65_p`)   AS `p6`,
  SUM(`imp_lap_jan_des`.`tot_l`)  AS `ltotal`,
  SUM(`imp_lap_jan_des`.`tot_p`)  AS `ptotal`,
  SUM(`imp_lap_jan_des`.`tot_pl`) AS `lptotal`,
  SUM(`imp_lap_jan_des`.`kematian_mal`) AS `kematian`,
  SUM(`imp_lap_jan_des`.`bumil_pos_mal`) AS `bumil`,
  SUM(`imp_lap_jan_des`.`pf`)     AS `pf`,
  SUM(`imp_lap_jan_des`.`pv`)     AS `pv`,
  SUM(`imp_lap_jan_des`.`po`)     AS `po`,
   SUM(`imp_lap_jan_des`.`pm`)     AS `pm`,
  SUM(`imp_lap_jan_des`.`pk`)     AS `pk`,
  SUM(`imp_lap_jan_des`.`mix`)    AS `mix`,
  SUM(`imp_lap_jan_des`.`peng_act`) AS `peng_act`,
  SUM(`imp_lap_jan_des`.`peng_nonact`) AS `peng_nonact`,
  SUM(`imp_lap_jan_des`.`peng_primaquin`) AS `peng_primaquin`,
  SUM(`imp_lap_jan_des`.`kasus_pe`) AS `kasus_pe`,
  SUM(`imp_lap_jan_des`.`indigenus`) AS `indigenus`,
  SUM(`imp_lap_jan_des`.`impor`)  AS `impor`,
  SUM(`imp_lap_jan_des`.`relaps`) AS `relap`,
  SUM(`imp_lap_jan_des`.`induced`) AS `induced`,
  SUM(`imp_lap_jan_des`.`indik_act`) AS `indik_act`,
  SUM(`imp_lap_jan_des`.`indik_primaq`) AS `indik_primaq`,
  SUM(`imp_lap_jan_des`.`indik_kasus_pe`) AS `indik_kasus_pe`,
  SUM(`imp_lap_jan_des`.`api`)    AS `api`
FROM `imp_lap_jan_des`
GROUP BY `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`idpropinsi`,`imp_lap_jan_des`.`idkabupaten`,`imp_lap_jan_des`.`kdfaskes`,`imp_lap_jan_des`.`kd_desa_kel`$$

DELIMITER ;

DELIMITER $$

USE `esismalv2`$$

DROP VIEW IF EXISTS `view_lap_tahunan_perkab`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_perkab` AS 
SELECT
  `imp_lap_jan_des`.`kdfaskes`    AS `kdfaskes`,
  `imp_lap_jan_des`.`idpropinsi`  AS `idpropinsi`,
  `imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,
  `imp_lap_jan_des`.`tahun`       AS `tahun`,
  `imp_lap_jan_des`.`bulan`       AS `bulan`,
  `imp_lap_jan_des`.`kd_desa_kel` AS `kd_desa_kel`,
  SUM(`imp_lap_jan_des`.`juml_pddk`) AS `jmlpddk`,
  SUM(`imp_lap_jan_des`.`mikroskopis`) AS `mikroskopis`,
  SUM(`imp_lap_jan_des`.`rdt`)    AS `rdt`,
  SUM(`imp_lap_jan_des`.`lainnya`) AS `lainnya`,
  SUM(`imp_lap_jan_des`.`total`)  AS `total`,
  SUM(`imp_lap_jan_des`.`0_11bln_l`) AS `l1`,
  SUM(`imp_lap_jan_des`.`0_11bln_p`) AS `p1`,
  SUM(`imp_lap_jan_des`.`1_4_l`)  AS `l2`,
  SUM(`imp_lap_jan_des`.`1_4_p`)  AS `p2`,
  SUM(`imp_lap_jan_des`.`5_9_l`)  AS `l3`,
  SUM(`imp_lap_jan_des`.`5_9_p`)  AS `p3`,
  SUM(`imp_lap_jan_des`.`10_14_l`) AS `l4`,
  SUM(`imp_lap_jan_des`.`10_14_p`) AS `p4`,
  SUM(`imp_lap_jan_des`.`15_64_l`) AS `l5`,
  SUM(`imp_lap_jan_des`.`15_64_p`) AS `p5`,
  SUM(`imp_lap_jan_des`.`65_l`)   AS `l6`,
  SUM(`imp_lap_jan_des`.`65_p`)   AS `p6`,
  SUM(`imp_lap_jan_des`.`tot_l`)  AS `ltotal`,
  SUM(`imp_lap_jan_des`.`tot_p`)  AS `ptotal`,
  SUM(`imp_lap_jan_des`.`tot_pl`) AS `lptotal`,
  SUM(`imp_lap_jan_des`.`kematian_mal`) AS `kematian`,
  SUM(`imp_lap_jan_des`.`bumil_pos_mal`) AS `bumil`,
  SUM(`imp_lap_jan_des`.`pf`)     AS `pf`,
  SUM(`imp_lap_jan_des`.`pv`)     AS `pv`,
  SUM(`imp_lap_jan_des`.`po`)     AS `po`,
   SUM(`imp_lap_jan_des`.`pm`)     AS `pm`,
  SUM(`imp_lap_jan_des`.`pk`)     AS `pk`,
  SUM(`imp_lap_jan_des`.`mix`)    AS `mix`,
  SUM(`imp_lap_jan_des`.`peng_act`) AS `peng_act`,
  SUM(`imp_lap_jan_des`.`peng_nonact`) AS `peng_nonact`,
  SUM(`imp_lap_jan_des`.`peng_primaquin`) AS `peng_primaquin`,
  SUM(`imp_lap_jan_des`.`kasus_pe`) AS `kasus_pe`,
  SUM(`imp_lap_jan_des`.`indigenus`) AS `indigenus`,
  SUM(`imp_lap_jan_des`.`impor`)  AS `impor`,
  SUM(`imp_lap_jan_des`.`relaps`) AS `relap`,
  SUM(`imp_lap_jan_des`.`induced`) AS `induced`,
  SUM(`imp_lap_jan_des`.`indik_act`) AS `indik_act`,
  SUM(`imp_lap_jan_des`.`indik_primaq`) AS `indik_primaq`,
  SUM(`imp_lap_jan_des`.`indik_kasus_pe`) AS `indik_kasus_pe`,
  SUM(`imp_lap_jan_des`.`api`)    AS `api`
FROM `imp_lap_jan_des`
GROUP BY `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`idpropinsi`,`imp_lap_jan_des`.`idkabupaten`,`imp_lap_jan_des`.`kd_desa_kel`$$

DELIMITER ;

DELIMITER $$

USE `esismalv2`$$

DROP VIEW IF EXISTS `view_lap_tahunan_perprop`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_perprop` AS 
SELECT
  `imp_lap_jan_des`.`kdfaskes`    AS `kdfaskes`,
  `imp_lap_jan_des`.`idpropinsi`  AS `idpropinsi`,
  `imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,
  `imp_lap_jan_des`.`tahun`       AS `tahun`,
  `imp_lap_jan_des`.`bulan`       AS `bulan`,
  `imp_lap_jan_des`.`kd_desa_kel` AS `kd_desa_kel`,
  SUM(`imp_lap_jan_des`.`juml_pddk`) AS `jmlpddk`,
  SUM(`imp_lap_jan_des`.`mikroskopis`) AS `mikroskopis`,
  SUM(`imp_lap_jan_des`.`rdt`)    AS `rdt`,
  SUM(`imp_lap_jan_des`.`lainnya`) AS `lainnya`,
  SUM(`imp_lap_jan_des`.`total`)  AS `total`,
  SUM(`imp_lap_jan_des`.`0_11bln_l`) AS `l1`,
  SUM(`imp_lap_jan_des`.`0_11bln_p`) AS `p1`,
  SUM(`imp_lap_jan_des`.`1_4_l`)  AS `l2`,
  SUM(`imp_lap_jan_des`.`1_4_p`)  AS `p2`,
  SUM(`imp_lap_jan_des`.`5_9_l`)  AS `l3`,
  SUM(`imp_lap_jan_des`.`5_9_p`)  AS `p3`,
  SUM(`imp_lap_jan_des`.`10_14_l`) AS `l4`,
  SUM(`imp_lap_jan_des`.`10_14_p`) AS `p4`,
  SUM(`imp_lap_jan_des`.`15_64_l`) AS `l5`,
  SUM(`imp_lap_jan_des`.`15_64_p`) AS `p5`,
  SUM(`imp_lap_jan_des`.`65_l`)   AS `l6`,
  SUM(`imp_lap_jan_des`.`65_p`)   AS `p6`,
  SUM(`imp_lap_jan_des`.`tot_l`)  AS `ltotal`,
  SUM(`imp_lap_jan_des`.`tot_p`)  AS `ptotal`,
  SUM(`imp_lap_jan_des`.`tot_pl`) AS `lptotal`,
  SUM(`imp_lap_jan_des`.`kematian_mal`) AS `kematian`,
  SUM(`imp_lap_jan_des`.`bumil_pos_mal`) AS `bumil`,
  SUM(`imp_lap_jan_des`.`pf`)     AS `pf`,
  SUM(`imp_lap_jan_des`.`pv`)     AS `pv`,
  SUM(`imp_lap_jan_des`.`po`)     AS `po`,
   SUM(`imp_lap_jan_des`.`pm`)     AS `pm`,
  SUM(`imp_lap_jan_des`.`pk`)     AS `pk`,
  SUM(`imp_lap_jan_des`.`mix`)    AS `mix`,
  SUM(`imp_lap_jan_des`.`peng_act`) AS `peng_act`,
  SUM(`imp_lap_jan_des`.`peng_nonact`) AS `peng_nonact`,
  SUM(`imp_lap_jan_des`.`peng_primaquin`) AS `peng_primaquin`,
  SUM(`imp_lap_jan_des`.`kasus_pe`) AS `kasus_pe`,
  SUM(`imp_lap_jan_des`.`indigenus`) AS `indigenus`,
  SUM(`imp_lap_jan_des`.`impor`)  AS `impor`,
  SUM(`imp_lap_jan_des`.`relaps`) AS `relap`,
  SUM(`imp_lap_jan_des`.`induced`) AS `induced`,
  SUM(`imp_lap_jan_des`.`indik_act`) AS `indik_act`,
  SUM(`imp_lap_jan_des`.`indik_primaq`) AS `indik_primaq`,
  SUM(`imp_lap_jan_des`.`indik_kasus_pe`) AS `indik_kasus_pe`,
  SUM(`imp_lap_jan_des`.`api`)    AS `api`
FROM `imp_lap_jan_des`
GROUP BY `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`idpropinsi`,`imp_lap_jan_des`.`kd_desa_kel`$$

DELIMITER ;


DELIMITER $$

USE `esismalv2`$$

DROP VIEW IF EXISTS `view_rekap_header_bulanan`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rekap_header_bulanan` AS 
SELECT
  `imp_header_jan_des`.`kdfaskes`    AS `kdfaskes`,
  `imp_header_jan_des`.`idpropinsi`  AS `idpropinsi`,
  `imp_header_jan_des`.`idkabupaten` AS `idkabupaten`,
  `imp_header_jan_des`.`tahun`       AS `tahun`,
  `imp_header_jan_des`.`bulan`       AS `bulan`,
  SUM(`imp_header_jan_des`.`jum_suspek`) AS `suspek`,
  SUM(`imp_header_jan_des`.`tot_dar_pos`) AS `darpositif`,
  SUM(`imp_header_jan_des`.`tot_dar_neg`) AS `darnegatif`,
  SUM(`imp_header_jan_des`.`tot_dar`) AS `darah`,
  SUM(`imp_header_jan_des`.`tot_pos_pcd`) AS `pos_pcd`,
  SUM(`imp_header_jan_des`.`tot_neg_pcd`) AS `neg_pcd`,
  SUM(`imp_header_jan_des`.`tot_pcd`) AS `tot_pcd`
FROM `imp_header_jan_des`
GROUP BY `imp_header_jan_des`.`kdfaskes`,`imp_header_jan_des`.`idpropinsi`,`imp_header_jan_des`.`idkabupaten`,`imp_header_jan_des`.`tahun`,`imp_header_jan_des`.`bulan`$$

DELIMITER ;

DELIMITER $$

USE `esismalv2`$$

DROP VIEW IF EXISTS `view_rekap_header_tahunan`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rekap_header_tahunan` AS 
SELECT
  `imp_header_jan_des`.`kdfaskes`    AS `kdfaskes`,
  `imp_header_jan_des`.`idpropinsi`  AS `idpropinsi`,
  `imp_header_jan_des`.`idkabupaten` AS `idkabupaten`,
  `imp_header_jan_des`.`tahun`       AS `tahun`,
  SUM(`imp_header_jan_des`.`jum_suspek`) AS `suspek`,
  SUM(`imp_header_jan_des`.`tot_dar_pos`) AS `darpositif`,
  SUM(`imp_header_jan_des`.`tot_dar_neg`) AS `darnegatif`,
  SUM(`imp_header_jan_des`.`tot_dar`) AS `darah`,
  SUM(`imp_header_jan_des`.`tot_pos_pcd`) AS `pos_pcd`,
  SUM(`imp_header_jan_des`.`tot_neg_pcd`) AS `neg_pcd`,
  SUM(`imp_header_jan_des`.`tot_pcd`) AS `tot_pcd`
FROM `imp_header_jan_des`
GROUP BY `imp_header_jan_des`.`kdfaskes`,`imp_header_jan_des`.`idpropinsi`,`imp_header_jan_des`.`idkabupaten`,`imp_header_jan_des`.`tahun`$$

DELIMITER ;
<div class="panel">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <span class="fa fa-cogs fa-fw"></span>&nbsp;Pengaturan
        </div>

        <div class="panel-body" style="height: 450px;">
            <!--- content here -->
           <div class="col-lg-12 text-center" style="padding:30px" id="blokmenu">
            
            <div class="col-lg-3" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="p_menu" onclick="configMenu()">

                <i class="fa fa-wrench fa-4x" aria-hidden="true"></i><br>

                <h5>Pengaturan Menu</h5>
            </div>
            </div>
            

            <div class="col-lg-3" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="p_user"  onclick="configUser()">

                <i class="fa fa-user fa-4x" aria-hidden="true"></i><br>

                <h5>Pengaturan User</h5>
            </div>
            </div>
            
            
            

             <div class="col-lg-3" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="p_group"   onclick="configGroup()">
                <i class="fa fa-users fa-4x" aria-hidden="true"></i><br>
               <h5> Pengaturan Group</h5>

            </div>
            </div>
             
             

             
              <div class="col-lg-3" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="p_akses" onclick="configAkses()">
                <i class="fa fa-sitemap fa-4x" aria-hidden="true"></i><br>

                <h5>Pengaturan Akses</h5>
            </div>
            </div>
              
            
           </div>
            
            
            <div class="col-lg-12" id="divConfig" style="display: none">
              
              <div class="col-lg-12">
                
                 <div class="col-lg-3" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="sp_menu" onclick="configMenu()">
                <i class="fa fa-wrench fa-fw" aria-hidden="true"></i>
                Pengaturan Menu
            </div>
            </div>
                 
                 
                 
            <div class="col-lg-3" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="sp_user" onclick="configUser()">
                <i class="fa fa-user fa-fw" aria-hidden="true"></i>
                Pengaturan User
            </div>
            </div> 
                  
                  
                  <div class="col-lg-3" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="sp_group" onclick="configGroup()">
                <i class="fa fa-users fa-fw" aria-hidden="true"></i>
                Pengaturan Group
            </div>
            </div>
                
                
                <div class="col-lg-3" style="padding:4px">
            <div class="col-xs-12 btn btn-default" id="sp_akses" onclick="configAkses()">
                <i class="fa fa-sitemap fa-fw" aria-hidden="true"></i>
                Pengaturan Akses
            </div>
            </div>  
                 
              </div>
              
              
              <div class="col-lg-12" id="divData" style="overflow-y: scroll; overflow-x: scroll; height: 350px">
			    
              </div>  
               
                
            </div>

        </div>
        
        <div class="panel-footer">&nbsp;</div>
    </div>
</div>
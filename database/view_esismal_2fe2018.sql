/*
SQLyog Ultimate v10.42 
MySQL - 5.1.33-community : Database - esismalv2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`esismalhp` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `esismalhp`;

/*Table structure for table `view_juml_penduduk_faskes` */

DROP TABLE IF EXISTS `view_juml_penduduk_faskes`;

/*!50001 DROP VIEW IF EXISTS `view_juml_penduduk_faskes` */;
/*!50001 DROP TABLE IF EXISTS `view_juml_penduduk_faskes` */;

/*!50001 CREATE TABLE  `view_juml_penduduk_faskes`(
 `tahun` varchar(4) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `kdfaskes` varchar(20) ,
 `juml_pddk` varchar(11) 
)*/;

/*Table structure for table `view_juml_penduduk_kabupaten` */

DROP TABLE IF EXISTS `view_juml_penduduk_kabupaten`;

/*!50001 DROP VIEW IF EXISTS `view_juml_penduduk_kabupaten` */;
/*!50001 DROP TABLE IF EXISTS `view_juml_penduduk_kabupaten` */;

/*!50001 CREATE TABLE  `view_juml_penduduk_kabupaten`(
 `tahun` varchar(4) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `juml_pddk` double 
)*/;

/*Table structure for table `view_juml_penduduk_propinsi` */

DROP TABLE IF EXISTS `view_juml_penduduk_propinsi`;

/*!50001 DROP VIEW IF EXISTS `view_juml_penduduk_propinsi` */;
/*!50001 DROP TABLE IF EXISTS `view_juml_penduduk_propinsi` */;

/*!50001 CREATE TABLE  `view_juml_penduduk_propinsi`(
 `tahun` varchar(4) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `juml_pddk` double 
)*/;

/*Table structure for table `view_lap_bulanan_nasional` */

DROP TABLE IF EXISTS `view_lap_bulanan_nasional`;

/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_nasional` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_nasional` */;

/*!50001 CREATE TABLE  `view_lap_bulanan_nasional`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `wilayah` varchar(50) ,
 `propinsi` varchar(50) ,
 `jmlpddk` double ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_bulanan_nasional_total` */

DROP TABLE IF EXISTS `view_lap_bulanan_nasional_total`;

/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_nasional_total` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_nasional_total` */;

/*!50001 CREATE TABLE  `view_lap_bulanan_nasional_total`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `area` varchar(64) ,
 `fasyankes` varchar(65) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_bulanan_perfaskes` */

DROP TABLE IF EXISTS `view_lap_bulanan_perfaskes`;

/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perfaskes` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perfaskes` */;

/*!50001 CREATE TABLE  `view_lap_bulanan_perfaskes`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `kd_desa_kel` varchar(30) ,
 `area` varchar(113) ,
 `propinsi` varchar(50) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_bulanan_perkab` */

DROP TABLE IF EXISTS `view_lap_bulanan_perkab`;

/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perkab` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perkab` */;

/*!50001 CREATE TABLE  `view_lap_bulanan_perkab`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `wilayah` varchar(100) ,
 `fasyankes` varchar(100) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_bulanan_perkab_total` */

DROP TABLE IF EXISTS `view_lap_bulanan_perkab_total`;

/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perkab_total` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perkab_total` */;

/*!50001 CREATE TABLE  `view_lap_bulanan_perkab_total`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `area` varchar(65) ,
 `fasyankes` varchar(65) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_bulanan_perprop` */

DROP TABLE IF EXISTS `view_lap_bulanan_perprop`;

/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perprop` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perprop` */;

/*!50001 CREATE TABLE  `view_lap_bulanan_perprop`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `wilayah` varchar(50) ,
 `kabupaten` varchar(50) ,
 `jmlpddk` double ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_bulanan_perprop_total` */

DROP TABLE IF EXISTS `view_lap_bulanan_perprop_total`;

/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perprop_total` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perprop_total` */;

/*!50001 CREATE TABLE  `view_lap_bulanan_perprop_total`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `area` varchar(61) ,
 `fasyankes` varchar(61) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_tahunan_nasional` */

DROP TABLE IF EXISTS `view_lap_tahunan_nasional`;

/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_nasional` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_nasional` */;

/*!50001 CREATE TABLE  `view_lap_tahunan_nasional`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `wilayah` varchar(50) ,
 `propinsi` varchar(50) ,
 `jmlpddk` double ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_tahunan_nasional_total` */

DROP TABLE IF EXISTS `view_lap_tahunan_nasional_total`;

/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_nasional_total` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_nasional_total` */;

/*!50001 CREATE TABLE  `view_lap_tahunan_nasional_total`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `area` varchar(65) ,
 `fasyankes` varchar(65) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_tahunan_perfaskes` */

DROP TABLE IF EXISTS `view_lap_tahunan_perfaskes`;

/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perfaskes` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perfaskes` */;

/*!50001 CREATE TABLE  `view_lap_tahunan_perfaskes`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `kd_desa_kel` varchar(30) ,
 `area` varchar(113) ,
 `propinsi` varchar(50) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_tahunan_perkab` */

DROP TABLE IF EXISTS `view_lap_tahunan_perkab`;

/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perkab` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perkab` */;

/*!50001 CREATE TABLE  `view_lap_tahunan_perkab`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `wilayah` varchar(100) ,
 `fasyankes` varchar(100) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_tahunan_perkab_total` */

DROP TABLE IF EXISTS `view_lap_tahunan_perkab_total`;

/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perkab_total` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perkab_total` */;

/*!50001 CREATE TABLE  `view_lap_tahunan_perkab_total`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `area` varchar(65) ,
 `fasyankes` varchar(65) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_tahunan_perprop` */

DROP TABLE IF EXISTS `view_lap_tahunan_perprop`;

/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perprop` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perprop` */;

/*!50001 CREATE TABLE  `view_lap_tahunan_perprop`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `wilayah` varchar(50) ,
 `kabupaten` varchar(50) ,
 `jmlpddk` double ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_lap_tahunan_perprop_total` */

DROP TABLE IF EXISTS `view_lap_tahunan_perprop_total`;

/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perprop_total` */;
/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perprop_total` */;

/*!50001 CREATE TABLE  `view_lap_tahunan_perprop_total`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `area` varchar(61) ,
 `fasyankes` varchar(61) ,
 `jmlpddk` varchar(11) ,
 `mikroskopis` double ,
 `rdt` double ,
 `lainnya` double ,
 `total` double ,
 `l1` double ,
 `p1` double ,
 `l2` double ,
 `p2` double ,
 `l3` double ,
 `p3` double ,
 `l4` double ,
 `p4` double ,
 `l5` double ,
 `p5` double ,
 `l6` double ,
 `p6` double ,
 `ltotal` double ,
 `ptotal` double ,
 `lptotal` double ,
 `kematian` double ,
 `bumil` double ,
 `pf` double ,
 `pv` double ,
 `po` double ,
 `pm` double ,
 `pk` double ,
 `mix` double ,
 `peng_act` double ,
 `peng_nonact` double ,
 `peng_primaquin` double ,
 `kasus_pe` double ,
 `indigenus` double ,
 `impor` double ,
 `relap` double ,
 `induced` double ,
 `indik_act` double(23,2) ,
 `indik_primaq` double(23,2) ,
 `indik_kasus_pe` double(23,2) ,
 `api` double(23,2) 
)*/;

/*Table structure for table `view_rekap_header_bulanan` */

DROP TABLE IF EXISTS `view_rekap_header_bulanan`;

/*!50001 DROP VIEW IF EXISTS `view_rekap_header_bulanan` */;
/*!50001 DROP TABLE IF EXISTS `view_rekap_header_bulanan` */;

/*!50001 CREATE TABLE  `view_rekap_header_bulanan`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `bulan` varchar(15) ,
 `suspek` double ,
 `darpositif` double ,
 `darnegatif` double ,
 `darah` double ,
 `pos_pcd` double ,
 `neg_pcd` double ,
 `tot_pcd` double ,
 `bumil_scmalaria` double ,
 `mtbs` double ,
 `kelambu_bumil` double ,
 `kelambu_bayi` double ,
 `kelambu_masal` double ,
 `kelambu_lainnya` double ,
 `irs` double ,
 `fokus_aktif` double 
)*/;

/*Table structure for table `view_rekap_header_tahunan` */

DROP TABLE IF EXISTS `view_rekap_header_tahunan`;

/*!50001 DROP VIEW IF EXISTS `view_rekap_header_tahunan` */;
/*!50001 DROP TABLE IF EXISTS `view_rekap_header_tahunan` */;

/*!50001 CREATE TABLE  `view_rekap_header_tahunan`(
 `kdfaskes` varchar(20) ,
 `idpropinsi` int(2) ,
 `idkabupaten` int(4) ,
 `tahun` varchar(4) ,
 `suspek` double ,
 `darpositif` double ,
 `darnegatif` double ,
 `darah` double ,
 `pos_pcd` double ,
 `neg_pcd` double ,
 `tot_pcd` double 
)*/;

/*View structure for view view_juml_penduduk_faskes */

/*!50001 DROP TABLE IF EXISTS `view_juml_penduduk_faskes` */;
/*!50001 DROP VIEW IF EXISTS `view_juml_penduduk_faskes` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_juml_penduduk_faskes` AS select `imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`juml_pddk` AS `juml_pddk` from `imp_lap_jan_des` where (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL') group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`kdfaskes` */;

/*View structure for view view_juml_penduduk_kabupaten */

/*!50001 DROP TABLE IF EXISTS `view_juml_penduduk_kabupaten` */;
/*!50001 DROP VIEW IF EXISTS `view_juml_penduduk_kabupaten` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_juml_penduduk_kabupaten` AS select `view_juml_penduduk_faskes`.`tahun` AS `tahun`,`view_juml_penduduk_faskes`.`idpropinsi` AS `idpropinsi`,`view_juml_penduduk_faskes`.`idkabupaten` AS `idkabupaten`,sum(`view_juml_penduduk_faskes`.`juml_pddk`) AS `juml_pddk` from `view_juml_penduduk_faskes` group by `view_juml_penduduk_faskes`.`tahun`,`view_juml_penduduk_faskes`.`idkabupaten` */;

/*View structure for view view_juml_penduduk_propinsi */

/*!50001 DROP TABLE IF EXISTS `view_juml_penduduk_propinsi` */;
/*!50001 DROP VIEW IF EXISTS `view_juml_penduduk_propinsi` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_juml_penduduk_propinsi` AS select `view_juml_penduduk_faskes`.`tahun` AS `tahun`,`view_juml_penduduk_faskes`.`idpropinsi` AS `idpropinsi`,`view_juml_penduduk_faskes`.`idkabupaten` AS `idkabupaten`,sum(`view_juml_penduduk_faskes`.`juml_pddk`) AS `juml_pddk` from `view_juml_penduduk_faskes` group by `view_juml_penduduk_faskes`.`tahun`,`view_juml_penduduk_faskes`.`idpropinsi` */;

/*View structure for view view_lap_bulanan_nasional */

/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_nasional` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_nasional` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_nasional` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,`mst_propinsi`.`nama_propinsi` AS `wilayah`,`mst_propinsi`.`nama_propinsi` AS `propinsi`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from ((`imp_lap_jan_des` join `mst_propinsi` on(((`imp_lap_jan_des`.`idpropinsi` = `mst_propinsi`.`id_propinsi`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_propinsi` `jpp` on((`jpp`.`idpropinsi` = `imp_lap_jan_des`.`idpropinsi`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`bulan`,`mst_propinsi`.`nama_propinsi` */;

/*View structure for view view_lap_bulanan_nasional_total */

/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_nasional_total` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_nasional_total` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_nasional_total` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,concat('TOTAL Nasional',`mst_propinsi`.`nama_propinsi`) AS `area`,concat('TOTAL Nasional ',`mst_propinsi`.`nama_propinsi`) AS `fasyankes`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from (((`imp_lap_jan_des` join `mst_fasyankes` on(((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_faskes` `jpp` on((`jpp`.`kdfaskes` = `imp_lap_jan_des`.`kdfaskes`))) join `mst_propinsi` on((`imp_lap_jan_des`.`idpropinsi` = `mst_propinsi`.`id_propinsi`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`bulan` */;

/*View structure for view view_lap_bulanan_perfaskes */

/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perfaskes` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perfaskes` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_perfaskes` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,`imp_lap_jan_des`.`kd_desa_kel` AS `kd_desa_kel`,if(isnull(`mst_kelurahan`.`nama_kelurahan`),if((`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL'),ucase(concat('<b>TOTAL ',`mst_fasyankes`.`nama_fasyankes`,'</b>')),`imp_lap_jan_des`.`kd_desa_kel`),`mst_kelurahan`.`nama_kelurahan`) AS `area`,`mst_propinsi`.`nama_propinsi` AS `propinsi`,`imp_lap_jan_des`.`juml_pddk` AS `jmlpddk`,sum(`imp_lap_jan_des`.`mikroskopis`) AS `mikroskopis`,sum(`imp_lap_jan_des`.`rdt`) AS `rdt`,sum(`imp_lap_jan_des`.`lainnya`) AS `lainnya`,sum(`imp_lap_jan_des`.`total`) AS `total`,sum(`imp_lap_jan_des`.`0_11bln_l`) AS `l1`,sum(`imp_lap_jan_des`.`0_11bln_p`) AS `p1`,sum(`imp_lap_jan_des`.`1_4_l`) AS `l2`,sum(`imp_lap_jan_des`.`1_4_p`) AS `p2`,sum(`imp_lap_jan_des`.`5_9_l`) AS `l3`,sum(`imp_lap_jan_des`.`5_9_p`) AS `p3`,sum(`imp_lap_jan_des`.`10_14_l`) AS `l4`,sum(`imp_lap_jan_des`.`10_14_p`) AS `p4`,sum(`imp_lap_jan_des`.`15_64_l`) AS `l5`,sum(`imp_lap_jan_des`.`15_64_p`) AS `p5`,sum(`imp_lap_jan_des`.`65_l`) AS `l6`,sum(`imp_lap_jan_des`.`65_p`) AS `p6`,sum(`imp_lap_jan_des`.`tot_l`) AS `ltotal`,sum(`imp_lap_jan_des`.`tot_p`) AS `ptotal`,sum(`imp_lap_jan_des`.`tot_pl`) AS `lptotal`,sum(`imp_lap_jan_des`.`kematian_mal`) AS `kematian`,sum(`imp_lap_jan_des`.`bumil_pos_mal`) AS `bumil`,sum(`imp_lap_jan_des`.`pf`) AS `pf`,sum(`imp_lap_jan_des`.`pv`) AS `pv`,sum(`imp_lap_jan_des`.`po`) AS `po`,sum(`imp_lap_jan_des`.`pm`) AS `pm`,sum(`imp_lap_jan_des`.`pk`) AS `pk`,sum(`imp_lap_jan_des`.`mix`) AS `mix`,sum(`imp_lap_jan_des`.`peng_act`) AS `peng_act`,sum(`imp_lap_jan_des`.`peng_nonact`) AS `peng_nonact`,sum(`imp_lap_jan_des`.`peng_primaquin`) AS `peng_primaquin`,sum(`imp_lap_jan_des`.`kasus_pe`) AS `kasus_pe`,sum(`imp_lap_jan_des`.`indigenus`) AS `indigenus`,sum(`imp_lap_jan_des`.`impor`) AS `impor`,sum(`imp_lap_jan_des`.`relaps`) AS `relap`,sum(`imp_lap_jan_des`.`induced`) AS `induced`,round(((sum(`imp_lap_jan_des`.`peng_act`) / sum(`imp_lap_jan_des`.`tot_pl`)) * 100),2) AS `indik_act`,round(((sum(`imp_lap_jan_des`.`peng_primaquin`) / ((sum(`imp_lap_jan_des`.`pv`) + sum(`imp_lap_jan_des`.`po`)) + sum(`imp_lap_jan_des`.`mix`))) * 100),2) AS `indik_primaq`,round(((sum(`imp_lap_jan_des`.`kasus_pe`) / sum(`imp_lap_jan_des`.`tot_pl`)) * 100),2) AS `indik_kasus_pe`,round(((sum(`imp_lap_jan_des`.`tot_pl`) / `imp_lap_jan_des`.`juml_pddk`) * 1000),2) AS `api` from (((`imp_lap_jan_des` join `mst_propinsi` on((`imp_lap_jan_des`.`idpropinsi` = `mst_propinsi`.`id_propinsi`))) left join `mst_kelurahan` on((`imp_lap_jan_des`.`kd_desa_kel` = `mst_kelurahan`.`id_kelurahan`))) join `mst_fasyankes` on((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`idpropinsi`,`imp_lap_jan_des`.`idkabupaten`,`imp_lap_jan_des`.`kdfaskes`,`imp_lap_jan_des`.`bulan`,`imp_lap_jan_des`.`kd_desa_kel` */;

/*View structure for view view_lap_bulanan_perkab */

/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perkab` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perkab` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_perkab` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,`mst_fasyankes`.`nama_fasyankes` AS `wilayah`,`mst_fasyankes`.`nama_fasyankes` AS `fasyankes`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from ((`imp_lap_jan_des` join `mst_fasyankes` on(((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_faskes` `jpp` on((`jpp`.`kdfaskes` = `imp_lap_jan_des`.`kdfaskes`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`bulan`,`mst_fasyankes`.`kd_fasyankes` */;

/*View structure for view view_lap_bulanan_perkab_total */

/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perkab_total` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perkab_total` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_perkab_total` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,concat('TOTAL Kab/Kota ',`mst_kabupaten`.`nama_kabupaten`) AS `area`,concat('TOTAL Kab/Kota ',`mst_kabupaten`.`nama_kabupaten`) AS `fasyankes`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from (((`imp_lap_jan_des` join `mst_fasyankes` on(((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_faskes` `jpp` on((`jpp`.`kdfaskes` = `imp_lap_jan_des`.`kdfaskes`))) join `mst_kabupaten` on((`imp_lap_jan_des`.`idkabupaten` = `mst_kabupaten`.`id_kabupaten`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`bulan`,`imp_lap_jan_des`.`idkabupaten` */;

/*View structure for view view_lap_bulanan_perprop */

/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perprop` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perprop` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_perprop` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,`mst_kabupaten`.`nama_kabupaten` AS `wilayah`,`mst_kabupaten`.`nama_kabupaten` AS `kabupaten`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from ((`imp_lap_jan_des` join `mst_kabupaten` on(((`imp_lap_jan_des`.`idkabupaten` = `mst_kabupaten`.`id_kabupaten`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_kabupaten` `jpp` on((`jpp`.`idkabupaten` = `imp_lap_jan_des`.`idkabupaten`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`bulan`,`mst_kabupaten`.`nama_kabupaten` */;

/*View structure for view view_lap_bulanan_perprop_total */

/*!50001 DROP TABLE IF EXISTS `view_lap_bulanan_perprop_total` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_bulanan_perprop_total` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_bulanan_perprop_total` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,concat('TOTAL Prop ',`mst_propinsi`.`nama_propinsi`) AS `area`,concat('TOTAL Prop ',`mst_propinsi`.`nama_propinsi`) AS `fasyankes`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from (((`imp_lap_jan_des` join `mst_fasyankes` on(((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_faskes` `jpp` on((`jpp`.`kdfaskes` = `imp_lap_jan_des`.`kdfaskes`))) join `mst_propinsi` on((`imp_lap_jan_des`.`idpropinsi` = `mst_propinsi`.`id_propinsi`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`bulan`,`imp_lap_jan_des`.`idpropinsi` */;

/*View structure for view view_lap_tahunan_nasional */

/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_nasional` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_nasional` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_nasional` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,`mst_propinsi`.`nama_propinsi` AS `wilayah`,`mst_propinsi`.`nama_propinsi` AS `propinsi`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from ((`imp_lap_jan_des` join `mst_propinsi` on(((`imp_lap_jan_des`.`idpropinsi` = `mst_propinsi`.`id_propinsi`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_propinsi` `jpp` on((`jpp`.`idpropinsi` = `imp_lap_jan_des`.`idpropinsi`))) group by `imp_lap_jan_des`.`tahun`,`mst_propinsi`.`nama_propinsi` */;

/*View structure for view view_lap_tahunan_nasional_total */

/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_nasional_total` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_nasional_total` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_nasional_total` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,concat('TOTAL Nasional ',`mst_propinsi`.`nama_propinsi`) AS `area`,concat('TOTAL Nasional ',`mst_propinsi`.`nama_propinsi`) AS `fasyankes`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from (((`imp_lap_jan_des` join `mst_fasyankes` on(((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_faskes` `jpp` on((`jpp`.`kdfaskes` = `imp_lap_jan_des`.`kdfaskes`))) join `mst_propinsi` on((`imp_lap_jan_des`.`idpropinsi` = `mst_propinsi`.`id_propinsi`))) group by `imp_lap_jan_des`.`tahun` */;

/*View structure for view view_lap_tahunan_perfaskes */

/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perfaskes` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perfaskes` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_perfaskes` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,`imp_lap_jan_des`.`kd_desa_kel` AS `kd_desa_kel`,if(isnull(`mst_kelurahan`.`nama_kelurahan`),if((`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL'),ucase(concat('<b>TOTAL ',`mst_fasyankes`.`nama_fasyankes`,'</b>')),`imp_lap_jan_des`.`kd_desa_kel`),`mst_kelurahan`.`nama_kelurahan`) AS `area`,`mst_propinsi`.`nama_propinsi` AS `propinsi`,`imp_lap_jan_des`.`juml_pddk` AS `jmlpddk`,sum(`imp_lap_jan_des`.`mikroskopis`) AS `mikroskopis`,sum(`imp_lap_jan_des`.`rdt`) AS `rdt`,sum(`imp_lap_jan_des`.`lainnya`) AS `lainnya`,sum(`imp_lap_jan_des`.`total`) AS `total`,sum(`imp_lap_jan_des`.`0_11bln_l`) AS `l1`,sum(`imp_lap_jan_des`.`0_11bln_p`) AS `p1`,sum(`imp_lap_jan_des`.`1_4_l`) AS `l2`,sum(`imp_lap_jan_des`.`1_4_p`) AS `p2`,sum(`imp_lap_jan_des`.`5_9_l`) AS `l3`,sum(`imp_lap_jan_des`.`5_9_p`) AS `p3`,sum(`imp_lap_jan_des`.`10_14_l`) AS `l4`,sum(`imp_lap_jan_des`.`10_14_p`) AS `p4`,sum(`imp_lap_jan_des`.`15_64_l`) AS `l5`,sum(`imp_lap_jan_des`.`15_64_p`) AS `p5`,sum(`imp_lap_jan_des`.`65_l`) AS `l6`,sum(`imp_lap_jan_des`.`65_p`) AS `p6`,sum(`imp_lap_jan_des`.`tot_l`) AS `ltotal`,sum(`imp_lap_jan_des`.`tot_p`) AS `ptotal`,sum(`imp_lap_jan_des`.`tot_pl`) AS `lptotal`,sum(`imp_lap_jan_des`.`kematian_mal`) AS `kematian`,sum(`imp_lap_jan_des`.`bumil_pos_mal`) AS `bumil`,sum(`imp_lap_jan_des`.`pf`) AS `pf`,sum(`imp_lap_jan_des`.`pv`) AS `pv`,sum(`imp_lap_jan_des`.`po`) AS `po`,sum(`imp_lap_jan_des`.`pm`) AS `pm`,sum(`imp_lap_jan_des`.`pk`) AS `pk`,sum(`imp_lap_jan_des`.`mix`) AS `mix`,sum(`imp_lap_jan_des`.`peng_act`) AS `peng_act`,sum(`imp_lap_jan_des`.`peng_nonact`) AS `peng_nonact`,sum(`imp_lap_jan_des`.`peng_primaquin`) AS `peng_primaquin`,sum(`imp_lap_jan_des`.`kasus_pe`) AS `kasus_pe`,sum(`imp_lap_jan_des`.`indigenus`) AS `indigenus`,sum(`imp_lap_jan_des`.`impor`) AS `impor`,sum(`imp_lap_jan_des`.`relaps`) AS `relap`,sum(`imp_lap_jan_des`.`induced`) AS `induced`,round(((sum(`imp_lap_jan_des`.`peng_act`) / sum(`imp_lap_jan_des`.`tot_pl`)) * 100),2) AS `indik_act`,round(((sum(`imp_lap_jan_des`.`peng_primaquin`) / ((sum(`imp_lap_jan_des`.`pv`) + sum(`imp_lap_jan_des`.`po`)) + sum(`imp_lap_jan_des`.`mix`))) * 100),2) AS `indik_primaq`,round(((sum(`imp_lap_jan_des`.`kasus_pe`) / sum(`imp_lap_jan_des`.`tot_pl`)) * 100),2) AS `indik_kasus_pe`,round(((sum(`imp_lap_jan_des`.`tot_pl`) / `imp_lap_jan_des`.`juml_pddk`) * 1000),2) AS `api` from (((`imp_lap_jan_des` join `mst_propinsi` on((`imp_lap_jan_des`.`idpropinsi` = `mst_propinsi`.`id_propinsi`))) left join `mst_kelurahan` on((`imp_lap_jan_des`.`kd_desa_kel` = `mst_kelurahan`.`id_kelurahan`))) join `mst_fasyankes` on((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`idpropinsi`,`imp_lap_jan_des`.`idkabupaten`,`imp_lap_jan_des`.`kdfaskes`,`imp_lap_jan_des`.`kd_desa_kel` */;

/*View structure for view view_lap_tahunan_perkab */

/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perkab` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perkab` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_perkab` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,`mst_fasyankes`.`nama_fasyankes` AS `wilayah`,`mst_fasyankes`.`nama_fasyankes` AS `fasyankes`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from ((`imp_lap_jan_des` join `mst_fasyankes` on(((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_faskes` `jpp` on((`jpp`.`kdfaskes` = `imp_lap_jan_des`.`kdfaskes`))) group by `imp_lap_jan_des`.`tahun`,`mst_fasyankes`.`kd_fasyankes` */;

/*View structure for view view_lap_tahunan_perkab_total */

/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perkab_total` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perkab_total` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_perkab_total` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,concat('TOTAL Kab/Kota ',`mst_kabupaten`.`nama_kabupaten`) AS `area`,concat('TOTAL Kab/Kota ',`mst_kabupaten`.`nama_kabupaten`) AS `fasyankes`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from (((`imp_lap_jan_des` join `mst_fasyankes` on(((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_faskes` `jpp` on((`jpp`.`kdfaskes` = `imp_lap_jan_des`.`kdfaskes`))) join `mst_kabupaten` on((`imp_lap_jan_des`.`idkabupaten` = `mst_kabupaten`.`id_kabupaten`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`idkabupaten` */;

/*View structure for view view_lap_tahunan_perprop */

/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perprop` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perprop` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_perprop` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,`mst_kabupaten`.`nama_kabupaten` AS `wilayah`,`mst_kabupaten`.`nama_kabupaten` AS `kabupaten`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from ((`imp_lap_jan_des` join `mst_kabupaten` on(((`imp_lap_jan_des`.`idkabupaten` = `mst_kabupaten`.`id_kabupaten`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_kabupaten` `jpp` on((`jpp`.`idkabupaten` = `imp_lap_jan_des`.`idkabupaten`))) group by `imp_lap_jan_des`.`tahun`,`mst_kabupaten`.`nama_kabupaten` */;

/*View structure for view view_lap_tahunan_perprop_total */

/*!50001 DROP TABLE IF EXISTS `view_lap_tahunan_perprop_total` */;
/*!50001 DROP VIEW IF EXISTS `view_lap_tahunan_perprop_total` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_lap_tahunan_perprop_total` AS select `imp_lap_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_lap_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_lap_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_lap_jan_des`.`tahun` AS `tahun`,`imp_lap_jan_des`.`bulan` AS `bulan`,concat('TOTAL Prop ',`mst_propinsi`.`nama_propinsi`) AS `area`,concat('TOTAL Prop ',`mst_propinsi`.`nama_propinsi`) AS `fasyankes`,`jpp`.`juml_pddk` AS `jmlpddk`,sum(if(isnull(`imp_lap_jan_des`.`mikroskopis`),0,`imp_lap_jan_des`.`mikroskopis`)) AS `mikroskopis`,sum(if(isnull(`imp_lap_jan_des`.`rdt`),0,`imp_lap_jan_des`.`rdt`)) AS `rdt`,sum(if(isnull(`imp_lap_jan_des`.`lainnya`),0,`imp_lap_jan_des`.`lainnya`)) AS `lainnya`,sum(if(isnull(`imp_lap_jan_des`.`total`),0,`imp_lap_jan_des`.`total`)) AS `total`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_l`),0,`imp_lap_jan_des`.`0_11bln_l`)) AS `l1`,sum(if(isnull(`imp_lap_jan_des`.`0_11bln_p`),0,`imp_lap_jan_des`.`0_11bln_p`)) AS `p1`,sum(if(isnull(`imp_lap_jan_des`.`1_4_l`),0,`imp_lap_jan_des`.`1_4_l`)) AS `l2`,sum(if(isnull(`imp_lap_jan_des`.`1_4_p`),0,`imp_lap_jan_des`.`1_4_p`)) AS `p2`,sum(if(isnull(`imp_lap_jan_des`.`5_9_l`),0,`imp_lap_jan_des`.`5_9_l`)) AS `l3`,sum(if(isnull(`imp_lap_jan_des`.`5_9_p`),0,`imp_lap_jan_des`.`5_9_p`)) AS `p3`,sum(if(isnull(`imp_lap_jan_des`.`10_14_l`),0,`imp_lap_jan_des`.`10_14_l`)) AS `l4`,sum(if(isnull(`imp_lap_jan_des`.`10_14_p`),0,`imp_lap_jan_des`.`10_14_p`)) AS `p4`,sum(if(isnull(`imp_lap_jan_des`.`15_64_l`),0,`imp_lap_jan_des`.`15_64_l`)) AS `l5`,sum(if(isnull(`imp_lap_jan_des`.`15_64_p`),0,`imp_lap_jan_des`.`15_64_p`)) AS `p5`,sum(if(isnull(`imp_lap_jan_des`.`65_l`),0,`imp_lap_jan_des`.`65_l`)) AS `l6`,sum(if(isnull(`imp_lap_jan_des`.`65_p`),0,`imp_lap_jan_des`.`65_p`)) AS `p6`,sum(if(isnull(`imp_lap_jan_des`.`tot_l`),0,`imp_lap_jan_des`.`tot_l`)) AS `ltotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_p`),0,`imp_lap_jan_des`.`tot_p`)) AS `ptotal`,sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) AS `lptotal`,sum(if(isnull(`imp_lap_jan_des`.`kematian_mal`),0,`imp_lap_jan_des`.`kematian_mal`)) AS `kematian`,sum(if(isnull(`imp_lap_jan_des`.`bumil_pos_mal`),0,`imp_lap_jan_des`.`bumil_pos_mal`)) AS `bumil`,sum(if(isnull(`imp_lap_jan_des`.`pf`),0,`imp_lap_jan_des`.`pf`)) AS `pf`,sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) AS `pv`,sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`)) AS `po`,sum(if(isnull(`imp_lap_jan_des`.`pm`),0,`imp_lap_jan_des`.`pm`)) AS `pm`,sum(if(isnull(`imp_lap_jan_des`.`pk`),0,`imp_lap_jan_des`.`pk`)) AS `pk`,sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)) AS `mix`,sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) AS `peng_act`,sum(if(isnull(`imp_lap_jan_des`.`peng_nonact`),0,`imp_lap_jan_des`.`peng_nonact`)) AS `peng_nonact`,sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) AS `peng_primaquin`,sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) AS `kasus_pe`,sum(if(isnull(`imp_lap_jan_des`.`indigenus`),0,`imp_lap_jan_des`.`indigenus`)) AS `indigenus`,sum(if(isnull(`imp_lap_jan_des`.`impor`),0,`imp_lap_jan_des`.`impor`)) AS `impor`,sum(if(isnull(`imp_lap_jan_des`.`relaps`),0,`imp_lap_jan_des`.`relaps`)) AS `relap`,sum(if(isnull(`imp_lap_jan_des`.`induced`),0,`imp_lap_jan_des`.`induced`)) AS `induced`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_act`),0,`imp_lap_jan_des`.`peng_act`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_act`,round(((sum(if(isnull(`imp_lap_jan_des`.`peng_primaquin`),0,`imp_lap_jan_des`.`peng_primaquin`)) / ((sum(if(isnull(`imp_lap_jan_des`.`pv`),0,`imp_lap_jan_des`.`pv`)) + sum(if(isnull(`imp_lap_jan_des`.`po`),0,`imp_lap_jan_des`.`po`))) + sum(if(isnull(`imp_lap_jan_des`.`mix`),0,`imp_lap_jan_des`.`mix`)))) * 100),2) AS `indik_primaq`,round(((sum(if(isnull(`imp_lap_jan_des`.`kasus_pe`),0,`imp_lap_jan_des`.`kasus_pe`)) / sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`))) * 100),2) AS `indik_kasus_pe`,round(((sum(if(isnull(`imp_lap_jan_des`.`tot_pl`),0,`imp_lap_jan_des`.`tot_pl`)) / `jpp`.`juml_pddk`) * 1000),2) AS `api` from (((`imp_lap_jan_des` join `mst_fasyankes` on(((`imp_lap_jan_des`.`kdfaskes` = `mst_fasyankes`.`kd_fasyankes`) and (`imp_lap_jan_des`.`kd_desa_kel` = 'TOTAL')))) join `view_juml_penduduk_faskes` `jpp` on((`jpp`.`kdfaskes` = `imp_lap_jan_des`.`kdfaskes`))) join `mst_propinsi` on((`imp_lap_jan_des`.`idpropinsi` = `mst_propinsi`.`id_propinsi`))) group by `imp_lap_jan_des`.`tahun`,`imp_lap_jan_des`.`idpropinsi` */;

/*View structure for view view_rekap_header_bulanan */

/*!50001 DROP TABLE IF EXISTS `view_rekap_header_bulanan` */;
/*!50001 DROP VIEW IF EXISTS `view_rekap_header_bulanan` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rekap_header_bulanan` AS select `imp_header_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_header_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_header_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_header_jan_des`.`tahun` AS `tahun`,`imp_header_jan_des`.`bulan` AS `bulan`,sum(`imp_header_jan_des`.`jum_suspek`) AS `suspek`,sum(`imp_header_jan_des`.`tot_dar_pos`) AS `darpositif`,sum(`imp_header_jan_des`.`tot_dar_neg`) AS `darnegatif`,sum(`imp_header_jan_des`.`tot_dar`) AS `darah`,sum(`imp_header_jan_des`.`tot_pos_pcd`) AS `pos_pcd`,sum(`imp_header_jan_des`.`tot_neg_pcd`) AS `neg_pcd`,sum(`imp_header_jan_des`.`tot_pcd`) AS `tot_pcd`,sum(`imp_header_jan_des`.`bumil_skrin_mal`) AS `bumil_scmalaria`,sum(`imp_header_jan_des`.`mtbs`) AS `mtbs`,sum(`imp_header_jan_des`.`kelambu_bumil`) AS `kelambu_bumil`,sum(`imp_header_jan_des`.`kelambu_bayi`) AS `kelambu_bayi`,sum(`imp_header_jan_des`.`kelambu_masal`) AS `kelambu_masal`,sum(`imp_header_jan_des`.`kelambu_lainnya`) AS `kelambu_lainnya`,sum(`imp_header_jan_des`.`irs`) AS `irs`,sum(`imp_header_jan_des`.`fokus_aktif`) AS `fokus_aktif` from `imp_header_jan_des` group by `imp_header_jan_des`.`kdfaskes`,`imp_header_jan_des`.`idpropinsi`,`imp_header_jan_des`.`idkabupaten`,`imp_header_jan_des`.`tahun`,`imp_header_jan_des`.`bulan` */;

/*View structure for view view_rekap_header_tahunan */

/*!50001 DROP TABLE IF EXISTS `view_rekap_header_tahunan` */;
/*!50001 DROP VIEW IF EXISTS `view_rekap_header_tahunan` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rekap_header_tahunan` AS select `imp_header_jan_des`.`kdfaskes` AS `kdfaskes`,`imp_header_jan_des`.`idpropinsi` AS `idpropinsi`,`imp_header_jan_des`.`idkabupaten` AS `idkabupaten`,`imp_header_jan_des`.`tahun` AS `tahun`,sum(`imp_header_jan_des`.`jum_suspek`) AS `suspek`,sum(`imp_header_jan_des`.`tot_dar_pos`) AS `darpositif`,sum(`imp_header_jan_des`.`tot_dar_neg`) AS `darnegatif`,sum(`imp_header_jan_des`.`tot_dar`) AS `darah`,sum(`imp_header_jan_des`.`tot_pos_pcd`) AS `pos_pcd`,sum(`imp_header_jan_des`.`tot_neg_pcd`) AS `neg_pcd`,sum(`imp_header_jan_des`.`tot_pcd`) AS `tot_pcd` from `imp_header_jan_des` group by `imp_header_jan_des`.`kdfaskes`,`imp_header_jan_des`.`idpropinsi`,`imp_header_jan_des`.`idkabupaten`,`imp_header_jan_des`.`tahun` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

<?php
$lprop = mysqli_query($dbconn,"select id_propinsi,nama_propinsi from mst_propinsi");
?>
<div class="col-lg-12">
    <div class="panel">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-list fa-fw"></i>&nbsp;Daftar Kabupaten
            </div>
            <div class="panel-body">
                <div class="row col-lg-12" id="inputForm" style="padding: 10px;">
                    <div class="col-lg-2">&nbsp;</div>
                    <div class="col-lg-10">
                        <form id="kabform" method="post" style="display: none">
                            <input type="hidden" id="idkab" name="idkab" value="">
<table class="tblInput">
   
    <tbody>
    <tr>
        <th>Propinsi</th>
        <th>:
        <select name="propinsi" id="propinsi">
            
            <option value="">-- Pilih --</option>
        <?php
        while($np=mysqli_fetch_object($lprop)){
            ?>
            <option value="<?php echo $np->id_propinsi;?>"><?php echo $np->nama_propinsi;?></option>
            <?php } ?>
        </select>
        </th>
    </tr>
    <tr>
        <th>Kode kabupaten</th>
    <th>: <input type="text" name="kodekab" size="20" id="kodekab" maxlength="4"></th>
    </tr>
     <tr>
        <th>Nama Kab/Kota</th>
    <th>: <input type="text" name="namakab" size="40" id="namakab"></th>
    </tr>
     <tr>
        <th>&nbsp;</th>
        <th>
            <button id="editdata" class="btn btn-primary btn-xs" style="display: none">Edit</button>&nbsp;
            <button id="newdata" class="btn btn-primary btn-xs">Simpan</button>
            
        </th>
     </tr>
    </tbody>
</table>
                        </form>
                </div>
         
                </div>
                <div class="row col-lg-12" style="height: 350px;overflow-y: scroll">
                <table class="tblListData">
                    <thead>
                        <tr><th colspan="5" style="text-align: left">
                            <button class="btn btn-default btn-xs" id="btnAddkab" onclick="showForm('#kabform');validasi('#kabform')"><i class="fa fa-plus-circle" aria-hidden="true"></i>
Tambah data</button><i class="fa fa-search" aria-hidden='true'></i><input type="text" id="cari" name="cari" placeholder="Cari Kode/Nama Kabupaten"  value="" onkeyUp="searchKab(this.value)">
                        </th></tr>
                        <tr>
                        <th style="text-align: center;width: 5%">No.</th>
                        <th style="text-align: center;width: 25%">Kode Kabupaten</th>
                        <th  style="text-align: center;width: 25%">Kabupaten/Kota</th>
                        <th  style="text-align: center;width: 25%">Propinsi</th>
                        <th  style="text-align: center;width: 20%">Ubah/Hapus</th>
                        </tr>
                    </thead>
                    <tbody id="datakabupaten">
                        
                    </tbody>
                </table>
            </div>
                
            </div>
            <div class="panel-footer text-right">&nbsp;
            
            
            </div>
        </div>
    </div>
    
</div>


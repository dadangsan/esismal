<?php
require_once("config/database-connect.php");
?>
<div class="panel" id="formMenu" style="display: none">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-server" aria-hidden="true"></i>&nbsp;Menu Baru
        </div>
        <div class="panel-body">
         <table cellpadding="0" cellspacing="0" border="0" class="tblInput" style="width: 80%">
         <tbody>
            <tr>
                <th>Label Menu</th>
                <th>: <input type="text" name="labelmenu" id="labelmenu" size="40"></th>
            </tr>
            <tr>
                <th>Url/Path</th>
                <th>: <input type="text" name="url" id="url" size="40"></th>
            </tr>
            <tr>
                <th>Menu Induk</th>
                <th>: <select name="mainmenu" id="mainmenu">
                <option value="0">-- Tidak ada --</option>
                <?php
                $str =mysqli_query($dbconn,"SELECT id_menu,caption from menu");
                while($mn=mysqli_fetch_array($str)){?>
                <option value="<?php echo $mn['id_menu'];?>"><?php echo $mn['caption'];?></option>
                
                <?php } ?>
                </select></th>
            </tr>
            
            <tr>
                <th>Status</th>
                <th>:
                <input type="radio" name="status" value="1" id="status_1" checked="checked">&nbsp;Aktif&nbsp;&nbsp;
                <input type="radio" name="status" value="0" id="status_0">&nbsp;Tidak Aktif
                </th>
            </tr>
            <input type="hidden" id="idmenu" value="">
            <tr>
                <th colspan="2" style="text-align: center">
                    <button type="button" class="btn btn-primary btn-xs" id="edmenu" style="display: none" onclick="updateMenu()">Edit</button>
                    <button type="button" class="btn btn-primary btn-xs" id="admenu" onclick="addNewMenu()">Tambahkan</button>
                   
                </th>
            </tr>
         </tbody>
         </table>
            
            
          
            
        </div>
        <div class="panel-footer">&nbsp;</div>
    </div>
</div>
<table class="tblListData">
    <thead>
        <tr>
            <th colspan="6" style="text-align: left">
                
                <button type="btn" class="btn btn-default btn-xs" onclick="showFormMenu()" id="simpan"><i class="fa fa-plus-circle fa-fw" aria-hidden="true"></i>
 Tambah Menu</button>
            </th>
        </tr>
        <tr>
        <th  style="width: 5%">No.</th>
        <th   style="width: 20%">Label</th>
        <th   style="width: 10%">Level</th>
        <th   style="width: 20%">Menu Utama</th>
        <th   style="width: 25%">Url/Path</th>
        <th   style="width: 20%">Konfigurasi</th>
        </tr>
    </thead>
    
    <tbody id="listdata">
    </tbody>
</table>
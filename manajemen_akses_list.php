<?php
require_once("config/database-connect.php");
$getGroup = mysqli_query($dbconn,"select id_group,`group` as namagroup from `group1`");
$getMenu1 = mysqli_query($dbconn,"SELECT id_menu,caption as label,isparent from menu where id_parent='0' order by seqno ASC");
?>
<div class="panel" id="formMenu" style="display: none">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-sitemap" aria-hidden="true"></i>&nbsp;Membuat Pemberian Akses
        </div>
        <div class="panel-body">
         <table cellpadding="0" cellspacing="0" border="0" class="tblInput" style="width: 80%">
         <tbody>
            <tr>
                <th>Nama Group</th>
                <th>:
                <select name="gid" id="gid" onchange="checkGroup(this)">
                    <option value="">-- Pilih --</option>
                    <?php
                    while($ls = mysqli_fetch_array($getGroup)){
                    ?>
                    <option value="<?php echo $ls['id_group'];?>"><?php echo $ls['namagroup'];?></option>
                    <?php  } ?>
                </select>
                </th>
            </tr>
            
            <tr>
                <th style="vertical-align: top">Akses Menu</th>
                <th>
                    <?php
                    while($mn = mysqli_fetch_array($getMenu1)){
                    ?>
                    <input type="checkbox" id="menu_<?php echo $mn['id_menu'];?>" name="idmenu" value="<?php echo $mn['id_menu'];?>" class="mainmenu menu">&nbsp;<label for="menu_<?php echo $mn['id_menu'];?>"><?php echo $mn['label'];?></label><br>
                    <?php
                    $subMenu = mysqli_query($dbconn,"SELECT id_menu,caption as label,isparent from menu where id_parent='".$mn['id_menu']."' order by seqno ASC");
                    while($sm=mysqli_fetch_array($subMenu)){
                        ?>
                         &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true"></i>

&nbsp;<input type="checkbox" id="menu_<?php echo $sm['id_menu'];?>" name="idmenu[]" value="<?php echo $sm['id_menu'];?>" class="submenu_<?php echo $mn['id_menu'];?> menu">&nbsp;<label for="menu_<?php echo $sm['id_menu'];?>"><?php echo $sm['label'];?></label><br>
                        <?php
                    }
                    }
                    ?>
                </th>
            </tr>
           
            <input type="hidden" id="gid" value="">
            <tr>
                <th colspan="2" style="text-align: center">
                    <button type="button" class="btn btn-primary btn-xs" id="edakses" style="display: none" onclick="updateAkses()">Edit</button>
                    <button type="button" class="btn btn-primary btn-xs" id="adakses" onclick="addNewAkses()">Simpan</button>
                    <button type="button" class="btn btn-primary btn-xs" id="clearAkses" onclick="checkall()">Pilih Semua</button>
                    <button type="button" class="btn btn-primary btn-xs" id="clearAkses" onclick="uncheck()">Hilangkan Pilihan</button>
                   
                </th>
            </tr>
         </tbody>
         </table>
            
            
          
            
        </div>
        <div class="panel-footer">&nbsp;</div>
    </div>
</div>
<table class="tblListData">
    <thead>
        <tr>
            <th colspan="6" style="text-align: left">
                
                <button type="btn" class="btn btn-default btn-xs" onclick="showFormMenu()" id="simpan"><i class="fa fa-sitemap fa-fw" aria-hidden="true"></i>
 Menambah Akses</button>
            </th>
        </tr>
        <tr>
        <th   style="width: 5%">No.</th>
        <th   style="width: 40%">Nama Group</th>
        <th   style="width: 40%">Akses Menu</th>
        <th   style="width: 15%">Ubah/Hapus</th>
        </tr>
    </thead>
    
    <tbody id="listdata">
    </tbody>
</table>
<?php
require_once("config/database-connect.php");
?>
<div class="panel" id="formMenu" style="display: none">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-server" aria-hidden="true"></i>&nbsp;Membuat Group Baru
        </div>
        <div class="panel-body">
         <table cellpadding="0" cellspacing="0" border="0" class="tblInput" style="width: 80%">
         <tbody>
            <tr>
                <th>Nama Group</th>
                <th>: <input type="text" name="labelgroup" id="labelgroup" size="40"></th>
            </tr>
           
            <input type="hidden" id="gid" value="">
            <tr>
                <th colspan="2" style="text-align: center">
                    <button type="button" class="btn btn-primary btn-xs" id="edgroup" style="display: none" onclick="updateGroup()">Edit</button>
                    <button type="button" class="btn btn-primary btn-xs" id="adgroup" onclick="addNewGroup()">Tambahkan</button>
                   
                </th>
            </tr>
         </tbody>
         </table>
            
            
          
            
        </div>
        <div class="panel-footer">&nbsp;</div>
    </div>
</div>
<table class="tblListData">
    <thead>
        <tr>
            <th colspan="6" style="text-align: left">
                
                <button type="btn" class="btn btn-default btn-xs" onclick="showFormMenu()" id="simpan"><i class="fa fa-users fa-fw" aria-hidden="true"></i>
 Membuat Group</button>
            </th>
        </tr>
        <tr>
        <th   style="width: 10%">id group</th>
        <th   style="width: 60%">Nama Group</th>
        <th   style="width: 30%">Ubah/Hapus</th>
        </tr>
    </thead>
    
    <tbody id="listdata">
    </tbody>
</table>
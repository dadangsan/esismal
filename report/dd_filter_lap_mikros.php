<?
session_start();
require_once("../config/database-connect.php");
$s = (object)$_SESSION;
$p = (object)$_POST;
//print_r($s);
?>
<script>
/* $('#idata').click(function () {
    // add loading image to div
    $('#loading7').html('<img src="../esismalv2/loader/ajax-loader.gif"> loading...');
    
    // run ajax request
    $.ajax({
        type: "GET",
        dataType: "json",
        //url: "https://api.github.com/users/jveldboom",
		url: "../esismalv2/data/dd_data_harian.php",
        
        success: function (d) {
            // replace div's content with returned data
            // $('#loading').html('<img src="'+d.avatar_url+'"><br>'+d.login);
            // setTimeout added to show loading
            $("#loading7").hide();
			setTimeout(function () {
                $('#loading7').html('<img src="' + d.avatar_url + '"><br>' + d.login);
            }, 500);
        }
    });
});
*/

    $('document').ready(function(){
		//filterValidation('#filterForm');
        $('.thn, .bln').hide();
        $('#ptahunan').click(function(){
            $('.thn').show();
            if ($('.bln').is(':visible')) {
                //code
                $('.bln').hide();
            }
        });
        
        $('#pbulanan').click(function(){
           $('.thn, .bln').show(); 
        });
        //level pusat
        $('#pusat').click(function(){
            $('.trprop,.trkab,.trfaskes').hide();
        })
        //level propinsi
        $('#prop').click(function(){
            $('.trprop').show();
            $('#propinsi').removeAttr('disabled');
            $('.trkab,.trfaskes').hide();
        });
        
        //level kabupaten
        $('#kab').click(function(){
            $('.trprop, .trkab').show();
            $('.trfaskes').hide();
            $('#propinsi, #kabupaten').removeAttr('disabled');
        })
        
        //level faskes
        $('#faskes').click(function(){
            $('.trprop,.trkab,.trfaskes').show();
            $('#propinsi, #kabupaten,#fasyankes').removeAttr('disabled');
            if ($('#kabupaten').is(":visible")) {
                //code
                getComboFaskesreport('fasyankes','#kabupaten');
            }
        });
        
        var lev = "<?php echo $p->level;?>";
        
        if (lev=='1' || lev=='2') {
            //code
            $('#pusat').attr('checked','checked');
            $('.trprop,.trkab,.trfaskes').hide();
            $('#pusat, #prop, #kab, #faskes').removeAttr('disabled');
        }else if (lev=='3') {
            //code
             $('#prop').attr('checked','checked');
             $('.nas').hide();
            $('.trkab,.trfaskes').hide();
            $('#prop, #kab, #faskes, #propinsi').removeAttr('disabled');
            $('#propinsi').val('<?php echo $s->propinsi;?>');
        }else if (lev=='4') {
            //code
             $('#kab').attr('checked','checked');
             $('.nas,.prov').hide();
            $('.trfaskes').hide();
            $('#kab, #faskes,#propinsi,#kabupaten,#fasyankes').removeAttr('disabled');
            $('#propinsi').val('<?php echo $s->propinsi;?>');
             $('#kabupaten').val('<?php echo $s->kabupaten;?>');
        }else if (lev=='5') {
            //code
             $('#faskes').attr('checked','checked');
             $('.nas,.prov,.kabs').hide();
            //$('.trfaskes').hide();
            $('#fasyankes').removeAttr('disabled');
            $('#propinsi').val('<?php echo $s->propinsi;?>');
            $('#kabupaten').val('<?php echo $s->kabupaten;?>');
			
			 $('#fasyankes').multipleSelect({
				//placeholder: "Pilih Propinsi",
				placeholder: "Pilih Faskes",
				filter:true
			}); 
        }
    });
</script>
<?php
if($p->level=='1' || $p->level=='2' || $p->level=='7' ){
    $SQLprop = "SELECT id_propinsi,nama_propinsi FROM mst_propinsi";
	$SQLkab = "SELECT id_kabupaten,nama_kabupaten FROM mst_kabupaten";
	$SQLfaskes = "SELECT kd_fasyankes,nama_fasyankes from mst_fasyankes where kabupaten='$s->kabupaten'";
	//echo "querynya= ".$SQLfaskes;
}elseif($p->level=='3'){
   //echo $s->propinsi;
$SQLprop = "SELECT id_propinsi,nama_propinsi FROM mst_propinsi WHERE id_propinsi='$s->propinsi'";
$SQLkab = "SELECT id_kabupaten,nama_kabupaten FROM mst_kabupaten WHERE id_propinsi='$s->propinsi'";
$SQLfaskes = "SELECT kd_fasyankes,nama_fasyankes from mst_fasyankes where propinsi='$s->propinsi'";
}elseif($p->level=='4'){
$SQLprop = "SELECT id_propinsi,nama_propinsi FROM mst_propinsi WHERE id_propinsi='$s->propinsi'";
$SQLkab = "SELECT id_kabupaten,nama_kabupaten FROM mst_kabupaten WHERE id_kabupaten='$s->kabupaten'";
$SQLfaskes = "SELECT kd_fasyankes,nama_fasyankes from mst_fasyankes where kabupaten='$s->kabupaten'";
}elseif($p->level=='5'){
$SQLprop = "SELECT id_propinsi,nama_propinsi FROM mst_propinsi WHERE id_propinsi='$s->propinsi'";
$SQLkab = "SELECT id_kabupaten,nama_kabupaten FROM mst_kabupaten WHERE id_kabupaten='$s->kabupaten'";
$SQLfaskes = "SELECT kd_fasyankes,nama_fasyankes  from mst_fasyankes where kd_fasyankes='$s->kd_fasyankes'";
//echo "faskes123= ".$SQLfaskes;
}
//echo $SQLprop;
$propQuery = mysqli_query($dbconn,$SQLprop);
$kabQuery = mysqli_query($dbconn,$SQLkab);
$fasQuery = mysqli_query($dbconn,$SQLfaskes);
//echo mysqli_errno($propQuery);
?>
<table class="tblListData">


<tr>
<td style="width: 20%;padding-left: 10px">
  <label>Periode Laporan</label> 
</td>
<td  style="border-bottom: none;width: 80%;padding-left: 10px">
    <!-- <input type="radio" id="ptahunan" name="periode" value="1">&nbsp;<label for="ptahunan">Tahunan</label> -->
&nbsp;
<input type="radio" id="pbulanan" name="periode" value="2">&nbsp;<label for="pbulanan">Triwulan</label>

</td>
</tr>
<tr>
    <td style="border-right:none;padding-left: 10px"></td>
    <td style="border-top:none;padding-left: 10px">
        <label for="tahun" class="thn">Tahun</label>
        <select id="tahun" name="tahun" class="thn">
            <?php
            for($i=date('Y');$i>=2018;$i--){
              ?>
              <option value="<?php echo $i;?>"><?php echo $i;?></option>
              <?php
            }
            ?>
        </select>		
       &nbsp;
       <label for="bulan1" class="bln">Triwulan</label>
       <select id="bulan1" name="bulan1" class="bln">
        <option value="">-- Pilih --</option>
        <?php for($i=1;$i<=4;$i++){?>
        <option value="<?php echo $i;?>"><?php echo $i;?></option>
        <?php } ?>
       </select>
       
      <!-- <label for="bulan2" class="bln">S/D</label>
       <select id="bulan2" name="bulan2" class="bln">
        <option value="">-- Pilih --</option>
        <?php for($i=1;$i<=12;$i++){?>
        <option value="<?php echo $i;?>"><?php echo $i;?></option>
        <?php } ?>
       </select>
	   -->
    </td>
</tr>

<tr>
<td style="width: 20%;padding-left: 10px">
  <label>Level</label> 
</td>
<td  style="width: 80%;padding-left: 10px">
<?php 
if ($_SESSION['id_group'] ==1 or $_SESSION['id_group'] ==2){
?>
    <input type="radio" id="pusat" name="level" value="1" class="nas">&nbsp;<label class="nas" for="pusat">Nasional</label>
	&nbsp;
	<input type="radio" id="prop" name="level" value="2" class="prov">&nbsp;<label class="prov" for="prop">Propinsi</label>
&nbsp;
<input type="radio" id="kab" name="level" value="3" class="kabs">&nbsp;<label class="kabs" for="kab">Kabupaten</label>
&nbsp;
<?php } elseif ($_SESSION['id_group'] ==3){ ?>
<input type="radio" id="prop" name="level" value="2" class="prov">&nbsp;<label class="prov" for="prop">Propinsi</label>
&nbsp;
<input type="radio" id="kab" name="level" value="3" class="kabs">&nbsp;<label class="kabs" for="kab">Kabupaten</label>
&nbsp;
<?php 
}elseif ($_SESSION['id_group'] ==4){ 
?>
<input type="radio" id="kab" name="level" value="3" class="kabs">&nbsp;<label class="kabs" for="kab">Kabupaten</label>
&nbsp;
<?php } ?>

<!-- <input type="radio" id="faskes" name="level" value="4" class="fas">&nbsp;<label for="faskes" class="fas">Faskes</label> -->
</td>
</tr>

<tr class="trprop">
    <td style="padding-left: 10px">Propinsi</td>
    <td style="padding-left: 10px">
        <select name="propinsi" id="propinsi" disabled="disabled" onchange="getComboKab('kabupaten',this)" >
            <option value="">-- Pilih --</option>
            <?php while($prow = mysqli_fetch_assoc($propQuery)){ ?>
            <option value="<?php echo $prow['id_propinsi'];?>"><?php echo $prow['nama_propinsi'];?></option>
            <?php } ?>
        </select>
    
    </td>
</tr>

<tr class="trkab">
    <td style="padding-left: 10px">Kabupaten</td>
    <td style="padding-left: 10px">
        <select name="kabupaten" id="kabupaten" disabled="disabled"   onchange="getComboFaskesreport('fasyankes',this)">
            <option value="">-- Pilih --</option>
             
            <?php while($krow = mysqli_fetch_assoc($kabQuery)){ ?>
            <option value="<?php echo $krow['id_kabupaten'];?>"><?php echo $krow['nama_kabupaten'];?></option>
            <?php } ?>
        </select>
    
    </td>
</tr>

<tr class="trfaskes">
    <td style="padding-left: 10px">Fasyankes</td>
    <td style="padding-left: 10px">
        <select name="fasyankes[]" id="fasyankes"  multiple="multiple" size='1' style="width: 305px">
            <!-- <option value="">-- Pilih --</option> -->
            <?php while($frow = mysqli_fetch_assoc($fasQuery)){ ?>
            <option value="<?php echo $frow['kd_fasyankes'];?>"><?php echo $frow['nama_fasyankes'];?></option>
            <?php } ?>
        </select>
    
    </td>
</tr>
<tr>
    <td style="padding-left: 10px;text-align: center" colspan="2">
        
        <!-- <input type="button" id="submit" name="Pilih" value="Submit" onclick="filterValidation('#filterForm','1');"/>
         <input type="button" name="phpexcel" value="EXCEL"  onclick="filterValidation('#filterForm','2');"/>
    -->
	<!-- <button id="idata" onclick="filterValidation('#filterForm');" value="1">Submit</button> -->
    <button id="idata" onclick="filterValidation('#filterForm');" value="1">Submit</button>
	<!-- filterValidation=assets/internal/js/hp_validasi_laporan.js -->
	
	<!-- <div id="loading7"></div> -->
	<button id="idata" onclick="getExcel()" value="2">Excel</button>
	</td>
</tr>
</table>
<?php
error_reporting(E_ALL); 
require_once("../config/database-connect.php");
//date_default_timezone_set('UTC');
date_default_timezone_set('Asia/Bangkok');

require_once '/../PHPexcel176/Classes/PHPExcel.php';
$kodep = implode("','", $_POST['fasyankes']);
$mrQ2=  "SELECT a.kd_fasyankes, a.nama_fasyankes, b.nama_propinsi, c.nama_kabupaten FROM  mst_fasyankes a inner join mst_propinsi b ON a.propinsi=b.id_propinsi inner join mst_kabupaten c on a.kabupaten=c.id_kabupaten where a.kd_fasyankes in ('".$kodep."')"; 
//echo "<br/>mrQ2-1= ".$mrQ2;
//die();
$mrQ = mysqli_query($dbconn,$mrQ2);
$row = mysqli_num_rows($mrQ); 
while($njk = mysqli_fetch_array($mrQ)){
	$namars2[] = $njk['nama_fasyankes']; 
	$namaprop = $njk['nama_propinsi']; 
	$namakab = $njk['nama_kabupaten']; 
	/* echo "<br/>Tahun= ".$_POST['tahun']." Bulan=".$_POST['bulan1'] ." s/d " .$_POST['bulan2'];
	echo "<br/>Prop= ".$namaprop;
		echo "  |Kab= ".$namakab;
		echo "  |Faskes=".implode(",", $namars2); */
	}
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("DadangSupriyadi")
							 ->setLastModifiedBy("DadangSupriyadi")
							 ->setTitle("Laporan RegMal-1")
							 ->setSubject("Laporan RegMal-1")
							 ->setDescription("Menampilkan Laporan register Malaria")
							 ->setKeywords("Laporan RegMal-1")
							 ->setCategory("Laporan Malaria");
 
// Create the worksheet
$objPHPExcel->setActiveSheetIndex(0);

// mulai judul kolom dengan row 12
$objPHPExcel->getActiveSheet()->setCellValue('A2', "Tahun:")
							->setCellValue('B2', $_POST['tahun'])

							->setCellValue('D2', "Bulan:")
							->setCellValue('E2', $_POST['bulan1'])
							->setCellValue('F2', "s/d:")
							->setCellValue('G2', $_POST['bulan2'])
							->setCellValue('A3', "Propinsi:")
							->setCellValue('B3', $namaprop)
							
							->setCellValue('A4', "Kabupaten:")
							->setCellValue('B4', $namakab)
							->setCellValue('A5', "Faskes:")	
							->setCellValue('B5', implode(",", $namars2))						
							
							  ->setCellValue('A12', "No")
							  ->setCellValue('B12', "Propinsi")
							  ->setCellValue('C12', "Kabupaten")
							  ->setCellValue('D12', "Faskes")
							  ->setCellValue('E12', "NIK")
							  ->setCellValue('F12', "Nama Penderita")
							  ->setCellValue('G12', "Umur (Angka)")
							  ->setCellValue('H12', "Umur (bln/Thn)")
							  ->setCellValue('I12', "Jenis Kelamin")
							  ->setCellValue('J12', "Hamil/Tidak Hamil")
							  ->setCellValue('K12', "Desa/ Kelurahan")
							  ->setCellValue('L12', "Bulan Kunjungan")
							  ->setCellValue('M12', "Tgl Kunjungan")
							  ->setCellValue('N12', "Konfirmasi Lab")
							  ->setCellValue('O12', "Parasit")
							  ->setCellValue('P12', "Kondisi Pasien")
							  ->setCellValue('Q12', "DHP Tab")
							  ->setCellValue('R12', "Primaquin Tab")
							  ->setCellValue('S12', "Kina Tab")
							  ->setCellValue('T12', "Artesunat Inj")
							  ->setCellValue('U12', "Artemeter Inj")
							  ->setCellValue('V12', "Kina Inj")
							  ->setCellValue('W12', "PE")
							  ->setCellValue('X12', "Klasifikasi Asal Penularan");
$objPHPExcel->getActiveSheet()->getRowDimension(5)->setRowHeight(4 * 12.75 + 2.25);
$objPHPExcel->getActiveSheet()->mergeCells('B5:O5');
$objPHPExcel->getActiveSheet()->getStyle('B5')->getAlignment()->setWrapText(true);


$datetime = date('Y-m-d_H:i:s');
//$jumlah = mysql_num_rows($SQL);
$dataArray= array();
$no=0;
$mrQ2=  "SELECT a.nik, a.nama_pende, a.umur, a.th_bl, a.jnskel, a.hamil, 
a.kd_desa_kel, a.bulankun, a.tglkun, a.lab, a.parasit, a.kondisi, 
a.dhp, a.primaquin, a.kinatablet, a.artesunat, a.artemeter, a.kinainj, a.pex, a.klasifikasi, c.`nama_kabupaten`, 
d.nama_propinsi, e.nama_fasyankes FROM imp_regmal1 a INNER JOIN mst_kabupaten c ON(a.idkabupaten=c.id_kabupaten) 
INNER JOIN mst_propinsi d ON(a.`idpropinsi`=d.`id_propinsi`) INNER JOIN mst_fasyankes e ON (a.idfaskes=e.kd_fasyankes) 
WHERE a.tahun_lap='".$_POST['tahun']."' AND a.bulankun >='".$_POST['bulan1']."' AND a.bulankun <='".$_POST['bulan2']."' AND a.idfaskes IN ('".$kodep."')"; 
//echo "<br/>mrQ2-2= ".$mrQ2;
//die();
$mrQ = mysqli_query($dbconn,$mrQ2);
//echo "<br/>mr2= ".$mrQ2;
$row = mysqli_num_rows($mrQ);    
//while($row = mysqli_fetch_array($mrQ2, MYSQL_ASSOC)){
	while($row = mysqli_fetch_array($mrQ)){
	
	$no++;
	$row_array['no'] 		  = $no;
	$row_array['propinsi'] 	  = $row['nama_propinsi'];
	$row_array['kabupaten']   = $row['nama_kabupaten'];
	$row_array['faskes'] 	  = $row['nama_fasyankes'];
	//$row_array['tpttgllahir'] = $row['nik'].", ".$row['tanggal_lahir'];
	$row_array['nik'] 			= $row['nik'];
	$row_array['nama_pende'] 	= $row['nama_pende'];
	$row_array['umur'] 	  		= $row['umur'];
	$row_array['th_bl']			= $row['th_bl'];
	$row_array['jnskel']  		= $row['jnskel'];
	$row_array['hamil'] 	  	= $row['hamil'];
	$row_array['kd_desa_kel'] 	= $row['kd_desa_kel'];
	$row_array['bulankun'] 	  	= $row['bulankun'];
	$row_array['tglkun'] 	  	= $row['tglkun'];
	$row_array['lab'] 	  		= $row['lab'];
	$row_array['parasit'] 	  	= $row['parasit'];
	$row_array['kondisi'] 	  	= $row['kondisi'];
	$row_array['dhp'] 	  		= $row['dhp'];
	$row_array['primaquin'] 	= $row['primaquin'];
	$row_array['kinatablet'] 	= $row['kinatablet'];
	$row_array['artesunat'] 	= $row['artesunat'];
	$row_array['artemeter'] 	= $row['artemeter'];
	$row_array['kinainj'] 	  	= $row['kinainj'];
	$row_array['pex'] 	  		= $row['pex'];
	$row_array['klasifikasi'] 	= $row['klasifikasi'];
	
	array_push($dataArray,$row_array);
}

// Mulai record dengan row 8
$nox=$no+12;
$objPHPExcel->getActiveSheet()->fromArray($dataArray, NULL, 'A13'); 

// Set page orientation and size
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_LEGAL);
$objPHPExcel->getActiveSheet()->getPageMargins()->setTop(0.75);
$objPHPExcel->getActiveSheet()->getPageMargins()->setRight(0.75);
$objPHPExcel->getActiveSheet()->getPageMargins()->setLeft(0.75);
$objPHPExcel->getActiveSheet()->getPageMargins()->setBottom(0.75);
$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');
 
// Set title row bold;
$objPHPExcel->getActiveSheet()->getStyle('A12:X12')->getFont()->setBold(true);
// Set fills
$objPHPExcel->getActiveSheet()->getStyle('A12:X12')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A12:X12')->getFill()->getStartColor()->setARGB('FF808080');

//untuk auto size colomn 
/* $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
*/
 
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
 
$sharedStyle1 = new PHPExcel_Style();
$sharedStyle2 = new PHPExcel_Style();
 
$sharedStyle1->applyFromArray(
 array('borders' => array(
 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
 'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
 'right' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM),
 'left' => array('style' => PHPExcel_Style_Border::BORDER_MEDIUM)
 ),
 ));
 
$objPHPExcel->getActiveSheet()->setSharedStyle($sharedStyle1, "A12:X$nox");
 
// Set style for header row using alternative method
$objPHPExcel->getActiveSheet()->getStyle('A12:X12')->applyFromArray(
 array(
 'font' => array(
 'bold' => true
 ),
 'alignment' => array(
 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
 ),
 'borders' => array(
 'top' => array(
 'style' => PHPExcel_Style_Border::BORDER_THIN
 )
 ),
 'fill' => array(
 'type' => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
 'rotation' => 90,
 'startcolor' => array(
 'argb' => 'FFA0A0A0'
 ),
 'endcolor' => array(
 'argb' => 'FFFFFFFF'
 )
 )
 )
);

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("content-disposition: attachment;filename=Lap_Regmal1_$datetime.xlsx");
header('Cache-Control: max-age=0'); 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();
$objWriter->save('php://output');
exit;
<?php
$lprop = mysqli_query($dbconn,"select id_propinsi,nama_propinsi from mst_propinsi");

$jenisQuery = mysqli_query($dbconn,"SELECT id_jns_fasyankes AS id,nm_jns_fasyankes AS jenis FROM mst_jns_fasyankes");
$milikQuery = mysqli_query($dbconn,"SELECT id_kepemilikan AS id,nm_kepemilikan AS kepemilikan FROM mst_kepemilikan_faskes");
?>
<div class="col-lg-12">
    <div class="panel">
        <div class="panel panel-primary">
            <div class="panel-heading">
               <i class="fa fa-hospital-o" aria-hidden="true"></i>
&nbsp;Daftar Fasyankes
            </div>
            <div class="panel-body">
                <div class="row col-lg-12" id="inputForm" style="padding: 10px;">
                    <div class="col-lg-2">&nbsp;</div>
                    <div class="col-lg-10">
                        <form id="fasform" method="post" style="display: none">
                            <input type="hidden" id="idfaskes" name="idfaskes" value="">
<table class="tblInput">
   
    <tbody>
        
        <tr>
        <th>Jenis Fasyankes *</th>
        <th>:
        <select name="jenis" id="jenis">
            
            <option value="">-- Pilih --</option>
            <?php
            while($jnsData=mysqli_fetch_object($jenisQuery)){
                ?>
             <option value="<?php echo $jnsData->id;?>"><?php echo $jnsData->jenis;?></option>
             
                <?php
            }
            ?>
        </select>
        </th>
    </tr>
        
    <tr>
        <th>Propinsi *</th>
        <th>:
        <select name="propinsi" id="propinsi" onchange="getComboKab('kabupaten',this)">
            
            <option value="">-- Pilih --</option>
        <?php
        while($np=mysqli_fetch_object($lprop)){
            ?>
            <option value="<?php echo $np->id_propinsi;?>"><?php echo $np->nama_propinsi;?></option>
            <?php } ?>
        </select>
        </th>
    </tr>
    
     <tr>
        <th>Kab/Kota *</th>
        <th>:
        <select name="kabupaten" id="kabupaten" onchange="getComboKec('kecamatan',this);alert(this)">
            
            <option value="">-- Pilih --</option>
        </select>
        </th>
    </tr>
     
     <tr>
        <th>Kecamatan</th>
        <th>:
        <select name="kecamatan" id="kecamatan">
            
            <option value="">-- Pilih --</option>
        </select>
        </th>
    </tr>
     
     
     <tr>
        <th>Kelurahan</th>
        <th>:
        <select name="kelurahan" id="kelurahan">
            
            <option value="">-- Pilih --</option>
        </select>
        </th>
    </tr>
     
     
     
     <tr>
        <th>Kepemilikan *</th>
        <th>:
        <select name="kepemilikan" id="kepemilikan">
            
            <option value="">-- Pilih --</option>
            <?php
            while($dataMilik=mysqli_fetch_object($milikQuery)){?>
            <option value="<?php echo $dataMilik->id;?>"><?php echo $dataMilik->kepemilikan;?></option>
            <?php } ?>
        </select>
        </th>
    </tr>
    <tr>
        <th>Kode Fasyankes *</th>
    <th>: <input type="text" name="kodefaskes" size="20" id="kodefaskes" maxlength="11"></th>
    </tr>
     <tr>
        <th>Nama Fasyankes *</th>
    <th>: <input type="text" name="namafaskes" size="40" id="namafaskes"></th>
    </tr>
     <tr>
        <th>&nbsp;</th>
        <th>
            <button id="editdata" class="btn btn-primary btn-xs" style="display: none">Edit</button>&nbsp;
            <button id="newdata" class="btn btn-primary btn-xs">Simpan</button>
            
        </th>
     </tr>
    </tbody>
</table>
                        </form>
                </div>
         
                </div>
                <div class="row col-lg-12" style="height: 350px;overflow-y: scroll;overflow-x: scroll">
                <table class="tblListData" style="width: 1200px">
                    <thead>
                        <tr><th colspan="10" style="text-align: left">
                            <button class="btn btn-default btn-xs" id="btnAddkab" onclick="showForm('#fasform');validasiFasyankes('#fasform')"><i class="fa fa-plus-circle" aria-hidden="true"></i>
Tambah data</button><i class="fa fa-search" aria-hidden='true'></i><input type="text" id="cari" name="cari" placeholder="Cari Kode/Nama Fasyankes"  value="" onkeyUp="searching(this.value)">
                        </th></tr>
                        <tr>
                        <th style="text-align: center;width: 5%">No.</th>
                        <th style="text-align: center;width: 10%">Kode Fasyankes</th>
                        <th  style="text-align: center;width: 10%">Jenis Fasyankes</th>
                        <th  style="text-align: center;width: 20%">Nama Fasyankes</th>
                        <th  style="text-align: center;width: 10%">Propinsi</th>
                        <th  style="text-align: center;width: 10%">Kabupaten</th>
                        <th  style="text-align: center;width: 10%">Kecamatan</th>
                        <th  style="text-align: center;width: 10%">Kelurahan</th>
                        <th  style="text-align: center;width: 10%">Kepemilikan</th>
                        <th  style="text-align: center;width: 30%">Ubah/Hapus</th>
                        </tr>
                    </thead>
                    <tbody id="datafasyankes"> <!-- ini masuk ke footer, pada document ready. -->
                        
                    </tbody>
                    
  <?php      $mrQ = mysqli_query($dbconn,"SELECT a.`kd_fasyankes`,a.`nama_fasyankes`,b.`nama_kabupaten`,c.`nama_propinsi` FROM mst_fasyankes a
INNER JOIN mst_kabupaten b ON(a.`kabupaten`=b.`id_kabupaten`)
INNER JOIN mst_propinsi c ON(a.`propinsi`=c.`id_propinsi`)");
$row = mysqli_num_rows($mrQ);             

$numrows = "150";
$start="0";


                    
$totalPage = ceil($row/$numrows);



        ?>
        <tfoot>
        <TR><TH colspan="6">
        Halaman : <select name="fpage" id="fpage" onchange="faskesPaging(this.value)">
            <?php for($i=0;$i<$totalPage;$i++){ ?>
            <option value="<?php echo $i;?>"><?php echo $i+1;?></option>
            <?php } ?>
        </select>
   <!-- <ul class="pagination pagination-sm">
                <?php for($i=$minpage;$i<=$maxnum;$i++) { ?> <li><a href="#"><?php echo $i; ?></a></li><?php } ?>
</ul>-->
    </TH>
</TR>
        </tfoot>
                    
                </table>
            </div>
                
            </div>
            <div class="panel-footer text-right">&nbsp;
            
            
            </div>
        </div>
    </div>
    
</div>

<script src="assets/internal/js/unit_faskes.js"></script>

<div class="col-lg-12">
    <div class="panel">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-list fa-fw"></i>&nbsp;Daftar Propinsi
            </div>
            <div class="panel-body">
                <div class="row col-lg-12" id="inputForm" style="padding: 10px;">
                    <div class="col-lg-1">&nbsp;</div>
                    <div class="col-lg-10">
                     <form id="propForm" method="post" action="" style="display: none">
                        
                        
                    <table cellpadding="0" cellspacing="0" border="0" class="tblInput" style="width: 100%">
                        <input type="hidden" name="idprop" id="idprop" value="">
                        <tbody>
                            <tr>
                                <th>Kode Propinsi</th>
                                <th> : <input name="kd_propinsi" id="kd_propinsi" type="text"></th>
                            </tr>
                            <tr>
                                <th>Nama Propinsi</th>
                                <th>: <input name="nama_propinsi" id="nama_propinsi" type="text" size="40"></th>
                            </tr>
                            
                             <!-- <tr>
                                <th>Nama Alias</th>
                                <th>: <input name="alias_propinsi" id="alias_propinsi" type="text" size="40"></th>
                            </tr>-->
                             <tr>
                                <th colspan="2" style="text-align: center">
                                    <button class="btn btn-primary btn-xs" id="addProp">Submit</button>
                                </th>
                             </tr>
                        </tbody>
                    </table>
                     </form>
                </div>
            <div class="col-lg-1">&nbsp;</div>
                </div>
                <div class="row col-lg-12" style="height: 350px;overflow-y: scroll">
                <table class="tblListData">
                    <thead>
                        <tr>
                            <th colspan="4" style="text-align: left">
                                <button class="btn btn-primary btn-xs" id="btnAddProp" onclick="showForm('#propForm');addPropValidation('#propForm')"><i class="fa fa-plus-square" aria-hidden="true"></i>
Tambah data</button>
                                
                            </th>
                        </tr>
                        <tr>
                        <th style="text-align: center;width: 5%">No.</th>
                        <th style="text-align: center;width: 25%">Kode Propinsi</th>
                        <th  style="text-align: center;width: 25%">Nama Propinsi</th>
                        <!-- <th  style="text-align: center;width: 25%">Nama Alias</th> -->
                        <th  style="text-align: center;width: 20%">Edit</th>
                        </tr>
                    </thead>
                    <tbody id="datapropinsi">
                        
                    </tbody>
                </table>
            </div>
                
            </div>
            <div class="panel-footer text-right">&nbsp;
            
            
            </div>
        </div>
    </div>
    
</div>


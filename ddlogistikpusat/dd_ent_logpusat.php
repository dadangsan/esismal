<?php

$lprop = mysqli_query($dbconn,"select id_propinsi,nama_propinsi from mst_propinsi");
$jenisQuery = mysqli_query($dbconn,"SELECT kd_log AS id,nama_log AS jenis FROM mst_logistik order by kd_log");
$milikQuery = mysqli_query($dbconn,"SELECT id_kepemilikan AS id,nm_kepemilikan AS kepemilikan FROM mst_kepemilikan_faskes");

?>
<div class="col-lg-12">
    <div class="panel">
        <div class="panel panel-primary">
            <div class="panel-heading">
               <i class="fa fa-hospital-o" aria-hidden="true"></i>
&nbsp;Logistik Malaria - Level Pusat2</div>
            <div class="panel-body">
                <div class="row col-lg-12" id="inputForm" style="padding: 10px;">
                    <div class="col-lg-2">&nbsp;</div>
                    <div class="col-lg-10">
                        <form id="fasform" method="post" style="display: none">
                            <input type="hidden" id="id_log" name="id_log" value="">
<table class="tblInput">
   
    <tbody>
        
    
     <tr>
        <th>Tahun *</th>
        <th>:
        <select name="tahun" id="tahun">
            
            <option value="">-- Pilih --</option>
            <?php       
		//for($i=date('Y');$i>date('Y')-2;$i--){ $tahun[$i] = $i; 
		for($i=date('Y');$i>=2018;$i--){ 
		//$tahun[$i] = $i; 
		 $sel = ($i == date('Y')) ? 'selected' : '';
		 //$sel = ($i == date('Y')+1) ? '' : '';
		 //echo "<option value=".$i." ".$i.">".date("Y", mktime(0,0,0,0,1,$i))."</option>";			    
		?>
    <OPTION VALUE="<?php echo $i;?>"><?php echo $i;?></OPTION>
    <?php }  ?>	
		
        </select> 
        </th>
    </tr>
    <tr>
        <th>Bulan *</th>
        <th>:
        <select name="bulan" id="bulan">
            
            <option value="">-- Pilih --</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
			<option value="11">11</option>
            <option value="12">12</option>
       </select>
        </th>
    </tr>
    
    <tr>
        <th>Logistik *</th>
        <th>:
        <select name="logistik" id="logistik">
            
            <option value="">-- Pilih --</option>
            <?php
            while($jnsData=mysqli_fetch_object($jenisQuery)){
                ?>
             <option value="<?php echo $jnsData->id;?>"><?php echo $jnsData->jenis;?></option>
             
                <?php
            }
            ?>
        </select>
        </th>
    </tr>
    
    <tr>
        <!-- <th>Kode Fasyankes *</th>-->
        <th>Stok *</th>
    <th>: <input type="text" name="stok" size="20" id="stok" maxlength="11"></th>
    </tr>
     <!-- <tr>
        <th>Nama Fasyankes *</th>
    <th>: <input type="text" name="namafaskes" size="40" id="namafaskes"></th>
    </tr>
     <tr> -->
        <th>&nbsp;</th>
        <th>
            <button id="editdata" class="btn btn-primary btn-xs" style="display: none">Edit</button>&nbsp;
            <button id="newdata" class="btn btn-primary btn-xs">Simpan</button>
            
        </th>
     </tr>
    </tbody>
</table>
                        </form>
                </div>
         
                </div>
                <div class="row col-lg-12" style="height: 350px;overflow-y: scroll;overflow-x: scroll">
                <!-- <table class="tblListData" style="width: 1200px"> -->
                <table class="tblListData">
                    <thead>
                        <tr><th colspan="8" style="text-align: left">
                            <button class="btn btn-default btn-xs" id="btnAddkab" onclick="showForm('#fasform');validasiLogistik('#fasform')"><i class="fa fa-plus-circle" aria-hidden="true"></i>
Tambah data</button><i class="fa fa-search" aria-hidden='true'></i><input type="text" id="cari" name="cari" placeholder="Cari Nama Logistik"  value="" onkeyup="searchFaskes(this)">
                        </th></tr>
                        <tr>
                        <th style="text-align: center;width: 5%">No.</th>
                        <th style="text-align: center;width: 10%">Logistik</th>
                        <th  style="text-align: center;width: 5%">Stok</th>
                        <th  style="text-align: center;width: 5%">Tahun</th>
                        <th  style="text-align: center;width: 5%">Bulan</th>
                                               
                        <th  style="text-align: center;width: 10%">Action</th>
                        </tr>
                    </thead>
                    <!-- <tbody id="datafasyankes"> -->
                    <tbody id="datalogistikpusat">
                        
                    </tbody>
                    
  <?php      
//dadang cek login
if($_SESSION['id_group'] ==2 or $_SESSION['id_group'] ==1 ){
 //$mrQ = mysqli_query($dbconn,"SELECT a.`kd_fasyankes`,a.`nama_fasyankes`,b.`nama_kabupaten`,c.`nama_propinsi` FROM mst_fasyankes a INNER JOIN mst_kabupaten b ON(a.`kabupaten`=b.`id_kabupaten`) INNER JOIN mst_propinsi c ON(a.`propinsi`=c.`id_propinsi`)");
 $mrQ = mysqli_query($dbconn,"SELECT a.`userlog`, a.id_log, a.kd_log, a.bulan, a.tahun, a.stok, a.kd_kab, a.kd_prop, b.nama_log, e.`id_group` FROM  ent_logistik_stok a INNER JOIN mst_logistik b ON (a.kd_log = b.kd_log) INNER JOIN user_account e ON a.`userlog` = e.`username` WHERE e.id_group in ('1','2')");
}else {
	echo "<br/>Anda Tidak Punya Akses ke Halaman ini";
	exit;
}
$row = mysqli_num_rows($mrQ);             
//echo "rows2= ";$row;
$numrows = "150";
$start="0";


                    
$totalPage = ceil($row/$numrows);



        ?>
        <tfoot>
        <TR><TH colspan="8">
        Halaman : <select name="page" id="page" onchange="paging(this)">
            <?php for($i=0;$i<$totalPage;$i++){ ?>
            <option value="<?php echo $i;?>"><?php echo $i+1;?></option>
            <?php } ?>
        </select>
   <!-- <ul class="pagination pagination-sm">
                <?php for($i=$minpage;$i<=$maxnum;$i++) { ?> <li><a href="#"><?php echo $i; ?></a></li><?php } ?>
</ul>-->
    </TH>
</TR>
        </tfoot>
                    
                </table>
            </div>
                
            </div>
            <div class="panel-footer text-right">&nbsp;
            
            
            </div>
        </div>
    </div>
    
</div>
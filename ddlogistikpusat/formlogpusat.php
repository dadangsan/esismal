<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<!-- <title>Basic DataGrid - jQuery EasyUI Demo</title> -->
<!--    <link rel="stylesheet" type="text/css" href="ddlogistikpusat/css/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="ddlogistikpusat/css/icon.css"> 
    	 <script type="text/javascript" src="ddlogistikpusat/js/jquery-1.8.0.min.js"></script> 
	<script type="text/javascript" src="ddlogistikpusat/js/jquery.easyui.min.js"></script>   	-->
<?php
$jenisQuery = mysqli_query($dbconn,"SELECT kd_log AS id,nama_log AS jenis FROM mst_logistik");
?>
<script type="text/javascript">
var url;  
function create(){
	$('#dialog-form').dialog('open').dialog('setTitle','Tambah Data');
	$('#form').form('clear');
}
function save(){
	var kode = $("#kode").val();
	var string = $("#form").serialize();
	if(kode.length==0){
		$.messager.show({
			title:'Info',
			msg:'Maaf, kode tidak boleh kosong',
			timeout:2000,
			showType:'slide'
		});
		$("#kode").focus();
		return false();
	}
	
	$.ajax({
		type	: "POST",
		url		: "simpan.php",
		data	: string,
		success	: function(data){
			$.messager.show({
				title:'Info',
				msg:data, //'Password Tidak Boleh Kosong.',
				timeout:2000,
				showType:'slide'
			});
			$('#datagrid-crud').datagrid('reload');
		}
	});
}

function save2(){
	//var kode = $("#kode").val();
	var tahun = $("#tahun").val();
	var bulan = $("#bulan").val();
	var logistik = $("#logistik").val();
	var stok = $("#stok").val();	
	var string = $("#form").serialize();
	if(tahun.length==0 || bulan.length==0 || logistik.length==0 || stok.length==0 ) {
		$.messager.show({
			title:'Info',
			msg:'Tahun, Bulan, Logistik, Stok   tidak boleh kosong',
			timeout:2000,
			showType:'slide'
		});
		$("#tahun").focus();
		return false();
	}
	
	$.ajax({
		type	: "POST",
		url		: "simpan.php",
		data	: string,
		success	: function(data){
			$.messager.show({
				title:'Info',
				msg:data, //'Password Tidak Boleh Kosong.',
				timeout:2000,
				showType:'slide'
			});
			$('#datagrid-crud').datagrid('reload');
		}
	});
}

function hapus(){  
	var row = $('#datagrid-crud').datagrid('getSelected');  
	if (row){  
		$.messager.confirm('Confirm','Apakah Anda akan menghapus data ini ?',function(r){  
			if (r){  
				$.ajax({
					type	: "POST",
					url		: "hapus.php",
					data	: 'id='+row.kodebarang,
					success	: function(data){
						$.messager.show({
							title:'Info',
							msg:data, //'Password Tidak Boleh Kosong.',
							timeout:2000,
							showType:'slide'
						});
						$('#datagrid-crud').datagrid('reload');
					}
				});  
			}  
		});  
	}  
} 
function update(){
	var row = $('#datagrid-crud').datagrid('getSelected');
	if(row){
		$('#dialog-form').dialog('open').dialog('setTitle','Edit Data');
		$('#form').form('load',row);
	}
}

function fresh(){  
	$('#datagrid-crud').datagrid('reload');
}  
function doSearch(value){  
	$('#datagrid-crud').datagrid('load',{    
        cari:value
    });  
}  
</script>
</head>
<body>
	<!-- <h2>CRUD DataGrid EsayUI</h2> -->
	<div style="margin:10px 0;"></div>
	
	<table id="datagrid-crud" title="Logistik Malaria - Level Pusat" class="easyui-datagrid" style="width:1000px; height: auto;" url="ddlogistikpusat\json.php" toolbar="#tb" pagination="true" rownumbers="true" fitColumns="true" singleSelect="true" collapsible="true">
    <thead>
        <tr>
            <th data-options="field:'nama_log',width:40" sortable="true">Logistik</th>
            <th data-options="field:'stok',width:20" sortable="true">Stok</th>
            <th data-options="field:'tahun',width:20" sortable="true">Tahun</th>
            <th data-options="field:'bulan',width:20,align:'left'" sortable="true">Bulan</th>
            <!-- <th data-options="field:'hargajual',width:80,align:'right'" sortable="true">Harga Jual</th>
            <th data-options="field:'stokawal',width:80,align:'center'">Stok</th> -->
        </tr>
    </thead>
	</table>
    <div id="tb" style="padding:2px;height:30px;">
		<div style="float:left;">
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="create()">Tambah</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="update()">Edit</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="hapus()">Hapus</a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="fresh()">Refresh</a>
		</div>
		<div style="float:right;">
        	Pencarian <input id="cari" class="easyui-searchbox" data-options="prompt:'Cari Nama Logistik..',searcher:doSearch" style="width:200px"></input> 
		</div>
	</div>
    
<!-- Dialog Form -->
<div id="dialog-form" class="easyui-dialog" style="width:400px; height:300px; padding: 10px 20px" closed="true" buttons="#dialog-buttons">
	<form id="form" method="post" novalidate>
		<div class="form-item">
			<br/> 
			<table cellspacing="0" cellpadding="0">
			  <col width="64">
			  <col width="16">
			  <col width="134">
			  <col width="81">
			  <col width="12">
			  <col width="64">
			  <tr>
			    <td width="60" style="font-size:12px"><label for="type"><strong>Tahun</strong></label></td>
			    <td width="16"><div align="center">:</div></td>
			    <td width="30" style="font-size:12px"><select name="tahun" id="tahun">
			      <!-- <option value="" selected="selected">-- Pilih --</option> -->
			      <?php       
		//for($i=date('Y');$i>date('Y')-2;$i--){ $tahun[$i] = $i; 
		for($i=date('Y');$i>2015;$i--){ 
		//$tahun[$i] = $i; 
		 $sel = ($i == date('Y')) ? 'selected' : '';
		 //$sel = ($i == date('Y')+1) ? '' : '';
		 //echo "<option value=".$i." ".$i.">".date("Y", mktime(0,0,0,0,1,$i))."</option>";			    
		?>
			      <option value="<?php echo $i;?>"><?php echo $i;?></option>
			      <?php }  ?>
		        </select></td>
			    <td width="60" style="font-size:12px"><div align="right"><strong>Bulan</strong></div></td>
			    <td width="14"><div align="center">:</div></td>
			    <td width="60" style="font-size:12px"><select name="bulan" id="bulan">
			      <!-- <option value="">-- Pilih --</option> -->
			      <option value="1">1</option>
			      <option value="2">2</option>
			      <option value="3">3</option>
			      <option value="4">4</option>
			      <option value="5">5</option>
			      <option value="6">6</option>
			      <option value="7">7</option>
			      <option value="8">8</option>
			      <option value="9">9</option>
			      <option value="10">10</option>
			      <option value="11">11</option>
			      <option value="12">12</option>
		        </select></td>
		      </tr>
			  <tr>
			    <td style="font-size:12px"><strong>Logistik</strong></td>
			    <td><div align="center">:</div></td>
			    <td width="30" style="font-size:12px"><select name="nama_log" id="nama_log">
			      <option value="">-Pilih-</option>
			      <?php
            while($jnsData=mysqli_fetch_object($jenisQuery)){
                ?>
			      <option value="<?php echo $jnsData->id;?>"><?php echo $jnsData->jenis;?></option>
			      <?php
            }
            ?>
		        </select></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
			    <td style="font-size:12px"><strong>Stok</strong></td>
			    <td><div align="center">:</div></td>
			    <td width="30" style="font-size:12px"><input type="text" name="stok" size="7"  id="stok" maxlength="7"></td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
		  </table>
			<br/>
            
          
    
            <br/><label for="type">Kode Barang2</label>
			<br />
			<input type="text" name="kodebarang" id="kode" class="easyui-validatebox" required="true" size="20" maxlength="20" />
	  </div>
		<div class="form-item">
			<label for="nama">Nama Barang2</label>
			<br />
			<input type="text" name="namabarang" id="nama" class="easyui-validatebox" required="true" size="53" maxlength="50" />
		</div>
		<div class="form-item">
			<label for="hargabeli">Harga Beli2</label>
			<br />
			<input class="easyui-numberbox" name="hargabeli" id="hargabeli" data-options="precision:2,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. '" class="easyui-validatebox" required="true" />
		</div>
        <div class="form-item">
			<label for="hargajual">Harga Jual2</label>
			
			<br />
			<input class="easyui-numberbox" name="hargajual" id="hargajual" data-options="precision:2,groupSeparator:'.',decimalSeparator:',',prefix:'Rp. '" class="easyui-validatebox" required="true" />
		</div>
        <div class="form-item">
			<label for="stok">Stok Awal2</label>
			<br />
			<input class="easyui-numberbox" name="stokawal" id="stok" class="easyui-validatebox" required="true" />
		</div>
	</form>
</div>

<!-- Dialog Button -->
<div id="dialog-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save()">Simpan</a> 
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save2()">Simpan2</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:jQuery('#dialog-form').dialog('close')">Batal</a>
</div>
</body>
</html>
<?php
session_start();
require_once("config/database-connect.php");
$p = (object)$_POST;
$s = (object)$_SESSION;

if($p->mode=='add'){
    $insSQL = "INSERT INTO menu(seqno,id_parent,caption,url,`status`,isparent) SELECT MAX(seqno)+1,'$p->mainmenu','$p->label','$p->url','$p->aktif','0' FROM menu where id_parent='$p->mainmenu'";

$newMenu = mysqli_query($dbconn,$insSQL);
if($newMenu){
    if($p->mainmenu=='0'){
        $json['status']='success';
        $json['msg'] = 'Menu Berhasil ditambahkan';
    }else{
        $sqlUpd = "UPDATE menu SET isparent='1' WHERE id_menu='$p->mainmenu'";
        $updParent = mysqli_query($dbconn,$sqlUpd);
        if($updParent){
            $json['status']='success';
            $json['msg'] = 'Menu Berhasil ditambahkan';
        }else{
            $json['status']='error';
            $json['msg'] = "Update Parent Menu Gagal!!";
        }
}


}else{
        $json['status']='error';
        $json['msg'] = 'Menu Gagal Ditambahkan';
    }
}elseif($p->mode=='del'){
    $delSTR = "DELETE FROM menu where id_menu='$p->id_menu'";
    $delete = mysqli_query($dbconn,$delSTR);
    if($delete){
        $ck =mysqli_query($dbconn,"select * from menu where id_parent='$p->idparent'");
        if(!$f=mysqli_fetch_array($ck)){
            $upd = mysqli_query($dbconn,"update menu SET isparent='0' WHERE id_menu='$p->idparent'");
        }
        $json['status']='success';
        $json['msg']='Data Telah dihapus';
    }
}elseif($p->mode=='getdata'){
    $getSTR = "SELECT id_menu,id_parent,status,caption as label,url from menu where id_menu='$p->idmenu'";
    $mQuery = mysqli_query($dbconn,$getSTR);
    $mdata = mysqli_fetch_array($mQuery);
    if($mdata){
    $json['label'] = $mdata['label'];
    $json['id_menu'] = $mdata['id_menu'];
    $json['id_parent']=$mdata['id_parent'];
    $json['url'] = $mdata['url'];
    $json['aktif'] = $mdata['status'];
    $json['status']='success';
    }else{
        $json['status']='error';
        $json['msg']="Data Tidak ditemukan";
    }
}elseif($p->mode=='edit'){
    $updSTR = "UPDATE menu SET id_parent='$p->mainmenu', status='$p->aktif',caption='$p->label', url='$p->url' where id_menu='$p->idmenu'";
    $mUpdate = mysqli_query($dbconn,$updSTR);
    if($mUpdate){
    if($p->mainmenu=='0'){
        $json['status']='success';
        $json['msg'] = 'Menu Berhasil Diedit';
    }else{
        $sqlUpd = "UPDATE menu SET isparent='1' WHERE id_menu='$p->mainmenu'";
        $updParent = mysqli_query($dbconn,$sqlUpd);
        if($updParent){
            $json['status']='success';
            $json['msg'] = 'Menu Berhasil Di Edit';
        }else{
            $json['status']='error';
            $json['msg'] = "Update Parent Menu Gagal!!";
        }
}


}else{
        $json['status']='error';
        $json['msg'] = 'Menu Gagal Di edit';
    }
    
}elseif($p->mode=='upmenu'){
   $selM = mysqli_query($dbconn,"select id_menu,seqno,id_parent from menu where id_menu='$p->idmenu'");
   $mQ = mysqli_fetch_array($selM);
   $seqNum = $mQ['seqno'];
   $idparent = $mQ['id_parent'];
   $seqNum0 = $seqNum-1;
    $chSeq = mysqli_query($dbconn,"UPDATE menu SET seqno='$seqNum' WHERE seqno='$seqNum0' && id_parent='$idparent'");
       
    if($chSeq){
       $upSTR = "UPDATE menu SET seqno=seqno-1 WHERE id_menu='$p->idmenu' ";
    $ex = mysqli_query($dbconn,$upSTR); 
        if($ex){
        $json['status']='success';
        }else{
            $json['status']='error';
        }
    }else{
        $json['status']='error';
    }
}elseif($p->mode=='downmenu'){
    $selM = mysqli_query($dbconn,"select id_menu,seqno,id_parent from menu where id_menu='$p->idmenu'");
   $mQ = mysqli_fetch_array($selM);
   $seqNum = $mQ['seqno'];
   $idparent = $mQ['id_parent'];
   $seqNum0 = $seqNum+1;
    $chSeq = mysqli_query($dbconn,"UPDATE menu SET seqno='$seqNum' WHERE seqno='$seqNum0' && id_parent='$idparent'");
       
    if($chSeq){
       $upSTR = "UPDATE menu SET seqno=seqno+1 WHERE id_menu='$p->idmenu' ";
    $ex = mysqli_query($dbconn,$upSTR); 
        if($ex){
        $json['status']='success';
        }else{
            $json['status']='error';
        }
    }else{
        $json['status']='error';
    }
}

    
    echo json_encode($json);
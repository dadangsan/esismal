function userFormValidation(fid){
    //alert(fid);
    var formid = fid;
    $(formid).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
            username:{
                required:true
            }
        },
        message:{
            username:{
                required: "username wajib diisi"
            }
        },
         submitHandler:function(form){
		alert('lolos');
         }
        
    })
    
}
/* validasi kabupaten
 * validasi penambahan kab/kota
 * 13/08/2017 by harmi
 * */

function validasi(id){
            $(id).validate({
                        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
            kodekab:{
                required:true,
                remote:{
                    url:'validasi/form_validation.php',
                    type:"post",
                    data:{
                        'validasi':'kodekab'
                    }
                },
                minlength:4
            },
            propinsi:{
                required:true
            },
            namakab:{
                required:true
            }
        },
        messages:{
            kodekab:{
                required: "Kode Kabupaten wajib diisi",
                remote:"Kode ini sudah digunakan",
                minlength:"kode Kab/Kota 4 digit"
            },
            propinsi:{
                required:"Propinsi wajib diisi"
            },
            namakab:{
                required:"Nama Kabupaten wajib diisi"
            }
        },
         submitHandler:function(form){
            var idkab = $('#idkab').val();
            
            if (idkab=='') {
                //code
                var mode = 'add';
            }else{
                var mode = 'edit';
            }
		$.ajax({
            url:"kabupaten_action.php",
            type:"post",
            dataType:"json",
            data:{
                'mode':mode,
                'propinsi':$('#propinsi').val(),
                'kodekab':$('#kodekab').val(),
                'namakab':$('#namakab').val()
            },
            success:function(jdata){
                if (jdata.status=='success') {
                    //code
                     $('#kabform').slideUp('slow',function(){ 
                     $('#datakabupaten').load('data/datatable_kabupaten.php');
                     });
                     alert(jdata.msg);
                      window.location.reload();
                }else{
                    alert(jdata.msg)
                }
            }
            })
         }
            })
}


/* validasi edit kabupaten
 * create 13/08/2017 by harmi
 * */

function validasiEdit(id,idkab){
            $(id).validate({
                        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
            kodekab:{
                required:true,
                remote:{
                    url:'validasi/form_validation.php',
                    type:"post",
                    data:{
                        'validasi':'editKodekab',
                        'id':idkab
                    }
                },
                minlength:4
            },
            propinsi:{
                required:true
            },
            namakab:{
                required:true
            }
        },
        messages:{
            kodekab:{
                required: "Kode Kabupaten wajib diisi",
                remote:"Kode ini sudah digunakan",
                minlength:"kode Kab/Kota 4 digit"
            },
            propinsi:{
                required:"Propinsi wajib diisi"
            },
            namakab:{
                required:"Nama Kabupaten wajib diisi"
            }
        },
         submitHandler:function(form){
		$.ajax({
            url:"kabupaten_action.php",
            type:"post",
            dataType:"json",
            data:{
                'mode':'edit',
                'propinsi':$('#propinsi').val(),
                'kodekab':$('#kodekab').val(),
                'namakab':$('#namakab').val(),
                'id':$('#idkab').val()
            },
            success:function(jdata){
                if (jdata.status=='success') {
                    //code
                     $('#kabform').slideUp('slow',function(){ 
                     $('#datakabupaten').load('data/datatable_kabupaten.php');
                     });
                     alert(jdata.msg);
                      window.location.reload();
                }else{
                    alert(jdata.msg)
                     
                }
            }
            })
         }
            })
}

/* validasi propinsi add */

function addPropValidation(id) {
    //code
     $(id).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          nama_propinsi:{
            required:true
          },
          kd_propinsi:{
            required:true,
             remote:{
                    url:'validasi/form_validation.php',
                    type:"post",
                    data:{
                        'validasi':'kodepropinsi'
                    }
                }
          }
        },
        messages:{
            nama_propinsi:{
                required:"Nama Propinsi Wajib dipilih"
            },
            
            kd_propinsi:{
                required:"Kode Propinsi Wajib diisi",
                remote:"Kode Propinsi ini sudah ada"
            }
            
        },
        submitHandler:function(form){
             $.ajax({
        url:"add_data_wilayah.php",
        type:"post",
        dataType:"json",
        data:{
            'kode':$('#kd_propinsi').val(),
            'nama':$('#nama_propinsi').val(),
            'mode':'add',
            'idprop':''
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                
                 $('#datapropinsi').load('data/datatable_propinsi.php');
            }
            
            alert(jdata.msg);
             window.location.reload();
        },
        error:function(){
            alert("internal server Error");
        }
    })
        }
    })
    
    
}


/* validasi propinsi edit */

function editPropValidation(id,idprop) {
    //code
     $(id).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          nama_propinsi:{
            required:true
          },
          kd_propinsi:{
            required:true,
             remote:{
                    url:'validasi/form_validation.php',
                    type:"post",
                    data:{
                        'validasi':'editpropinsi',
                        'id_propinsi':idprop
                    }
                }
          }
        },
        messages:{
            nama_propinsi:{
                required:"Nama Propinsi Wajib dipilih"
            },
            
            kd_propinsi:{
                required:"Kode Propinsi Wajib diisi",
                remote:"Kode Propinsi ini sudah ada"
            }
            
        },
        submitHandler:function(form){
             $.ajax({
        url:"add_data_wilayah.php",
        type:"post",
        dataType:"json",
        data:{
            'kode':$('#kd_propinsi').val(),
            'nama':$('#nama_propinsi').val(),
            'mode':'edit',
            'idprop':$('#idprop').val()
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                
                 $('#datapropinsi').load('data/datatable_propinsi.php');
            }
            
            alert(jdata.msg);
             window.location.reload();
        },
        error:function(){
            alert("internal server Error");
        }
    })
        }
    })
    
    
}


/* validasi penambahan fasyankes */

function validasiFasyankes(id){
    $(id).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },
          kabupaten:{
            required:true
          },
          jenis:{
            required:true
          },
          kepemilikan:{
            required:true
          },
          
          kodefaskes:{
            required:true,
             remote:{
                    url:'validasi/form_validation.php',
                    type:"post",
                    data:{
                        'validasi':'kodefaskes'
                    }
                }
          },
          namafaskes:{
            required:true
          }
        },
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },
            kabupaten:{
                required:"Kabupaten Wajib dipilih"
            },
            kodefaskes:{
                required:"Kode Fasyankes Wajib diisi",
                remote:"Kode Fasyankes ini sudah ada"
            },
            namafaskes:{
                required:"Nama Fasyankes Wajib diisi"
            },
            jenis:{
                required:"Jenis Fasyankes Wajib diisi"
            },
            kepemilikan:{
                required:"Kepemilikan Fasyankes Wajib diisi"
            }
            
        },
        submitHandler:function(form){
             $.ajax({
        url:"fasyankes_action.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),
            'kabupaten':$('#kabupaten').val(),
            'kodefaskes':$('#kodefaskes').val(),
            'namafaskes':$('#namafaskes').val(),
            'jenis':$('#jenis').val(),
            'kepemilikan':$('#kepemilikan').val(),
            'kodekecamatan':$('#kecamatan').val(),
            'kodekelurahan':$('#kelurahan').val(),
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                
                 $('#datafasyankes').load('data/datatable_fasyankes.php');
            }
            
            alert(jdata.msg);
            window.location.reload();
        },
        error:function(){
            alert("internal server Error");
        }
    })
        }
    })
}




// validasi fasyankes edit

function validasiFasyankesEdit(id,kdfaskes){
    
    $(id).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },
          kabupaten:{
            required:true
          },
          jenis:{
            required:true
          },
          kepemilikan:{
            required:true
          },
          
          kodefaskes:{
            required:true,
             remote:{
                    url:'validasi/form_validation.php',
                    type:"post",
                    data:{
                        'validasi':'kodefaskes',
                        'idfaskes':kdfaskes
                    }
                }
          },
          namafaskes:{
            required:true
          }
        },
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },
            kabupaten:{
                required:"Kabupaten Wajib dipilih"
            },
            kodefaskes:{
                required:"Kode Fasyankes Wajib diisi",
                remote:"Kode Fasyankes ini sudah ada"
            },
            namafaskes:{
                required:"Nama Fasyankes Wajib diisi"
            },
            jenis:{
                required:"Jenis Fasyankes Wajib diisi"
            },
            kepemilikan:{
                required:"Kepemilikan Fasyankes Wajib diisi"
            }
            
        },
        submitHandler:function(form){
             $.ajax({
        url:"fasyankes_action.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),
            'kabupaten':$('#kabupaten').val(),
            'kodefaskes':$('#kodefaskes').val(),
            'namafaskes':$('#namafaskes').val(),
            'jenis':$('#jenis').val(),
            'kepemilikan':$('#kepemilikan').val(),
            'kodekecamatan':$('#kecamatan').val(),
            'kodekelurahan':$('#kelurahan').val(),
            'idfaskes':$('#idfaskes').val(),
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                
                 $('#datafasyankes').load('data/datatable_fasyankes.php');
            }
            
            alert(jdata.msg);
             window.location.reload();
        },
        error:function(){
            alert("internal server Error");
        }
    })
        }
    })
}
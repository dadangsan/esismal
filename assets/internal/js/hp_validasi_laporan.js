function filterValidation(idform) {
    //code
   $(idform).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },
         
          bulan1:{ //bulan
            required:true
          },
         
          tahun:{ //tahun
            required:true
          }     
        },
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },
            
			bulan1:{
                required:"Bulan/Triwulan Wajib dipilih"
            },			
           
			tahun:{
                required:"Tahun Wajib diisi"
            }
        },
        submitHandler:function(form){
            var irl = $('#url').val();
            //var p = $('#idata').val();
           // alert(part+p);
             $('#databody').load(irl,{
                'propinsi':$('#propinsi').val(),
                'kabupaten':$('#kabupaten').val(),
                'tahun':$('#tahun').val(),
                'bulan1':$('#bulan1').val(),
                'faskes':$('#fasyankes').val(),
                'level':$("input[name='level']:checked").val(),
                'periode':$("input[name='periode']:checked").val()
                })
        }
    })
}


function getExcel() {
    //code
  
            var irl = $('#url').val();
            var propinsi = $('#propinsi').val();
            var kabupaten = $('#kabupaten').val();
            var tahun = $('#tahun').val();
            var bulan1 = $('#bulan1').val();
            var faskes =$('#fasyankes').val();
            var level = $("input[name='level']:checked").val();
            var periode = $("input[name='periode']:checked").val();
            
            var par = 'propinsi='+propinsi;
            par += '&kabupaten='+kabupaten;
            par += '&tahun='+tahun;
            par += '&bulan1='+bulan1;
            par += '&faskes='+faskes.join(',');
            par += '&level='+level;
            par += '&periode='+periode;
            par += '&export=excel';
            
            var target = irl.replace('.php','_excel.php')+'?'+par;
            document.location = target;
        }
   


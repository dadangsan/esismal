function searchFaskes(uid){

   // $('#datafasyankes').load('data/datatable_logistik.php',{
	    $('#datalogistik').load('data/datatable_logistik.php',{
        'mode':'search',
        'search':$(uid).val()
        })
}

function paging(pagenum) {
    //code
    
     $('#datalogistik').load('data/datatable_logistik.php',{
        'mode':'paging',
        'page':$(pagenum).val()
        });
}

//function editFaskes(kd_fasyankes) {
	function editFaskes(id_log) {
    $.ajax({
        url:"logistik_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'getdata',
            //'id':id
			'id_log':id_log
        },
        success:function(jdata){
            //alert(jdata.kd_fasyankes);
            $('#id_log').val(jdata.id_log);
              $('#fasform').slideDown('slow');
              validasiLogistikEdit('#fasform',jdata.id_log);
            $('#propinsi').val(jdata.propinsi);
            $('#kabupaten').html(jdata.kabupaten);
			$('#kabupaten').val(jdata.kabfile);
            //$('#kecamatan').html(jdata.kecamatan);
            //$('#kelurahan').html(jdata.kelurahan);
            $('#tahun').val(jdata.tahun);
            $('#bulan').val(jdata.bulan);
			$('#logistik').val(jdata.logistik); 
			$('#stok').val(jdata.stok);						
            //$('#jenis').val(jdata.jns_fasyankes);
            //$('#kepemilikan').val(jdata.id_kepemilikan);
			
			//dadang, tambahan coding 13nov2017
			//$('#editdata').show();
			//$('#newdata').hide();
        
       
        },
        error:function(){
            alert("Internal server Error88");
        }
    })
    //code	
}

//function deleteFaskes(id){ //dadang sukses
	function deleteFaskes(id_log){
    var conf = confirm("Apakah yakin anda akan menghapus data Logistik ini?");
    if (conf) {
        //code
    
    $.ajax({
        url:"logistik_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'del',
            'id_log':id_log
			//'kd_fasyankes':kd_fasyankes
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                alert('Data Logistik berhasil dihapus');
                 $('#datalogistik').load('data/datatable_logistik.php');
            }else{
                alert(jdata.msg);
            }
        },
        error:function(){
            alert("internal server error");
        }
    });
    
    }
}
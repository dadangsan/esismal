function propinsi(){
    
            $.ajax({
                url:"add_data_wilayah.php",
                type:"Post",
                dataType:"json",
                data:{
                    "idProp":$('#idprop').val(),
                    "level":"propinsi",
                    "kode":$('#kd_propinsi').val(),
                    "nama":$('#nama_propinsi').val()
                   // "alias":$('#alias_propinsi').val()
                },
                success:function(jdata){
                    if (jdata.status=='success') {
                        //code
                        alert('data berhasil disimpan');
                         $('#datapropinsi').load('data/datatable_propinsi.php');
                         $('#inputForm').slideUp('slow');
                    }else{
                        alert("data gagal disimpan");
                    }
                },
                error:function(){
                    alert('ada Kesalahan sistem')
                }
            });
            
}

function editPropinsi(idprop){
   $.ajax({
                url:"add_data_wilayah.php",
                type:"Post",
                dataType:"json",
                data:{
                    "id":idprop,
                    "mode":"getData"
                },
                success:function(jdata){
                    if (jdata.status=='success') {
                        //code
                        $('#idprop').val(jdata.id_propinsi);
                        $('#kd_propinsi').val(jdata.id_propinsi);
                        $('#nama_propinsi').val(jdata.nama_propinsi);
                        //$('#alias_propinsi').val(jdata.alias);
                        editPropValidation('#propForm',jdata.id_propinsi);
                        $('#propForm').slideDown('slow');
                        ///location.reload();
                    }else{
                        alert("data tidak ditemukan");
                    }
                },
                error:function(){
                    alert('ada Kesalahan sistem')
                }
            });
}

function deletePropinsi(idprop){
    var tanya = confirm("Apakah Data ini akan Dihapus");
    if (tanya) {
        //code
    
      $.ajax({
                url:"add_data_wilayah.php",
                type:"Post",
                dataType:"json",
                data:{
                    "idProp":idprop,
                    "mode":"delete"
                },
                success:function(jdata){
                    if (jdata.status=='success') {
                        //code
                        alert("data berhasil dihapus");
                         $('#datapropinsi').load('data/datatable_propinsi.php');
                    }else{
                        alert("data tidak ditemukan");
                    }
                },
                error:function(){
                    alert('ada Kesalahan sistem')
                }
            });
    }
}



/* manajemen data kabupaten/kota */
/* 12/08/2017 by harmi */
function searchKab(keyword){
            $('#datakabupaten').load('data/datatable_kabupaten.php',{
                        'mode':'search',
                        'keyword':keyword
                        });
}
function editKabupaten(idkab){
            $.ajax({
                        url:"kabupaten_action.php",
                        type:"post",
                        dataType:"json",
                        data:{
                                    'mode':'getKab',
                                    'idkab':idkab
                        },success:function(jdata){
                                    $('#idkab').val(jdata.id_kabupaten);
                                    $('#propinsi').val(jdata.id_propinsi);
                                    $('#kodekab').val(jdata.id_kabupaten);
                                    $('#namakab').val(jdata.nama_kabupaten);
                                    $('#kabform').slideDown('slow');
                                    $('#editdata').show();
                                    $('#newdata').hide();
                        },error:function(){
                                    alert('Internal Server ERROR');
                        }
            })
}

function deleteKabupaten(idkab){
           var conf = confirm("Apakah anda akan menghapus data ini?");
           if (conf) {
            //code
           
            $.ajax({
                        url:"kabupaten_action.php",
                        type:"post",
                        dataType:"json",
                        data:{
                                    'id':idkab,
                                    'mode':'del'
                        },success:function(jdata){
                              $('#datakabupaten').load('data/datatable_kabupaten.php');
                              alert(jdata.msg);
                        },error:function(){
                                    alert("internal server error");
                        }
            })
           }
}
function checkGroup(id){
    $('.menu').prop('checked',false);
    $.ajax({
        url:"akses_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'checkdata',
            'gid':$(id).val()
        },
        success:function(jdata){
        $.each(jdata.id_menu, function(){
            $('#menu_'+this).prop('checked',true)
            });
        }
    })
}


function editAkses(id){
    
    $('.menu').prop('checked',false);
    $.ajax({
        url:"akses_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'checkdata',
            'gid':id
        },
        success:function(jdata){
        $.each(jdata.id_menu, function(){
            $('#menu_'+this).prop('checked',true)
            });
        $('#gid').val(jdata.id_group);
        $('#formMenu').slideDown('slow');
        }
    })
}

function deleteAkses(id){
    var conf = confirm("Apakah Anda akan Menghapus akses?");
    if (conf) {
        //code
    
    $.ajax({
        url:"akses_action.php",
        type:"post",
        dataType:"json",
        data:{
            'gid':id,
            'mode':'del'
        },
        success:function(jdata){
             $('#listdata').load('data/datatable_akses.php');
        alert(jdata.msg)
        },
        error:function(){
            alert('Internal Server Error');
        }
    })
    }
}

function checkall(){
    $('.menu').prop('checked',true);
}
function uncheck(){
    $('.menu').prop('checked',false);
}



function addNewAkses(){
    if ($('#gid').val()=='') {
        //code
        alert('Nama Group harus dipilih');
    }else{
        
   var  menuArray = [];
   $(".menu:checked").each(function() {
		menuArray.push($(this).val());
	});
 
    $.ajax({
        url:"akses_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'add',
            'menu':menuArray,
            'gid':$('#gid').val()
            
        },success:function(jdata){
            if (jdata.status=='success') {
                $('#formMenu').slideUp('slow',function(){
                 $('#listdata').load('data/datatable_akses.php');
                });
           }
        alert(jdata.msg);
        },
        error:function(){
            alert("internal server error");
        }
        
        
    })
    }
}
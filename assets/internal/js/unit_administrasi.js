
function getComboKab(id,idprop){
 var idcombo = "#"+id;
 var prop = $(idprop).val();
 
 $.ajax({
    url:"data/data_administratif.php",
    type:"post",
    dataType:"json",
    data:{
        'que':'kab',
        'idprop':prop,
        'kodeid':''
    },
    success:function(jdata){
        $(idcombo).html(jdata.list);
    }
 })
}


function getComboFaskes(id,thiskab){
    var idcombo = '#'+id;
    var idkab = $(thiskab).val();
    $.ajax({
        url:"data/data_administratif.php",
        type:"post",
        dataType:"json",
        data:{
            'que':'faskes',
            'idkab':idkab,
            'kodeid':''
        },
        success:function(jdata){
            $(idcombo).html(jdata.list);
        }
    })
}

function getComboKec(id,idkab) {
    //code
    var idcombo = '#'+id;
    $.ajax({
      url:"data/data_administratif.php",
      type:"post",
      dataType:"json",
      data:{
         'que':'kec',
         'idkab':idkab,
         'kodeid':''
      },
      success:function(jdata){
         $(idcombo).html(jdata.list);
      }
    })
}


function getComboKel(id,idkec) {
    //code
    var idcombo = '#'+id;
    $.ajax({
      url:"data/data_administratif.php",
      type:"post",
      dataType:"json",
      data:{
         'que':'kel',
         'idkec':idkec,
         'kodeid':''
      },
      success:function(jdata){
         $(idcombo).html(jdata.list);
      }
    })
}
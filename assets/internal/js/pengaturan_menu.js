/* pengaturan menu by Harmi Prasetyo @ 2017 */

function addNewMenu(){
    $.ajax({
        url:"menu_action.php",
        type:"post",
        dataType:"json",
        data:{
        'mode' : 'add',
         'label':$('#labelmenu').val(),
         'url':$('#url').val(),
         'mainmenu':$('#mainmenu').val(),
         'aktif':$("input[name=status]").val()
        },
        success:function(jdata){
           if (jdata.status=='success') {
            //code
            $('#formMenu').slideUp('slow',function(){
                 $('#listdata').load('data/datatable_menu.php');
                });
           }
        alert(jdata.msg);
            
        },
        error:function(){
            alert(jdata.msg);
        }
    })
}

function editMenu(id){
    var idmenu = id;
    $.ajax({
        url:"menu_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'getdata',
            'idmenu':idmenu
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                $('#labelmenu').val(jdata.label);
                $('#url').val(jdata.url);
                $('#mainmenu').val(jdata.id_parent);
                $('input[name=status]').val(jdata.aktif);
                $('#idmenu').val(jdata.id_menu);
                $('#formMenu').slideDown('slow');
                $('#admenu').hide();
                $('#edmenu').show();
            }
        }
    })
}

function updateMenu(){
    $.ajax({
        url:"menu_action.php",
        type:"post",
        dataType:"json",
        data:{
        'mode' : 'edit',
         'label':$('#labelmenu').val(),
         'url':$('#url').val(),
         'mainmenu':$('#mainmenu').val(),
         'aktif':$("input[name=status]").val(),
         'idmenu':$('#idmenu').val()
        },
        success:function(jdata){
           if (jdata.status=='success') {
            //code
            $('#formMenu').slideUp('slow',function(){
                 $('#listdata').load('data/datatable_menu.php');
                });
           }
        alert(jdata.msg);
            
        },
        error:function(){
            alert(jdata.msg);
        }
    })
}
function delMenu(id,idparent){
    var idmenu = id;
    var conf = confirm("Apakah Menu ini akan dihapus?");
    if (conf) {
        //code
    
    $.ajax({
        url:"menu_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode' : 'del',
            'id_menu' : idmenu,
            'idparent':idparent
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                alert(jdata.msg)
                $('#listdata').load('data/datatable_menu.php');
            }else{
                alert(jdata.msg)
            }
        },
        error:function(){
            alert('Internal Server Error');
        }
    })
    }

}

function upMenu(idmenu) {
    //code
    //var id = idmenu;
    $.ajax({
        url:"menu_action.php",
        type:"post",
        dataType:"json",
        data:{
            "idmenu":idmenu,
            "mode":"upmenu"
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                 $('#listdata').load('data/datatable_menu.php');
            }
        }
    })
    
}

function downMenu(idmenu) {
    //code
    //var id = idmenu;
    $.ajax({
        url:"menu_action.php",
        type:"post",
        dataType:"json",
        data:{
            "idmenu":idmenu,
            "mode":"downmenu"
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                 $('#listdata').load('data/datatable_menu.php');
            }
        }
    })
    
}


/* Blok pengaturan Group */



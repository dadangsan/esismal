
/* validasi penambahan Logistik Stok */

//function validasiFasyankes(id){
/* function validasiLogistik(id){
    $(id).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },
          kabupaten:{
            required:true
          },
          bulan:{ //bulan
            required:true
          },
		  jenis:{ //nama_logistik
            required:true
          },
          kepemilikan:{ //tahun
            required:true
          },
          
          kodefaskes:{
            required:true,
              remote:{
                    url:'validasi/form_validation.php',
                    type:"post",
                    data:{
                        'validasi':'kodefaskes'
                    }
                } 
          },
          namafaskes:{
            required:true
          }
        },
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },
            kabupaten:{
                required:"Kabupaten Wajib dipilih"
            },
            kodefaskes:{ //stok
                required:"Stok Wajib diisi; kosong=0",
                //remote:"Kode Fasyankes ini sudah ada"
            },
            namafaskes:{
                required:"Nama Fasyankes Wajib diisi"
            },
			bulan:{
                required:"Bulan Wajib diisi"
            },
            jenis:{
                required:"Logistik Wajib diisi"
            },
            kepemilikan:{
                required:"Tahun Wajib diisi"
            }
            
        },
        submitHandler:function(form){
             $.ajax({
        url:"logistik_action.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),
            'kabupaten':$('#kabupaten').val(),
            'kodefaskes':$('#kodefaskes').val(),
            'namafaskes':$('#namafaskes').val(),
            'jenis':$('#jenis').val(),
            'kepemilikan':$('#kepemilikan').val(),
            'kodekecamatan':$('#kecamatan').val(),
            'kodekelurahan':$('#kelurahan').val(),
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                
                 $('#datalogistik').load('data/datatable_logistik.php');
            }
            
            alert(jdata.msg);
        },
        error:function(){
            alert("internal server Error");
        }
    })
        }
    })
} */

//dadang, validasi logistik
function validasiLogistik(id_log){
    $(id_log).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },
          kabupaten:{
            required:true
          },
          bulan:{ //bulan
            required:true
          },
		  logistik:{ //nama_logistik
            required:true
          },
          tahun:{ //tahun
            required:true
          },          
          stok:{
            required:true,             
          }          
        },
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },
            kabupaten:{
                required:"Kabupaten Wajib dipilih"
            },
			bulan:{
                required:"Bulan Wajib diisi"
            },			
            logistik:{
                required:"Logistik Wajib diisi"
            },
			tahun:{
                required:"Tahun Wajib diisi"
            },
            stok:{ //stok
                required:"Stok Wajib diisi; kosong=0",
                //remote:"Kode Fasyankes ini sudah ada"
            },  
        },
        submitHandler:function(form){
             $.ajax({
        url:"logistik_action.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),
            'kabupaten':$('#kabupaten').val(),            
			'bulan':$('#bulan').val(),
			'logistik':$('#logistik').val(),
            'tahun':$('#tahun').val(),
            'stok':$('#stok').val(),            
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                //logistik level kabupaten
                 $('#datalogistik').load('data/datatable_logistik.php');
            alert(jdata.msg);
            } 
			else {
				//alert("Data Sudah Ada!!!");
				alert(jdata.msg);
			} 
            //alert(jdata.msg);
        },
        error:function(){
            alert("Kabupaten: Data Logistik Sudah Ada!");
        }
    })
        }
    })
}


// validasi Logistik edit
//function validasiFasyankesEdit(id,kdfaskes){
function validasiLogistikEdit(id_log){
    
    $(id_log).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },
          kabupaten:{
            required:true
          },
          bulan:{
            required:true
          },
          logistik:{
            required:true
          },
          
          /* kodefaskes:{
            required:true,
             remote:{
                    url:'validasi/form_validation.php',
                    type:"post",
                    data:{
                        'validasi':'kodefaskes',
                        'idfaskes':kdfaskes
                    }
                }
          }, */
          tahun:{
            required:true
          },
		  stok:{
            required:true
          }
        },		
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },
            kabupaten:{
                required:"Kabupaten Wajib dipilih"
            },
			bulan:{
                required:"Bulan Wajib diisi"
            },			
            logistik:{
                required:"Logistik Wajib diisi"
            },
			tahun:{
                required:"Tahun Wajib diisi"
            },
            stok:{ //stok
                required:"Stok Wajib diisi; kosong=0",
                //remote:"Kode Fasyankes ini sudah ada"
            },             
        },
        submitHandler:function(form){
             $.ajax({
        url:"logistik_action.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),
            'kabupaten':$('#kabupaten').val(),
            'bulan':$('#bulan').val(),
            'logistik':$('#logistik').val(),
            'tahun':$('#tahun').val(),
            'stok':$('#stok').val(),
			'id_log':$('#id_log').val(),
            //'kodekecamatan':$('#kecamatan').val(),
            //'kodekelurahan':$('#kelurahan').val(),
            //'idfaskes':$('#idfaskes').val(), 
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                
                 //$('#datafasyankes').load('data/datatable_fasyankes.php');
				 $('#datalogistik').load('data/datatable_logistik.php');
            }
            
            alert(jdata.msg);
        },
        error:function(){
            alert("internal server Error271");
        }
    })
        }
    })
}
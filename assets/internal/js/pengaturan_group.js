function addNewGroup(){
    $.ajax({
        url:"group_action.php",
        type:"POST",
        dataType:"json",
        data:{
            'namagroup':$('#labelgroup').val(),
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                $('#formMenu').slideUp('slow',function(){
                 $('#listdata').load('data/datatable_group.php');
                });
           }
        alert(jdata.msg);
        },
        error:function(){
            alert("internal server error");
        }
    })
}

function getGroup(gid){
    $.ajax({
        url:"group_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'getdata',
            'gid':gid
        },success:function(jdata){
            $('#labelgroup').val(jdata.namagroup);
            $('#gid').val(jdata.gid);
            $('#formMenu').slideDown('slow');
            $('#adgroup').hide();
            $('#edgroup').show();
        },
        error:function(){
            alert("internal server Error");
        }
    })
}

function updateGroup(){
    $.ajax({
        url:"group_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'edit',
            'gid':$('#gid').val(),
            'namagroup':$('#labelgroup').val()
        },
        success:function(jdata){
           if (jdata.status=='success') {
                $('#formMenu').slideUp('slow',function(){
                 $('#listdata').load('data/datatable_group.php');
                });
           }
        alert(jdata.msg);
        },
        error:function(){
            alert("internal server error");
        } 
        
    })
}

function deleteGroup(gid){
   var conf = confirm("Anda akan menghapus data ini?");
   if (conf) {
    //code
   
    $.ajax({
        url:"group_action.php",
        type:"post",
        dataType:"json",
        data:{
            'gid':gid,
            'mode':'del'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                $('#formMenu').slideUp('slow',function(){
                 $('#listdata').load('data/datatable_group.php');
                });
           }
        alert(jdata.msg);
        },
        error:function(){
            alert("internal server error");
        }
    })
   }
}
function searchFaskes(uid){

   // $('#datafasyankes').load('data/datatable_logistik.php',{
	    $('#datalogistikpusat').load('data/datatable_logistik_pusat_dd.php',{
        'mode':'search',
        'search':$(uid).val()
        })
}

function paging(pagenum) {
    //code
    
     $('#datalogistikpusat').load('data/datatable_logistik_pusat_dd.php',{
        'mode':'paging',
        'page':$(pagenum).val()
        });
}

//function editFaskes(kd_fasyankes) {
	function editFaskes(id_log) {
    $.ajax({
        url:"logistik_action_pusat_dd.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'getdata',
            //'id':id
			'id_log':id_log
        },
        success:function(jdata){
            //alert(jdata.kd_fasyankes);
            $('#id_log').val(jdata.id_log);
              $('#fasform').slideDown('slow');
              validasiLogistikEdit('#fasform',jdata.id_log);
            $('#tahun').val(jdata.tahun);
            $('#bulan').val(jdata.bulan);
			$('#logistik').val(jdata.logistik); 
			$('#stok').val(jdata.stok);						
            
        },
        error:function(){
            alert("Internal server Error889");
        }
    })
    //code	
}

//function deleteFaskes(id){ //dadang sukses
	function deleteFaskes(id_log){
    var conf = confirm("Apakah yakin anda akan menghapus data Logistik ini?");
    if (conf) {
        //code
    
    $.ajax({
        url:"logistik_action_pusat_dd.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'del',
            'id_log':id_log
			//'kd_fasyankes':kd_fasyankes
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                alert('Data Logistik berhasil dihapus');
                 $('#datalogistikpusat').load('data/datatable_logistik_pusat_dd.php');
            }else{
                alert(jdata.msg);
            }
        },
        error:function(){
            alert("internal server error");
        }
    });
    
    }
}
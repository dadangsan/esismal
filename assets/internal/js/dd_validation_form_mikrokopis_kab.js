

//dadang, validasi mikroskopis Kab
function validasiMikroskopis(id_mikros){
    $(id_mikros).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },
          kabupaten:{
            required:true
          },
		  fasyankes:{
            required:true
          },		  
          triwulan:{ //bulan
            required:true
          },
		  tahun:{ //tahun
            required:true
          },
		  mikros1:{  //ketersediaan mikroskopis
            required:true
          }
		  
		           
        },
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },
            kabupaten:{
                required:"Kabupaten Wajib dipilih"
            },
			fasyankes:{
                required:"Fasyankes Wajib dipilih"
            },
			triwulan:{ 
            required:"Triwulan Wajib dipilih"
			 }, 
			 tahun:{ //tahun
				required:"Tahun Wajib dipilih"
			  },
			  mikros1:{ 
				required:"Wajib dipilih"
			  },
        },
        submitHandler:function(form){
             $.ajax({
        url:"dd_mikroskopis_action.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),
            'kabupaten':$('#kabupaten').val(),
			'fasyankes':$('#fasyankes').val(),            
			'triwulan':$('#triwulan').val(),
			'tahun':$('#tahun').val(),			
			//'namapetugas':$('#namapetugas').val(),
			'kirimsed':$('#kirimsed').val(),
			'mikros1':$('#mikros1').val(),
			'mikros2':$('#mikros2').val(), 
			'sensi1':$('#sensi1').val(),
			'spes1':$('#spes1').val(),
			'akurasi1':$('#akurasi1').val(),
			//'akurasi1':$('#akurasi1').val(),
			'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code                
                 $('#datamikrokopiskab').load('data/dd_datatable_mikroskopis.php');
            alert(jdata.msg);
            } 
			else {
				//alert("Data Sudah Ada!!!");
				alert(jdata.msg);
			} 
            //alert(jdata.msg);
        },
        error:function(){
            alert("Kabupaten: Data Mikroskopis Sudah Ada!");
        }
    })
        }
    })
}

//dadang, validasi crosceker propinsi
function validasicroscekerProp(id_mikros){
    $(id_mikros).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },
          kabupaten:{
            required:true
          },
			tahun:{ //tahun
            required:true
          },
			mikros1:{ 
            required:true
          }		  
          /* triwulan:{ //triwulan
            required:true
          }, 
		  namapetugas:{ 
            required:true
          },
          
          		  
          mikros2:{ 
            required:true
          },		  
          sensi1:{ 
            required:true
          },
          spes1:{ 
            required:true
          },          
          akurasi1:{
            required:true,             
          }   */       
        },
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },
            kabupaten:{
                required:"Kabupaten Wajib dipilih"
            },
			tahun:{ //tahun
				required:"Tahun Wajib dipilih"
			  },
			mikros1:{ 
				required:"Wajib dipilih"
			  },
			
			
			/* triwulan:{ 
				required:"triwulan Wajib dipilih"
			},
			  namapetugas:{ 
				required:"Nama Petugas Wajib diisi"
			  },
			  		  
			  		  
			  mikros2:{ 
				required:"Wajib dipilih"
			  },		  
			  sensi1:{ 
				required:"Wajib dipilih"
			  },
			  spes1:{ 
				required:"Wajib dipilih"
			  },          
			  akurasi1:{
				required:"Wajib dipilih",             
			  },  */
		  
        },
        submitHandler:function(form){
             $.ajax({
        url:"dd_mikroskopis_action.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),
            'kabupaten':$('#kabupaten').val(),
			//'fasyankes':$('#fasyankes').val(),            
			//'triwulan':$('#triwulan').val(),
			'tahun':$('#tahun').val(),			
			'namapetugas':$('#namapetugas').val(),
			'mikros1':$('#mikros1').val(),
			'mikros2':$('#mikros2').val(), //kompetensi mikroskopis
			'sensi1':$('#sensi1').val(),
			'spes1':$('#spes1').val(),
			'akurasi1':$('#akurasi1').val(), 
			'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code                
                 $('#datamikrokopiskab').load('data/dd_datatable_mikroskopis.php');
            alert(jdata.msg);
            } 
			else {
				//alert("Data Sudah Ada!!!");
				alert(jdata.msg);
			} 
            //alert(jdata.msg);
        },
        error:function(){
            alert("Propinsi: Data Cross-Checker Sudah Ada!");
        }
    })
        }
    })
}

//dadang, validasi crosceker Pusat
function validasicroscekerPusat(id_mikros){
    $(id_mikros).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },          		  
          /* triwulan:{ //triwulan
            required:true
          }, */
		  namapetugas:{ 
            required:true
          },
          tahun:{ //tahun
            required:true
          },		  
          mikros1:{ 
            required:true
          }		  
         /* mikros2:{ 
            required:true
          },		  
          sensi1:{ 
            required:true
          },
          spes1:{ 
            required:true
          },          
          akurasi1:{
            required:true,             
          }     */     
        },
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },
            /* triwulan:{ 
				required:"Triwulan Wajib dipilih"
			}, */
			  namapetugas:{ 
				required:"Nama Petugas Wajib diisi"
			  },
			  tahun:{ //tahun
				required:"Tahun Wajib dipilih"
			  },		  
			  mikros1:{ 
				required:"Wajib dipilih"
			  },		  
			  /* mikros2:{ 
				required:"Wajib dipilih"
			  },		  
			  sensi1:{ 
				required:"Wajib dipilih"
			  },
			  spes1:{ 
				required:"Wajib dipilih"
			  },          
			  akurasi1:{
				required:"Wajib dipilih",             
			  }, */
		  
        },
        submitHandler:function(form){
             $.ajax({
        url:"dd_mikroskopis_action.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),
            //'kabupaten':$('#kabupaten').val(),
			//'fasyankes':$('#fasyankes').val(),            
			//'triwulan':$('#triwulan').val(),
			'tahun':$('#tahun').val(),			
			'namapetugas':$('#namapetugas').val(),
			'mikros1':$('#mikros1').val(),
			'mikros2':$('#mikros2').val(),
			/* 'sensi1':$('#sensi1').val(),
			'spes1':$('#spes1').val(),
			'akurasi1':$('#akurasi1').val(), */
			
			'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code                
                 $('#datamikrokopiskab').load('data/dd_datatable_mikroskopis.php');
            alert(jdata.msg);
            } 
			else {
				//alert("Data Sudah Ada!!!");
				alert(jdata.msg);
			} 
            //alert(jdata.msg);
        },
        error:function(){
            alert("Propinsi: Data Cross-Checker Sudah Ada!");
        }
    })
        }
    })
}


function deletemikroskab(id_mikros){
    var conf = confirm("Hapus Data Mikroskopis /CrossChecker ini?");
    if (conf) {
        //code
    
    $.ajax({
        url:"dd_mikroskopis_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'del',
            'id_mikros':id_mikros
			//'kd_fasyankes':kd_fasyankes
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                //alert('Data Logistik berhasil dihapus');
				alert(jdata.msg);                 
				 $('#datamikrokopiskab').load('data/dd_datatable_mikroskopis.php');
            }else{
                alert(jdata.msg);
            }
        },
        error:function(){
            alert("internal server error");
        }
    });
    
    }
}

//function searchMikroskopis(uid){
function searchMikroskopis(namapetugas){
   // $('#datafasyankes').load('data/datatable_logistik.php',{
	    $('#datamikrokopiskab').load('data/dd_datatable_mikroskopis.php',{
        'mode':'search',
        'search':$(namapetugas).val()
        })
}

function paging(pagenum) {
    //code
    
     $('#datamikrokopiskab').load('data/dd_datatable_mikroskopis.php',{
        'mode':'paging',
        'page':$(pagenum).val()
        });
}

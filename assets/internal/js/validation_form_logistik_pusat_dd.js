
//dadang, validasi logistik
function validasiLogistik(id_log){
    $(id_log).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{          
          bulan:{ //bulan
            required:true
          },
		  logistik:{ //nama_logistik
            required:true
          },
          tahun:{ //tahun
            required:true
          },          
          stok:{
            required:true,             
          }          
        },
        messages:{            
			bulan:{
                required:"Bulan Wajib diisi"
            },			
            logistik:{
                required:"Logistik Wajib diisi"
            },
			tahun:{
                required:"Tahun Wajib diisi"
            },
            stok:{ //stok
                required:"Stok Wajib diisi; kosong=0",
                //remote:"Kode Fasyankes ini sudah ada"
            },  
        },
        submitHandler:function(form){
             $.ajax({
        url:"logistik_action_pusat_dd.php",
        type:"post",
        dataType:"json",
        data:{                     
			'bulan':$('#bulan').val(),
			'logistik':$('#logistik').val(),
            'tahun':$('#tahun').val(),
            'stok':$('#stok').val(),            
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code                
                 $('#datalogistikpusat').load('data/datatable_logistik_pusat_dd.php');
				 //alert("Data Berhasil Disimpan!!!");
				 alert(jdata.msg);
            } 
			else {
				//alert("Data Sudah Ada!!!");
				alert(jdata.msg);
			} 
            //alert(jdata.msg);
			
            
        },
        error:function(){
            alert("Pusat: Data Logistik Sudah Ada!");
        }
    })
        }
    })
}


// validasi Logistik edit
//function validasiFasyankesEdit(id,kdfaskes){
function validasiLogistikEdit(id_log){
    
    $(id_log).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{          
          bulan:{
            required:true
          },
          logistik:{
            required:true
          },
          tahun:{
            required:true
          },
		  stok:{
            required:true
          }
        },		
        messages:{            
			bulan:{
                required:"Bulan Wajib diisi"
            },			
            logistik:{
                required:"Logistik Wajib diisi"
            },
			tahun:{
                required:"Tahun Wajib diisi"
            },
            stok:{ //stok
                required:"Stok Wajib diisi; kosong=0",
                //remote:"Kode Fasyankes ini sudah ada"
            },             
        },
        submitHandler:function(form){
             $.ajax({
        url:"logistik_action_pusat_dd.php",
        type:"post",
        dataType:"json",
        data:{
            'bulan':$('#bulan').val(),
            'logistik':$('#logistik').val(),
            'tahun':$('#tahun').val(),
            'stok':$('#stok').val(),
			'id_log':$('#id_log').val(),
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                
                 //$('#datafasyankes').load('data/datatable_fasyankes.php');
				 $('#datalogistikpusat').load('data/datatable_logistik_pusat_dd.php');
            }
            
            alert(jdata.msg);
        },
        error:function(){
            alert("internal server Error271");
        }
    })
        }
    })
}
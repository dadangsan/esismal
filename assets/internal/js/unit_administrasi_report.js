// ori
function getComboFaskesreport1(id,thiskab){	
    var idcombo = '#'+id;
    var idkab = $(thiskab).val();
    $.ajax({
        url:"data/data_administratif_report.php",
        type:"post",
        dataType:"json",
        data:{
            'que':'faskes',
            'idkab':idkab,
            'kodeid':''
        },
        success:function(jdata){
            $(idcombo).html(jdata.list);			 
			 $('#fasyankes').multiselect({
				
				//onInitialized: function(select, container) {			
				//},			
				nonSelectedText: 'Pilih Faskes',
				includeSelectAllOption: true,
				enableFiltering: true
				//filter:true
			}); 
        }
    })
} 

function getComboFaskesreport(id,thiskab){	
    var idcombo = '#'+id;
    var idkab = $(thiskab).val();
    $.ajax({
        url:"data/data_administratif_report.php",
        type:"post",
        dataType:"json",
        data:{
            'que':'faskes',
            'idkab':idkab,
            'kodeid':idkab
        },
        success:function(jdata){
            $(idcombo).html(jdata.list);			 
			 $('#fasyankes').multipleSelect({
				//placeholder: "Pilih Propinsi",
				placeholder: "Pilih Faskes",
				filter:true,
                buttonWidth:'400px'
			}); 
        }
    })
}

//untuk kab multiselect
function getComboKabreport(id,thisprop){
    var idcombo = '#'+id;
    var idprop = $(thisprop).val();
    $.ajax({
        url:"data/data_administratif_report.php",
        type:"post",
        dataType:"json",
        data:{
            'que':'kab',
            'idprop':idprop,
            'kodeid':''
        },
        success:function(jdata){
            $(idcombo).html(jdata.list);
			$('#kabupaten').multiselect(
			/*{
				//placeholder: "Pilih Propinsi",
				onInitialized: function(select, container) {
                },
				nonSelectedText: 'Pilih Kabupaten',
				includeSelectAllOption: true,
				filter:true
			} */
			); 
        }
    })
}
function getComboFaskesmikros(id,thiskab){	
    var idcombo = '#'+id;
    var idkab = $(thiskab).val();
    $.ajax({
        url:"data/data_administratif_report.php",
        type:"post",
        dataType:"json",
        data:{
            'que':'faskes',
            'idkab':idkab,
            'kodeid':idkab
        },
        success:function(jdata){
            $(idcombo).html(jdata.list);			 
			 $('#fasyankes').multipleSelect({
				//placeholder: "Pilih Propinsi",
				placeholder: "Pilih Faskes",
				filter:true,
                buttonWidth:'400px'
			}); 
        }
    })
}
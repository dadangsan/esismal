
//dadang, validasi logistik
function validasiLogistik(id_log){
    $(id_log).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },          
          bulan:{ //bulan
            required:true
          },
		  logistik:{ //nama_logistik
            required:true
          },
          tahun:{ //tahun
            required:true
          },          
          stok:{
            required:true,             
          }          
        },
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },            
			bulan:{
                required:"Bulan Wajib diisi"
            },			
            logistik:{
                required:"Logistik Wajib diisi"
            },
			tahun:{
                required:"Tahun Wajib diisi"
            },
            stok:{ //stok
                required:"Stok Wajib diisi; kosong=0",
                //remote:"Kode Fasyankes ini sudah ada"
            },  
        },
        submitHandler:function(form){
             $.ajax({
        url:"logistik_action_prov_dd.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),
            //'kabupaten':$('#kabupaten').val(),            
			'bulan':$('#bulan').val(),
			'logistik':$('#logistik').val(),
            'tahun':$('#tahun').val(),
            'stok':$('#stok').val(),            
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code                
                $('#datalogistikprov').load('data/datatable_logistik_prov_dd.php');
				alert(jdata.msg);
            } 
			else {
				//alert("Data Sudah Ada!!!");
				alert(jdata.msg);
			} 
            //alert(jdata.msg);
        },
        error:function(){
            alert("Provinsi: Data Logistik Sudah Ada!");
        }
    })
        }
    })
}


// validasi Logistik edit
//function validasiFasyankesEdit(id,kdfaskes){
function validasiLogistikEdit(id_log){
    
    $(id_log).validate({
        onfocusout: function(element) {
           this.element(element);
        },
        rules:{
          propinsi:{
            required:true
          },          
          bulan:{
            required:true
          },
          logistik:{
            required:true
          },
          tahun:{
            required:true
          },
		  stok:{
            required:true
          }
        },		
        messages:{
            propinsi:{
                required:"Propinsi Wajib dipilih"
            },            
			bulan:{
                required:"Bulan Wajib diisi"
            },			
            logistik:{
                required:"Logistik Wajib diisi"
            },
			tahun:{
                required:"Tahun Wajib diisi"
            },
            stok:{ //stok
                required:"Stok Wajib diisi; kosong=0",
                //remote:"Kode Fasyankes ini sudah ada"
            },             
        },
        submitHandler:function(form){
             $.ajax({
        url:"logistik_action_prov_dd.php",
        type:"post",
        dataType:"json",
        data:{
            'propinsi':$('#propinsi').val(),            
            'bulan':$('#bulan').val(),
            'logistik':$('#logistik').val(),
            'tahun':$('#tahun').val(),
            'stok':$('#stok').val(),
			'id_log':$('#id_log').val(),            
            'mode':'add'
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                
                 //$('#datafasyankes').load('data/datatable_fasyankes.php');
				 $('#datalogistikprov').load('data/datatable_logistik_prov_dd.php');
            }
            
            alert(jdata.msg);
        },
        error:function(){
            alert("internal server Error271");
        }
    })
        }
    })
}
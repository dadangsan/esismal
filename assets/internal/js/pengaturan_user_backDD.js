/* pengaturan user by harmi prasetyo @2017 */
function addNewUser(){
    
    $.ajax({
        url:'user_action.php',
        type:"post",
        dataType:"json",
        data:{
            'mode':'add',
            'username':$('#username').val(),
            'usergroup':$('#usergroup').val(),
            'propinsi':$('#propinsi').val(),
            'kabupaten':$('#kabupaten').val(),
            'fasyankes':$('#fasyankes').val(),
            'nama':$('#nama').val(),
            'userpassword':$('#userpassword').val()
        },
        success:function(jdata){
            if (jdata.status=='success') {
                //code
                 $('#formMenu').slideUp('slow',function(){
                 $('#listdata').load('data/datatable_user.php');
                });
           }
        alert(jdata.msg);
            },
            error:function(){
                alert('Internal Server Error');
            }
        });
}

function getUser(uid){
    $.ajax({
        url:"user_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'getdata',
            'uid':uid
        },
        success:function(jdata){
            $('#uid').val(jdata.username);
            $('#username').val(jdata.username);
            $('#usergroup').val(jdata.id_group);
            $('#nama').val(jdata.nama);
            $('#propinsi').val(jdata.propinsi);
            $('#formMenu').slideDown('slow');
            $('#aduser').hide();
            $('#eduser').show();
            //$('#kabupaten').val(jdata.kabupaten);
            //$('')
            if (jdata.kabupaten!='') {
                //code
                $.ajax({
                    url:"data/data_administratif.php",
                    type:"post",
                    dataType:"json",
                    data:{
                        'que':'kab',
                        'idprop':jdata.propinsi,
                        'kodeid':jdata.kabupaten
                        },
                        success:function(jdata){
                            $('#kabupaten').html(jdata.list);
                            }
                            })
                //$('#kabupaten').val(jdata.kabupaten);
                }
            
           
           if (jdata.kd_fasyankes!='') {
                //code
               $.ajax({
                url:"data/data_administratif.php",
                type:"post",
                dataType:"json",
                data:{
                    'que':'faskes',
                    'idkab':jdata.kabupaten,
                    'kodeid':jdata.kd_fasyankes
                    },
                    success:function(jdata){
                        $('#fasyankes').html(jdata.list);
                        }
                        })
                //$('#fasyankes').val(jdata.kabupaten);
                } 
         
         
            
        }
    })
}
function updateUser(){
    //alert('go');
    
    $.ajax({
        url:"user_action.php",
        type:"post",
        dataType:"json",
        data:{
            'mode':'edit',
            'uid':$('#uid').val(),
            'username':$('#username').val(),
            'usergroup':$('#usergroup').val(),
            'propinsi':$('#propinsi').val(),
            'kabupaten':$('#kabupaten').val(),
            'fasyankes':$('#fasyankes').val(),
            'nama':$('#nama').val(),
            'userpassword':$('#userpassword').val()
        },success:function(jdata){
            if (jdata.status=='success') {
                //code
                 $('#formMenu').slideUp('slow',function(){
                 $('#listdata').load('data/datatable_user.php');
                });
           }
        alert(jdata.msg);
            },
            error:function(){
                alert('Internal Server Error');
            }
    });

}

function deleteUser(uid){
   var conf = confirm("Apakah anda akan menghapus data ini?");
   if (conf) {
    //code
   
    $.ajax({
        url:"user_action.php",
        type:"post",
        dataType:"json",
        data:{
            'uid':uid,
            'mode':'del'
        },success:function(jdata){
            if (jdata.status=='success') {
                //code
                 $('#formMenu').slideUp('slow',function(){
                 $('#listdata').load('data/datatable_user.php');
                });
           }
        alert(jdata.msg);
            },
            error:function(){
                alert('Internal Server Error');
            }
    });
   }
}

function search(uid){
    $('#listdata').load('data/datatable_user.php',{
        'mode':'search',
        'search':$(uid).val()
        })
}

function userPaging(pagenum){
     $('#listdata').load('data/datatable_user.php',{
        'mode':'paging',
        'page':$(pagenum).val()
        });
}

function regmal1Paging9(pagenum){
     $('#dataregmal').load('data/dd_datatable_regmal1.php',{		 
		'mode':'paging',
        'page':$(pagenum).val()
        });
}
function regmal1Paging8(pagenum){
    
    $.ajax({
        //url:'user_action.php',
		url:'data/dd_datatable_regmal1.php.php',
        type:"post",
        dataType:"json",
        data:{
            'mode':'paging',
			'page':$(pagenum).val()
            'tahun':$('#tahun').val(),
            'level':$('#level').val(),
            'bulan1':$('#bulan1').val(),
			'propinsi':$('#propinsi').val(),			
            'kabupaten':$('#kabupaten').val(),
            'faskes':$('#faskes').val()
            
        }        
        });
}

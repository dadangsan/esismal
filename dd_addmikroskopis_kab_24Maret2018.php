<?php
//ref add logistik (e4s_logistik.php)
session_start();
require_once("/config/database-connect.php");
//print_r($_SESSION);
$lprop = mysqli_query($dbconn,"select id_propinsi,nama_propinsi from mst_propinsi");
?>
<div class="col-lg-12">
    <div class="panel">
        <div class="panel panel-primary">
            <div class="panel-heading">
               <i class="fa fa-hospital-o" aria-hidden="true"></i>
&nbsp;Mikroskopis Malaria
            (Kabupaten)</div>
            <div class="panel-body">
                <div class="row col-lg-12" id="inputForm" style="padding: 10px;">
                    <div class="col-lg-2">&nbsp;</div>
                    <div class="col-lg-10">
                        <form id="fasform" method="post" style="display: none">
                        <!-- <input type="hidden" id="id_log" name="id_log" value=""> -->
                        <input type="hidden" id="id_mikros" name="id_mikros" value="">
<table class="tblInput">
   
    <tbody>
    <tr>
                <th>Tahun</th>                
                <th>:<select name="tahun" id="tahun">            
				<option value="">-Pilih-</option>
				<?php       		
				for($i=date('Y');$i>=2018;$i--){ 		
				 $sel = ($i == date('Y')) ? 'selected' : '';		 		    
				?>
				<OPTION VALUE="<?php echo $i;?>"><?php echo $i;?></OPTION>
				<?php }  ?>	
			
				</select>  
					&nbsp;&nbsp; Triwulan : 
					<select name="triwulan" id="triwulan">
				<option value="">-Pilih-</option>
				 <option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>            
				</select>
				  </th>
				</tr>
        
    <tr>
        <th>Propinsi *</th>
        <th>:
        <?php //cek login, login as superuser or pusat
		if($_SESSION['id_group'] ==2 or $_SESSION['id_group'] ==1 ){
                $lprop = mysqli_query($dbconn,"select id_propinsi,nama_propinsi from mst_propinsi");            
		?>
        <select name="propinsi" id="propinsi" onchange="getComboKab('kabupaten',this)">
        <option value="">-- Pilih --</option>
         <?php
		 while($np=mysqli_fetch_object($lprop)){
			 echo "<option value=".$np->id_propinsi.">".$np->nama_propinsi."</option>";	
		 }
		} //login as propinsi
		elseif ($_SESSION['id_group'] ==3 ){
			 $lprop = mysqli_query($dbconn,"select id_propinsi,nama_propinsi from mst_propinsi where id_propinsi = '".$_SESSION['propinsi']."'");                           
		?>
        <select name="propinsi" id="propinsi" onchange="getComboKab('kabupaten',this)">
        <option value="">-- Pilih --</option>
         <?php
		 while($np=mysqli_fetch_object($lprop)){
			 echo "<option value=".$np->id_propinsi.">".$np->nama_propinsi."</option>";	
		 }
		} //login kabupaten, faskes		
		else {
			 $lprop = mysqli_query($dbconn,"select id_propinsi,nama_propinsi from mst_propinsi where id_propinsi = '".$_SESSION['propinsi']."'");                           
		?>
        <!-- <select name="propinsi" id="propinsi" >   -->
        <select name="propinsi" id="propinsi" onchange="getComboKab('kabupaten',this)"> 
        <option value="">-- Pilih --</option>    
         <?php
		 while($np=mysqli_fetch_object($lprop)){
			 $sel = ($np->id_propinsi) ? 'selected' : '';			 
			 echo "<option value=".$np->id_propinsi." ".$sel.">".$np->nama_propinsi."</option>";	
		 }
		} 
		?>
        </select>
        </th>
    </tr>
    <?php if($_SESSION['id_group'] ==4 {	
	?>
     <tr>
        <th>Kab/Kota *</th>
        <th>:
        <!-- <select name="kabupaten" id="kabupaten" onchange="getComboKec('kecamatan',this);alert(this)">  -->
        
        <!-- <select name="kabupaten" id="kabupaten" onchange="getComboFaskes('fasyankes',this)">
            <option value="">-- Pilih --</option>
        </select> -->
        
        <?php //cek login, login as superuser or pusat
		if($_SESSION['id_group'] ==4 or $_SESSION['id_group'] ==5){
                $lkab = mysqli_query($dbconn,"select id_kabupaten,nama_kabupaten from mst_kabupaten where id_kabupaten ='".$_SESSION['kabupaten']."'");             
		?>
        <!-- <select name="kabupaten" id="kabupaten" >   -->
        <select name="kabupaten" id="kabupaten" onchange="getComboFaskes('fasyankes',this)">
        <option value="">-- Pilih --</option>

         <?php
		 while($np=mysqli_fetch_object($lkab)){
			 echo "<option value=".$np->id_kabupaten.">".$np->nama_kabupaten."</option>";	
		 }
		} //login superuser, pusat, prop
		else {                            
		?>
        <select name="kabupaten" id="kabupaten" >  
        <option value="">-- Pilih --</option> 
         <?php } ?>
        </select>

        
        </th>
    </tr>
	<?php
	} ?>
    <tr>
        <th>Fasyankes</th>
        <th>:
        <select name="fasyankes" id="fasyankes">            
            <option value="">-- Pilih --</option>
        </select>
        </th>
    </tr>    
    <tr>
        <!-- <th>Kode Fasyankes *</th>-->
        <th>Nama Petugas *</th>
<!--     <th>: <input type="text" name="stok" size="20" id="stok" maxlength="11"></th> -->
<th>: <input type="text" name="namapetugas" size="30" id="namapetugas" maxlength="30"></th>
    </tr>
     <!-- <tr>
        <th>Nama Fasyankes *</th>
    <th>: <input type="text" name="namafaskes" size="40" id="namafaskes"></th>
    </tr>
     <tr> -->
     	<tr>
            <th>Ketersedian Mikroskop</th>
            <th>: <select name="mikros1" id="mikros1">
			<option value="">-Pilih-</option>
            <option value="1">Tersedia</option>
            <option value="2">Tidak Tersedia</option>                      
			</select>
			| Kompetensi Mikroskopis:
            <select name="mikros2" id="mikros2"> 			
			<option value="">-Pilih-</option>
            <option value="1">Level1</option>
            <option value="2">Level2</option>
			<option value="3">Level3</option>
			<option value="4">Level4</option>
			<option value="5">Belum Diketahui</option>
			<option value="6">Belum Terlatih</option>			
			</select>
			</th>
            </tr>
            
            <tr>
            <th>Uji Silang | Sensitivitas</th>
            <th>: <select name="sensi1" id="sensi1">
			<option value="">-Pilih-</option>
            <option value="0"><70</option>
            <option value="1">>=70</option>                      
			</select>
			|Spesivitas: 
			<select name="spes1" id="spes1"> 			
			<option value="">-Pilih-</option>
            <option value="0"><70</option>
            <option value="1">>=70</option> 			
			</select>
			|Akurasi Spesies:
			<select name="akurasi1" id="akurasi1"> 			
			<option value="">-Pilih-</option>
            <option value="0"><70</option>
            <option value="1">>=70</option> 
			</th>
            </tr>

        <th>&nbsp;</th>
        <th>
            <button id="editdata" class="btn btn-primary btn-xs" style="display: none">Edit</button>&nbsp;
            <button id="newdata" class="btn btn-primary btn-xs">Simpan</button>
            
        </th>
     </tr>

			
		

    </tbody>
</table>
                        </form>
                </div>
         
                </div>
                <div class="row col-lg-12" style="height: 350px;overflow-y: scroll;overflow-x: scroll">
                <!-- <table class="tblListData" style="width: 1200px"> -->
                <table class="tblListData">
                    <thead>
                        <tr><th colspan="14" style="text-align: left">
						<!--dadang, kalo login sebagai kabupaten, mikroskopis -->
						<?php 
						if($_SESSION['id_group'] ==4){ 						
						?>
                            <button class="btn btn-default btn-xs" id="btnAddkab" onclick="showForm('#fasform');validasiMikroskopis('#fasform')">
							<i class="fa fa-plus-circle" aria-hidden="true"></i>
							Tambah Data Mikroskopis</button>
						<?php 
						}
						elseif($_SESSION['id_group'] ==3){ 						
						?>
                            <button class="btn btn-default btn-xs" id="btnAddkab" onclick="showForm('#fasform');validasicroscekerProp('#fasform')">
							<i class="fa fa-plus-circle" aria-hidden="true"></i>
							Tambah Data CrossChekerKab</button>
						<?php 
						}
						elseif($_SESSION['id_group'] ==2 or $_SESSION['id_group'] ==1 ){ 						
						?>
                            <button class="btn btn-default btn-xs" id="btnAddkab" onclick="showForm('#fasform');validasicroscekerPusat('#fasform')">
							<i class="fa fa-plus-circle" aria-hidden="true"></i>
							Tambah Data CrossChekerProp</button>
						<?php 
						}
						?>
						
							<i class="fa fa-search" aria-hidden='true'></i><input type="text" id="cari" name="cari" placeholder="Cari Nama Mikroskopis"  value="" onkeyup="searchMikroskopis(this)">
<!-- <input type="text" id="cari" name="cari" placeholder="Cari Nama Mikroskopis"  value="" onkeyup="searchFaskes(this)"> -->

                        </th></tr>
                        <tr>
                        <th style="text-align: center;width: 5%">No.</th>
                        <th style="text-align: center;width: 10%">Nama</th>
                        <th  style="text-align: center;width: 5%">Tahun</th>
                        <th  style="text-align: center;width: 5%">Triwulan</th>
                        
						<!-- <th  style="text-align: center;width: 5%">Faskes</th>
                        <th  style="text-align: center;width: 10%">Kabupaten</th>
                        <th  style="text-align: center;width: 10%">Provinsi</th> -->
						<?php 
							if($_SESSION['id_group'] ==4){ 						
						?>
						<th  style="text-align: center;width: 5%">Faskes</th>
                        <th  style="text-align: center;width: 10%">Kabupaten</th>
                        <th  style="text-align: center;width: 10%">Provinsi</th>
						  <?php } elseif ($_SESSION['id_group'] ==3){ 						
							?>  
						<th  style="text-align: center;width: 10%">Kabupaten</th>
                        <th  style="text-align: center;width: 10%">Provinsi</th>
						  <?php } elseif ($_SESSION['id_group'] ==1 or $_SESSION['id_group'] ==2){ 						
							?>    
						<th  style="text-align: center;width: 10%">Provinsi</th>
						  <?php }else {				
							} ?>
						  
                       
                        <th  style="text-align: center;width: 5%">Mikroskop</th>
                        <th  style="text-align: center;width: 5%">Komp. Mikroskopis</th>
                        <th  style="text-align: center;width: 5%">Sensitivitas</th>
                        <th  style="text-align: center;width: 5%">Spesivitas</th>
                        <th  style="text-align: center;width: 5%">Akurasi Spesies</th>
                        <th  style="text-align: center;width: 5%">Hasil Uji Silang</th>
                        <th  style="text-align: center;width: 10%">Action</th>
                        </tr>
                    </thead>                    
                    <tbody id="datamikrokopiskab"> <!-- terkait dg file /assets/internal/js/dd_validation_form_mikroskopis_kab.js -->
                        
                    </tbody>
                    
  <?php      
//dadang cek login
if($_SESSION['id_group'] ==2 or $_SESSION['id_group'] ==1 ){
 $mrQ4 = "SELECT a.id_mikros, a.user_log, a.kd_kab, a.kd_prop, a.kd_faskes, a.namapetugas, a.tahun, a.triwulan, a.ksediaan_mikros, a.kompt_mikros, a.sensitivitas1, a.spesivitas1, a.akurasi_spes, c.`nama_kabupaten`, d.`nama_propinsi` FROM  ent_mikroskops a INNER JOIN mst_fasyankes b ON (a.kd_faskes=b.kd_fasyankes) INNER JOIN mst_kabupaten c ON(a.`kd_kab`=c.`id_kabupaten`) INNER JOIN mst_propinsi d ON(a.`kd_prop`=d.`id_propinsi`)";

}elseif ($_SESSION['id_group'] ==3 ) {
	$mrQ4 = "SELECT a.id_mikros, a.user_log, a.kd_kab, a.kd_prop, a.kd_faskes, a.namapetugas, a.tahun, a.triwulan, a.ksediaan_mikros, a.kompt_mikros, a.sensitivitas1, a.spesivitas1, a.akurasi_spes, c.`nama_kabupaten`, d.`nama_propinsi` FROM  ent_mikroskops a INNER JOIN mst_fasyankes b ON (a.kd_faskes=b.kd_fasyankes) INNER JOIN mst_kabupaten c ON(a.`kd_kab`=c.`id_kabupaten`) INNER JOIN mst_propinsi d ON(a.`kd_prop`=d.`id_propinsi`) where a.kd_prop = '".$_SESSION['propinsi']."'";
}elseif ($_SESSION['id_group'] ==4 ) {
	$mrQ4 = "SELECT a.id_mikros, a.user_log, a.kd_kab, a.kd_prop, a.kd_faskes, a.namapetugas, a.tahun, a.triwulan, a.ksediaan_mikros, a.kompt_mikros, a.sensitivitas1, a.spesivitas1, a.akurasi_spes, c.`nama_kabupaten`, d.`nama_propinsi` FROM  ent_mikroskops a INNER JOIN mst_fasyankes b ON (a.kd_faskes=b.kd_fasyankes) INNER JOIN mst_kabupaten c ON(a.`kd_kab`=c.`id_kabupaten`) INNER JOIN mst_propinsi d ON(a.`kd_prop`=d.`id_propinsi`) where a.kd_kab = '".$_SESSION['kabupaten']."'";
	
}else {
	echo "<br/>Fasyankes Tidak Punya Akses ke Halaman ini";
	exit;
}
//echo "</br>mrQ4= ".$mrQ4;
$mrQ = mysqli_query($dbconn,$mrQ4);
$row = mysqli_num_rows($mrQ);
//echo "</br>row= ".$row; 
$numrows = "150";
$start="0";


                    
$totalPage = ceil($row/$numrows);



        ?>
        <tfoot>
        <TR><TH colspan="14">
        Halaman : <select name="page" id="page" onchange="paging(this)">
            <?php for($i=0;$i<$totalPage;$i++){ ?>
            <option value="<?php echo $i;?>"><?php echo $i+1;?></option>
            <?php } ?>
        </select>
   <!-- <ul class="pagination pagination-sm">
                <?php for($i=$minpage;$i<=$maxnum;$i++) { ?> <li><a href="#"><?php echo $i; ?></a></li><?php } ?>
</ul>-->
    </TH>
</TR>
        </tfoot>
                    
                </table>
            </div>
                
            </div>
            <div class="panel-footer text-right">&nbsp;
            
            
            </div>
        </div>
    </div>
    
</div>
<head>
<style type="text/css">
.auto-style2 {
	text-align: right;
}
</style>
</head>

<script>
/* 
function tampilGambar(){
      document.getElementById('myLoadingGif').style.display = "block";
} */
</script>
<?php
session_start();
require_once("../config/database-connect.php");
$p = (object)$_POST;
$s = (object)$_SESSION;
$prop = "SELECT * FROM mst_propinsi where id_propinsi='$p->propinsi'";
$pray = mysqli_query($dbconn,$prop);
$row = mysqli_fetch_object($pray);

  $kabs = "SELECT * FROM mst_kabupaten where id_kabupaten='$p->kabupaten'";
  $krow = mysqli_query($dbconn,$kabs);
$kray = mysqli_fetch_object($krow);

$strViewRow4 ="SELECT a.id_mikros2 as idmikros2, a.namapetugas, IF(a.ksediaan_mikros='1','Tersedia', IF(a.ksediaan_mikros='2','Tidak Tersedia','')) AS sediamikroskop, 
IF(a.kompt_mikros='1','Level1', IF(a.kompt_mikros='2','Level2', IF(a.kompt_mikros='3','Level3', IF(a.kompt_mikros='4','Level4', 
IF(a.kompt_mikros='5','Belum Diketahui', ''))))) AS komptmikros2, IF(a.sensitivitas1='0','<70',IF(a.sensitivitas1='1','>=70','')) AS sensi2, 
IF(a.spesivitas1='0','<70',IF(a.spesivitas1='1','>=70','')) AS spesi2, IF(a.akurasi_spes='0','<70', 
IF(a.akurasi_spes='1','>=70','')) AS akurasi2, a.hasil_uji_sil AS hasil2, ";


if($p->level=='1'){ //nasional    
    $namaPropinsi = "Semua Propinsi";
    $strViewRow = $strViewRow4." b.id_kabupaten, b.nama_kabupaten AS AREA, b.`id_propinsi` ,c.`nama_propinsi` as namaprop
FROM ent_mikroskops a LEFT JOIN mst_kabupaten b ON(a.kd_kab=b.id_kabupaten ) 
 RIGHT JOIN mst_propinsi c ON (a.`kd_prop`=c.`id_propinsi` && tahun='$p->tahun' AND ((RIGHT(a.id_mikros2,2)='/1') or (RIGHT(a.id_mikros2,2)='/2'))) ";
 //RIGHT JOIN mst_propinsi c ON (a.`kd_prop`=c.`id_propinsi`) && tahun='$p->tahun' && triwulan='$p->bulan1'  ";
 
}elseif($p->level=='2'){ //radiobuttn propinsi      
	$namaPropinsi = $row->nama_propinsi;
	$namaKab = "Semua Kabupaten";	
	$strViewRow = $strViewRow4." b.id_kabupaten, b.nama_kabupaten AS AREA, b.`id_propinsi` ,c.`nama_propinsi` as namaprop
FROM ent_mikroskops a RIGHT JOIN mst_kabupaten b ON(a.kd_kab=b.id_kabupaten && tahun='$p->tahun' AND (RIGHT(a.id_mikros2,2)='/3'))  
 RIGHT JOIN mst_propinsi c ON (b.`id_propinsi`=c.`id_propinsi`)
WHERE b.id_propinsi='$p->propinsi' ";
 //FROM ent_mikroskops a RIGHT JOIN mst_kabupaten b ON(a.kd_kab=b.id_kabupaten && tahun='$p->tahun') && triwulan='$p->bulan1' 	
}


//echo $strView;
//echo $strViewRow;
?>

<table class="tblInput" style="width: 100%">
    <thead>
        <tr>
		<?php 
		//if ($_SESSION['id_group'] ==3){ 
		if ($p->level=='2'){  
		?>
            <th><h3>Laporan CrossChecker Individu per-Kabupaten</h3></th>
		<?php 
		} // elseif ($_SESSION['id_group'] ==1 or $_SESSION['id_group'] ==2 or $_SESSION['id_group'] ==7){ 		
		elseif ($p->level=='1'){
		?>
		<th><h3>Laporan CrossChecker Individu per-Propinsi</h3></th>
		<?php } ?>
        </tr>
        <tr>
            <th>
                
                
                <table width="500" class="tblInput" style="width: 400px">
                    <thead>
                        <tr>
                            <th width="131" class="auto-style2">Tahun :</th>
                            <th style="width: 300px">&nbsp;<?php echo $p->tahun;
							if($p->periode=='2'){
							?>| Triwulan:<?php echo $p->bulan1;?></th> 
                            <?php } ?>
                            
                            
                        </tr>
                        <tr>
                            <th class="auto-style2" style="height: 23px">Propinsi 
							:</th>
                            <th style="height: 23px; width: 300px;">&nbsp;<?php echo $namaPropinsi;?></th>
                        </tr>
                        
                        <tr>
                            <th class="auto-style2">Kabupaten/Kota :</th>
                            <th style="width: 300px">&nbsp;<?php echo $namaKab;?></th>
                        </tr> 
                        
                        <!-- <tr>
                            <th style="vertical-align: top" class="auto-style2">Fasyankes 
							:</th>
                            <th style="width: 300px">&nbsp;<?php // echo $namaFaskes;?></th>
                        </tr> -->
                    </thead>
                </table>
            </th>
            
        </tr>
        <!-- <tr>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>
               
                
            </th>
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr> -->
        <tr>
            <th>
                
                <table class="tblListData" style="width: 100%">
                    <thead>
					  
						
						<tr>
                            <!-- <th width="132" rowspan="1" style="width: 20px">No</th> -->
                            <th rowspan="1" style="width: 3%">No</th> 
							<th rowspan="1"  style="width: 15%">Propinsi</th>
							<?php if ($p->level=='2'){ ?> 
							<th rowspan="1"  style="width: 15%">Kabupaten</th>
                            <?php } ?>	                     
                            <th colspan="1"  style="width: 9%">Cross Checker</th>
							<th colspan="1"  style="width: 15%">Nama CrossChecker</th>
							<th colspan="1"  style="width: 5%">Komp. CrossChecker</th>
							<?php if ($p->level=='2'){ ?> 
							<th colspan="1"  style="width: 5%">Sensitivitas</th>
                            <th colspan="1"  style="width: 5%">Spesivitas</th>
                            <th colspan="1"  style="width: 5%">Akurasi Spesiemen</th>
                            <th colspan="1"  style="width: 8%">HASIL</th>
							<!-- <th colspan="1"  style="width: 5%"><span style="width: 5px">HASIL</span></th> -->
                            <?php } ?>	
                            
                        </tr>
					
                    </thead>
                    <tbody>
                                              

<?php
// if ($p->level=='1' or $p->level=='2'){ 
//echo "<br/>strViewRow99=".$strViewRow;
$queView = mysqli_query($dbconn,$strViewRow);
                        $i=1;											
						while($rowView=mysqli_fetch_object($queView)){ 
						$idmikros3=substr($rowView->idmikros2,-2);
						
						// if ($idmikros3 =='/1') { ?>
						<!--	<tr bgcolor="yellow">
							<?php
						//	}else { ?>
							<tr> -->
							<?php // }  ?>  
                        
						 <tr> 
                            <td><?php echo $i; ?></td>     
							<td><?php echo $rowView->namaprop;?></td>
							<?php if ($p->level=='2'){ ?> 
							<td><?php echo $rowView->AREA;?></td>		
                            <?php } ?>	
										
							
							<td><?php echo $rowView->sediamikroskop;	 ?></td>
							<td><?php //echo "strViewRow= ".$strViewRow;
							echo $rowView->namapetugas; ?></td>
                          <td><?php echo $rowView->komptmikros2;  ?></td>
                          
						  <?php if ($p->level=='2'){ ?> 
							<td><?php echo $rowView->sensi2; ?></td>
							<td><?php echo $rowView->spesi2; ?></td>
							<td><?php echo $rowView->akurasi2; ?></td>						  
							<td><?php echo $rowView->hasil2; ?></td>   
                            <?php } ?> 
							
						                                                   
                        </tr>
						<?php $i++;						
                        } 
// } 
 ?>
					
                    </tbody>
                    
                </table>
                
                
            </th>
        </tr>
    </thead>
</table>
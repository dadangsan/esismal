<?php
session_start();
require_once("../config/database-connect.php");
$p = (object)$_POST;
$s = (object)$_SESSION;


$prop = "SELECT * FROM mst_propinsi where id_propinsi='$p->propinsi'";
$pray = mysqli_query($dbconn,$prop);
$row = mysqli_fetch_object($pray);

  $kabs = "SELECT * FROM mst_kabupaten where id_kabupaten='$p->kabupaten'";
  $krow = mysqli_query($dbconn,$kabs);
$kray = mysqli_fetch_object($krow);
  

/*if($p->kabupaten!=''){
    $namakabs = $kray->nama_kabupaten;
}else{
    $namakabs = "Semua Kab/Kota";
}
if(!isset($p->fasyankes)){
    $ListFasyankes = "Semua";
}
*/

$sqlSTR = "SELECT b.`nama_fasyankes`,a.`idfaskes`,a.`nik`,a.`nkk`,a.`j_penemuan`,a.`nama_pende` AS namapasien,a.`umur`,
a.`th_bl`,a.`jnskel`,a.`hamil`,a.`alamat1`,IF(c.`id_kelurahan` IS NULL,a.kd_desa_kel,c.nama_kelurahan) AS kel_desa,
a.bulankun,a.tglkun AS tglkunj,a.pekerjaan,a.lab,a.`parasit`,a.`derajat`,a.`dhp`,
a.`primaquin`,a.`kinatablet`,a.`artesunat`,a.`artemeter`,a.`kinainj`,a.`non_prog`,a.`p_standar`,a.`p_rawat`,
a.`faskes_menemukan` AS dirujuk,a.`dirujuk_ke`,a.`tgl_pe`,a.baru_relaps, a.`klasifikasi`,a.`kddesa_pe`,
a.`namadesa_pe`,a.lintang, a.bujur, a.`harike3` AS h3,a.`harike14` AS h14,a.`harike28` AS h28,a.`primaquin14hari` AS pq14,a.`kematian` 
FROM imp_regmal1 a INNER JOIN mst_fasyankes b ON(a.`idfaskes`=b.`kd_fasyankes`) 
LEFT JOIN mst_kelurahan c ON(a.`kd_desa_kel`=c.`id_kelurahan`)
WHERE a.tahun_lap='$p->tahun'";
//DATE_FORMAT(a.tglkun,'%d/%m/%Y')

//level condition by harmi
if($p->periode=='2'){
    $sqlSTR .= " && a.bulankun='$p->bulan1'";
}
if($p->level=='1'){
    //$sqlSTR .="WHERE tahun_lap='$p->tahun'";
     $namakabs = "Semua Kab/Kota";
     $ListFasyankes = "Semua";
     $namaPropinsi = "Semua Propinsi";
}elseif($p->level=='2'){
    $sqlSTR .= " && idpropinsi='$p->propinsi'";
     $namakabs = "Semua Kab/Kota";
     $ListFasyankes = "Semua";
     $namaPropinsi = $row->nama_propinsi;
}elseif($p->level=='3'){
     $sqlSTR.= " && idkabupaten='$p->kabupaten'";
     $namakabs = $kray->nama_kabupaten;
      $namaPropinsi = $row->nama_propinsi;
     $ListFasyankes = "Semua";
}else{
    $namakabs = $kray->nama_kabupaten;
      $namaPropinsi = $row->nama_propinsi;
    $faskesImplode = implode("','",$p->faskes);
        $sqlSTR.= " && idfaskes in ('".$faskesImplode."')";
$fask = mysqli_query($dbconn,"select * from mst_fasyankes where kd_fasyankes IN ('".$faskesImplode."')");
while($lf=mysqli_fetch_array($fask)){
    $fasname[] = $lf['nama_fasyankes'];
}

$ListFasyankes = implode("<li>",$fasname);
}

$qu1 = mysqli_query($dbconn,$sqlSTR);
//echo "</br>sqlSTR99= ".$sqlSTR;
?>

<table class="tblInput" style="width: 100%">
    <thead>
        <tr>
            <th><h3>REGISTER PASIEN MALARIA</h3></th>
        </tr>
        <tr>
            <th>
                
                
                <table class="tblInput" style="width: 400px">
                    <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>: <?php echo $p->tahun;
							if($p->periode=='2'){
							?>| Bulan:<?php echo $p->bulan1;?></th> 
                            <?php } ?>
                        </tr>
                        <tr>
                            <th>Propinsi</th>
                            <th>: <?php echo $namaPropinsi;?></th>
                        </tr>
                        
                        <tr>
                            <th>Kabupaten/Kota</th>
                            <th>: <?php echo $namakabs;?></th>
                        </tr>
                        
                        <tr>
                            <th style="vertical-align: top">Fasyankes</th>
                            <th><ul><li><?php echo $ListFasyankes;?></ul></th>
                        </tr>
                    </thead>
                </table>
                
                
                
                
            </th>
            
        </tr>
        <tr>
            <th>
                
                <table class="tblListData" style="width: 3000px">
                    <thead>
                        <tr>
                            <th rowspan="3" style="width: 10px">No</th>
                            <th rowspan="3" style="width: 150px">Nama Faskes</th>
                            <th rowspan="3" style="width: 150px">Nik</th>
                            <th rowspan="3" style="width: 150px">No. KK</th>
                            <th rowspan="3" style="width: 50px">Jenis Penemuan</th>
                            <th rowspan="3" style="width: 150px">Nama Pasien</th>
                            <th colspan="2" style="width: 100px">Umur</th>
                            <th rowspan="3" style="width: 50px">Jenis </br>
                            Kelamin</th>
                            <th  rowspan="3" style="width: 50px">Hamil </br> 
                            
                            /Tidak</th>
                            <th colspan="2" style="width: 300px">alamat</th>
                            <th  rowspan="3" style="width: 50px">Bulan Kunjungan</th>
                            <th rowspan="3" style="width: 50px">Tgl Kunjungan</th>
                            <th rowspan="3" style="width: 100px">Pekerjaan</th>
                            <th rowspan="3" style="width: 100px">Konfirmasi Lab</th>
                            <th rowspan="3" style="width: 50px">Jenis Parasit</th>
                            <th rowspan="3" style="width: 100px">Derajat Penyakit Malaria</th>
                            <th colspan="8" style="width: 200px">Pengobatan (Jumlah Diterima)</th>
                            <th rowspan="3" style="width: 70px">Pasien RJ/RI</th>
                            <th rowspan="3" style="width: 70px">Faskes yg Menemukan</th>
                            <th rowspan="3" style="width: 100px">Dirujuk Ke</th>
                            <th colspan="7" style="width: 300px">Penyelidikan Epidemiologi</th>
                            <th colspan="4" style="width: 200px">Pemantauan Pengobatan</th>
                            <th rowspan="3" style="width: 100px">Kematian Akibat Malaria</th>
                        </tr>
                        <tr>
                            <th rowspan="2" style="width: 50px">Angka</th>
                              <th rowspan="2" style="width: 50px">Bulan/ Tahun</th>
                              <th rowspan="2" style="width: 200px">Dusun</th>
                              <th rowspan="2" style="width: 100px">Desa/ Kelurahan</th>
                               <th rowspan="2" style="width: 25px">DHP</th>
                               <th rowspan="2" style="width: 25px">Primaquin</th>
                               <th rowspan="2" style="width: 25px">Kina</th>
                               <th rowspan="2" style="width: 25px">Artesunat</th>
                               <th rowspan="2" style="width: 50px">Artemeter (inj)</th>
                               <th rowspan="2" style="width: 25px">Kina (inj)</th>
                               <th rowspan="2" style="width: 25px">Non Program</th>
                               <th rowspan="2" style="width: 25px">Pengobatan Standar</th>
                                <th rowspan="2" style="width: 70px">Tgl PE</th>
                               <th rowspan="2" style="width: 50px">Baru/ Relaps</th>
                                <th rowspan="2" style="width: 50px">Klasifikasi Penularan</th>
                               <th colspan="2" style="width: 200px">Tempat Penularan</th>
                               <th colspan="2" style="width: 200px">Titik Koordinat Tempat Penularan</th>
                               
                               <th rowspan="2" style="width: 25px">Hari ke 3</th>
                               <th rowspan="2" style="width: 25px">Hari ke 14</th>
                               <th rowspan="2" style="width: 25px">Hari Ke 28</th>
                               <th rowspan="2" style="width: 25px">Pengobatan Primaquin 14 hari</th>
                               
                        </tr>
                        <tr>
                            <th style="width: 100px">Desa</th>
                            <th style="width: 200px">Rt,Rw, Dusun, Kab, Provinsi</th>
                            <th style="width: 100px">Lintang</th>
                            <th style="width: 100px">Bujur</th>
                        </tr>
                    </thead>
                    
                    <tbody id="dataregmal">
                        <?php
                        $i=1;
                        while($sm=mysqli_fetch_object($qu1)){
                            ?>
                        <tr>
                        <td><?php echo $i;?></td>
                         <td><?php echo $sm->nama_fasyankes;?></td>
                         <td><?php echo $sm->nik;?></td>
                         <td><?php echo $sm->nkk;?></td>
                         <td><?php echo $sm->j_penemuan;?></td>
                         <td><?php echo $sm->namapasien;?></td>
                         <td><?php echo $sm->umur;?></td>
                         <td><?php echo $sm->th_bl;?></td>
                          <td><?php echo $sm->jnskel;?></td>
                            <td><?php echo $sm->hamil;?></td>
                              <td><?php echo $sm->alamat1;?></td>
                                <td><?php echo $sm->kel_desa;?></td>
                                  <td><?php echo $sm->bulankun;?></td>
                                    <td><?php echo $sm->tglkunj;?></td>
                                      <td><?php echo $sm->pekerjaan;?></td>
                                        <td><?php echo $sm->lab;?></td>
                                      <td><?php echo $sm->parasit;?></td>
                                        <td><?php echo $sm->derajat;?></td>
                                        
                                          <td><?php echo $sm->dhp;?></td>
                                           <td><?php echo $sm->primaquin;?></td>
                                            <td><?php echo $sm->kinatablet;?></td>
                                             <td><?php echo $sm->artesunat;?></td>
                                             <td><?php echo $sm->artemeter;?></td>
                                              <td><?php echo $sm->kinainj;?></td>
                                               <td><?php echo $sm->non_prog;?></td>
                                                <td><?php echo $sm->p_standar;?></td>
                                                
                                                 <td><?php echo $sm->p_rawat;?></td>
                                                  <td><?php echo $sm->dirujuk;?></td>
                                                   <td><?php echo $sm->dirujuk_ke;?></td>
                                                    <td><?php echo $sm->tgl_pe;?></td>
                                                     <td><?php echo $sm->baru_relaps;?></td>
                                                     <td><?php echo $sm->klasifikasi;?></td>
                                                     
                                                     <td><?php echo $sm->kddesa_pe;?></td>
                                                     <td><?php echo $sm->namadesa_pe;?></td>
                                                     
                                                     <td><?php echo $sm->lintang;?></td>
                                                     <td><?php echo $sm->bujur;?></td>
                                                     <td><?php echo $sm->h3;?></td>
                                                     <td><?php echo $sm->h14;?></td>
                                                     <td><?php echo $sm->h28;?></td>
                                                     <td><?php echo $sm->pq14;?></td>
                                                     <td><?php echo $sm->kematian;?></td>
                        </tr>
                          
                            <?php
                        $i++;
                        }
                        ?>
                      
                    </tbody>
                </table>
                
                
            </th>
        </tr>
    </thead>
</table>
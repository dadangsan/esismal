<head>
<style type="text/css">
.auto-style2 {
	text-align: right;
}
</style>
</head>

<script>
/* 
function tampilGambar(){
      document.getElementById('myLoadingGif').style.display = "block";
} */
</script>
<?php
session_start();
require_once("../config/database-connect.php");
$p = (object)$_POST;
$s = (object)$_SESSION;
$prop = "SELECT * FROM mst_propinsi where id_propinsi='$p->propinsi'";
$pray = mysqli_query($dbconn,$prop);
$row = mysqli_fetch_object($pray);

  $kabs = "SELECT * FROM mst_kabupaten where id_kabupaten='$p->kabupaten'";
  $krow = mysqli_query($dbconn,$kabs);
$kray = mysqli_fetch_object($krow);

//echo '<div id="container" ><img src="/esismalv2/loader/ajax-loader.gif" id="myLoadingGif" style= "display: none;"></div>';
$strViewRow4 = "SELECT a.user_log, 
SUM(IF(a.ksediaan_mikros='1',1,0)) AS adaMikroskopis,
SUM(IF(a.kompt_mikros = '1',1,0)) AS level1,
SUM(IF(a.kompt_mikros = '2',1,0)) AS level2,
SUM(IF(a.kompt_mikros = '3',1,0)) AS level3,
SUM(IF(a.kompt_mikros = '4',1,0)) AS level4,
SUM(IF(a.kompt_mikros = '5',1,0)) AS level5,
SUM(IF(a.kompt_mikros = '6',1,0)) AS level6, ";

if($p->level=='1'){ //nasional    
    $namaPropinsi = "Semua Propinsi";
    $strViewRow = $strViewRow4." b.id_propinsi, b.nama_propinsi AS AREA 
FROM ent_mikroskops a RIGHT JOIN mst_propinsi b 
ON(a.kd_prop=b.id_propinsi && a.tahun='$p->tahun' AND (RIGHT(a.id_mikros2,2)='/2' or RIGHT(a.id_mikros2,2)='/1')) 
 GROUP BY b.`id_propinsi`";
 //ON(a.kd_prop=b.id_propinsi && a.tahun='$p->tahun' && a.triwulan='$p->bulan1' AND (RIGHT(a.id_mikros2,2)='/2' or RIGHT(a.id_mikros2,2)='/1')) 
 
}elseif($p->level=='2'){ //radiobuttn propinsi      
	$namaPropinsi = $row->nama_propinsi;
	$namaKab = "Semua Kabupaten";
	$strViewRow = $strViewRow4." SUM(IF(a.hasil_uji_sil='Baik',1,0)) AS hasil3, 
FORMAT((SUM(IF(a.hasil_uji_sil='Baik',1,0)) / SUM(IF(a.ksediaan_mikros='1',1,0)) * 100),2) AS persenBaik, b.id_kabupaten, b.nama_kabupaten AS AREA
FROM ent_mikroskops a RIGHT JOIN mst_kabupaten b 
ON(a.kd_kab=b.id_kabupaten && a.tahun='$p->tahun' && b.id_propinsi='$p->propinsi' AND (RIGHT(a.id_mikros2,2)='/3')) 
 WHERE b.id_propinsi='$p->propinsi' 
 GROUP BY b.`id_kabupaten`"; 	
 //ON(a.kd_kab=b.id_kabupaten && a.tahun='$p->tahun' && a.triwulan='$p->bulan1' && b.id_propinsi='$p->propinsi' AND (RIGHT(a.id_mikros2,2)='/3')) 
}
?>

<table class="tblInput" style="width: 100%">
    <thead>
        <tr>
		<?php 
		if ($p->level=='2'){  
		?>
            <th><h3>Laporan CrossChecker Agregat per-Kabupaten</h3></th>
		<?php 
		} // elseif ($_SESSION['id_group'] ==1 or $_SESSION['id_group'] ==2 or $_SESSION['id_group'] ==7){ 		
		elseif ($p->level=='1'){
		?>
		<th><h3>Laporan CrossChecker Agregat per-Propinsi</h3></th>
		<?php } ?>
        </tr>
        <tr>
            <th>
                
                
                <table width="500" class="tblInput" style="width: 400px">
                    <thead>
                        <tr>
                            <th width="131" class="auto-style2">Tahun :</th>
                            <th style="width: 300px">&nbsp;<?php echo $p->tahun;
							if($p->periode=='2'){
							?>| Triwulan:<?php echo $p->bulan1;?></th> 
                            <?php } ?>
                            
                            
                        </tr>
                        <tr>
                            <th class="auto-style2" style="height: 23px">Propinsi 
							:</th>
                            <th style="height: 23px; width: 300px;">&nbsp;<?php echo $namaPropinsi;?></th>
                        </tr>
                        
                        <tr>
                            <th class="auto-style2">Kabupaten/Kota :</th>
                            <th style="width: 300px">&nbsp;<?php echo $namaKab;?></th>
                        </tr> 
                        
                        <!-- <tr>
                            <th style="vertical-align: top" class="auto-style2">Fasyankes 
							:</th>
                            <th style="width: 300px">&nbsp;<?php // echo $namaFaskes;?></th>
                        </tr> -->
                    </thead>
                </table>
            </th>
            
        </tr>
        <!-- <tr>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>
               
                
            </th>
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr> -->
        <tr>
            <th>
                
                <table class="tblListData" style="width: 100%">
                    <thead>
					<?php
					if ($p->level=='2'){
					?>
                        <tr>
                            <!-- <th width="132" rowspan="1" style="width: 20px">No</th> -->
                            <th rowspan="2" style="width: 4%">No</th> 
                            <th rowspan="2"  style="width: 20%">Wilayah</th>                            
                            <th rowspan="2"  style="width: 5%">Jum Faskes</th>
                            <th rowspan="2"  style="width: 5%">Jum CrossChecker</th>
                            
                          <th colspan="6" rowspan="1"  style="width: 5%">Jum CrossChecker Berdasarkan Kompetensi</th>
							<th rowspan="2"  style="width: 5%">Jum CrossChecker dg hasil Uji Silang Baik</th>
                            <th rowspan="2"  style="width: 5%">% CrossChecker dg Hasil  Uji Silang  Baik</th>                   
                        </tr>
                        
                        <tr>
                            <!-- <th width="132" rowspan="1" style="width: 20px">No</th> -->
                          <th rowspan="1"  style="width: 5%">Level1</th>
							<th rowspan="1"  style="width: 5%"><span style="width: 5px">Level2</span></th>
							<th rowspan="1"  style="width: 5%"><span style="width: 5px">Level3</span></th>
                            <th rowspan="1"  style="width: 5%"><span style="width: 5px">Level4</span></th>
                            <th rowspan="1"  style="width: 5%"><span style="width: 5px">Belum Diketahui</span></th>
                            <th rowspan="1"  style="width: 5%"><span style="width: 5px">Belum Terlatih</span></th>
                        </tr>
					<?php } elseif ($p->level=='1'){ ?> 
					 <tr>
                            <!-- <th width="132" rowspan="1" style="width: 20px">No</th> -->
                            <th rowspan="2" style="width: 4%">No</th> 
                            <th rowspan="2"  style="width: 20%">Wilayah</th>                            
                            <th rowspan="2"  style="width: 5%">Jum Faskes</th>
                            <th rowspan="2"  style="width: 5%">Jum CrossChecker</th>
                            
                          <th colspan="6" rowspan="1"  style="width: 5%">Jum CrossChecker Berdasarkan Kompetensi</th>
                        </tr>
                        
                        <tr>
                            <!-- <th width="132" rowspan="1" style="width: 20px">No</th> -->
                          <th rowspan="1"  style="width: 5%">Level1</th>
							<th rowspan="1"  style="width: 5%"><span style="width: 5px">Level2</span></th>
							<th rowspan="1"  style="width: 5%"><span style="width: 5px">Level3</span></th>
                            <th rowspan="1"  style="width: 5%"><span style="width: 5px">Level4</span></th>
                            <th rowspan="1"  style="width: 5%"><span style="width: 5px">Belum Diketahui</span></th>
                            <th rowspan="1"  style="width: 5%"><span style="width: 5px">Belum Terlatih</span></th>
                        </tr>
					<?php } elseif ($p->level=='3'){ ?>   
						
						<tr>
                            <!-- <th width="132" rowspan="1" style="width: 20px">No</th> -->
                            <th rowspan="1" style="width: 4%">No</th> 
                            <th rowspan="1"  style="width: 20%">Wilayah</th>                            
                            <th colspan="1"  style="width: 5%">Mikroskopis</th>
							<th colspan="1"  style="width: 15%"><span style="width: 5px">Nama Mikroskopis</span></th>
							<th colspan="1"  style="width: 5%"><span style="width: 5px">Komp. Mikroskopis</span></th>
                            <th colspan="1"  style="width: 5%"><span style="width: 5px">Sensitivitas</span></th>
                            <th colspan="1"  style="width: 5%"><span style="width: 5px">Spesivitas</span></th>
                            <th colspan="1"  style="width: 5%"><span style="width: 5px">Akurasi Spesiemen</span></th>
                            <th colspan="1"  style="width: 5%"><span style="width: 5px">HASIL</span></th>
                        </tr>
					<?php } ?>	
                    </thead>
                    <tbody>
                                              

<?php
/* if ($p->level=='3'){ //radioButton Kab
$queView = mysqli_query($dbconn,$strViewRow);
                        $i=1;
											
						while($rowView=mysqli_fetch_object($queView)){ 							
						    
                            ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rowView->area;?></td>
                            <td><?php echo $rowView->sediamikroskop;	 ?></td>
							<td><?php //echo "strViewRow= ".$strViewRow;
							echo $rowView->namapetugas; ?></td>
                          <td><?php echo $rowView->komptmikros2;  ?></td>
                          <td><?php echo $rowView->sensi2; ?></td>
                          <td><?php echo $rowView->spesi2; ?></td>
                          <td><?php echo $rowView->akurasi2; ?></td>						  
                          <td><?php echo $rowView->hasil2; ?></td>                                                    
                        </tr>
						<?php $i++;						
                        } 
} */
// else {

 //if ($p->level=='1' or $p->level=='2'){ 
//echo "<br/>strViewRow99= ".$strViewRow;   
						$queView = mysqli_query($dbconn,$strViewRow);
                        $i=1;
						//dadang, tambahkan loader
						//echo '<div id="container" ><img src="/esismalv2/loader/ajax-loader.gif" id="myLoadingGif" style= "display: none;"></div>';
						
						// dadang, khusus radiobutton kab dan faskes.
						while($rowView=mysqli_fetch_object($queView)){ 
							$kodekab2 = $rowView->id_kabupaten;
							$kodeprop2 = $rowView->id_propinsi;
							
                        // dadang, akhir khusus radiobutton kab dan faskes.						
                            
                            ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rowView->AREA;?></td>
                             <td>
							 <?php 
							 if ($p->level=='2') {
									$kab3 = "SELECT count(kd_fasyankes) as jumfaskes FROM mst_fasyankes WHERE kabupaten=$kodekab2";
							 }elseif ($p->level=='1') {
								 $kab3 = "SELECT count(kd_fasyankes) as jumfaskes FROM mst_fasyankes WHERE propinsi=$kodeprop2";
							 }
							 							 							 
							 $quekab3 = mysqli_query($dbconn,$kab3);
							 while($jumfaskes3=mysqli_fetch_object($quekab3)) {
								 $jumfks=$jumfaskes3->jumfaskes;
								 echo $jumfks;
								 }
							 ?>							 
							 </td>
                          <td><?php 
						  
	$mikros3=$rowView->adaMikroskopis;			
	echo $mikros3;
						  ?></td>
                         
                          <td><?php 		
	$level1=$rowView->level1; 
	echo $level1;    
						  ?></td>
                          <td><?php 	
	$level2=$rowView->level2;    
	echo $level2;    
						  ?></td>
                          <td><?php 
   $level3=$rowView->level3;
   echo $level3;
						  ?></td>
                          <td><?php 	
	$level4=$rowView->level4;
	echo $level4;
						  ?></td>
                          <td><?php 
  $level5=$rowView->level5;
  echo $level5;
						  ?></td>
                          <td><?php 
  $level6=$rowView->level6;
  echo $level6;
						  ?></td>
						  
						  <?php if ($p->level=='2'){ ?>
						  <td><?php 	
	$hasil3=$rowView->hasil3; 
	echo $hasil3; 
						  ?></td>						  
                          <td><?php 	
	$persenbaik=$rowView->persenBaik; 
	echo $persenbaik; 
						  ?></td>						  
						  <?php } ?>
						  
                          
                          
                          
                        </tr>
						<?php $i++;
						$post01=$post01+$jumfks;
						$post02=$post02+$mikros3;
						$post03=$post03+$level1;
						$post04=$post04+$level2;
						$post05=$post05+$level3;
						$post06=$post06+$level4;
						$post07=$post07+$level5;
						$post08=$post08+$level6;
						$post09=$post09+$hasil3;						
						//$post10=$post09/$post02*100;
                        } ?>
                        <!-- BARIS TOTAL -->
						<tr style="background: #3EDD6E">
						<td ></td>
						<td >TOTAL</td>
						<td ><?php echo $post01;?></td>
                        <td ><?php echo $post02;?></td>		
                        <td ><?php echo $post03;?></td>
                        <td ><?php echo $post04;?></td>		
                        <td ><?php echo $post05;?></td>
                        <td ><?php echo $post06;?></td>		
                        <td ><?php echo $post07;?></td>
                        <td ><?php echo $post08;?></td>		
						
						<?php if ($p->level=='2'){ ?>
                        <td ><?php echo $post09;?></td>
                        <td ><?php 
						if ($post02==0){
							echo '';
						}else {
							echo number_format($post09/$post02*100,2);
						}
						?></td>		
                        <?php } ?>
                        					
						</tr>
                    
<?php 
// }

 ?>
					
                    </tbody>
                    
                </table>
                
                
            </th>
        </tr>
    </thead>
</table>
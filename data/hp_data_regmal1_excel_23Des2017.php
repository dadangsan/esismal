<?php
session_start();

require_once("../config/database-connect.php");
$datetime = date("YmdHis");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("content-disposition: attachment;filename=Lap_Regmal1_$datetime.xls");
header('Cache-Control: max-age=0'); 

$p = (object)$_GET;


$s = (object)$_SESSION;


$prop = "SELECT * FROM mst_propinsi where id_propinsi='$p->propinsi'";
$pray = mysqli_query($dbconn,$prop);
$row = mysqli_fetch_object($pray);

  $kabs = "SELECT * FROM mst_kabupaten where id_kabupaten='$p->kabupaten'";
  $krow = mysqli_query($dbconn,$kabs);
$kray = mysqli_fetch_object($krow);
  

/*if($p->kabupaten!=''){
    $namakabs = $kray->nama_kabupaten;
}else{
    $namakabs = "Semua Kab/Kota";
}
if(!isset($p->fasyankes)){
    $ListFasyankes = "Semua";
}
*/

$sqlSTR = "SELECT b.`nama_fasyankes`,a.`idfaskes`,a.`nik`,a.`nkk`,a.`j_penemuan`,a.`nama_pende` AS namapasien,a.`umur`,a.`th_bl`,a.`jnskel`,a.`hamil`,a.`alamat1`,IF(c.`id_kelurahan` IS NULL,a.kd_desa_kel,c.nama_kelurahan) AS kel_desa,a.bulankun,DATE_FORMAT(a.tglkun,'%d/%m/%Y') AS tglkunj,a.pekerjaan,a.lab,a.`parasit`,a.`kondisi`,a.`dhp`,a.`primaquin`,a.`kinatablet`,a.`artesunat`,a.`artemeter`,a.`kinainj`,a.`non_act`,a.`peng_act`,a.`rawat_pend`,a.`rujuk_dari_asal_penemuan` AS dirujuk,a.`dirujuk_ke`,a.`pex`,a.`klasifikasi`,a.`kddesa_tmpt_penularan`,a.`namadesa_tmpt_penularan`,a.`harike3` AS h3,a.`harike14` AS h14,a.`harike28` AS h28,a.`primaquin14hari` AS pq14,a.`hasilpeng` FROM imp_regmal1 a
INNER JOIN mst_fasyankes b ON(a.`idfaskes`=b.`kd_fasyankes`)
LEFT JOIN mst_kelurahan c ON(a.`kd_desa_kel`=c.`id_kelurahan`) ";

//level condition by harmi
if($p->level=='1'){
    $sqlSTR .="WHERE tahun_lap='$p->tahun'";
     $namakabs = "Semua Kab/Kota";
     $ListFasyankes = "Semua";
     $namaPropinsi = "Semua Propinsi";
}elseif($p->level=='2'){
    $sqlSTR .= "WHERE tahun_lap='$p->tahun' && idpropinsi='$p->propinsi'";
     $namakabs = "Semua Kab/Kota";
     $ListFasyankes = "Semua";
     $namaPropinsi = $row->nama_propinsi;
}elseif($p->level=='3'){
     $sqlSTR.= "WHERE idkabupaten='$p->kabupaten' && tahun_lap='$p->tahun'";
     $namakabs = $kray->nama_kabupaten;
      $namaPropinsi = $row->nama_propinsi;
     $ListFasyankes = "Semua";
}else{
    $namakabs = $kray->nama_kabupaten;
      $namaPropinsi = $row->nama_propinsi;
      $fks = explode(",",$p->faskes);
    $faskesImplode = implode("','",$fks);
        $sqlSTR.= "WHERE idfaskes in ('".$faskesImplode."') && tahun_lap='$p->tahun'";
$fask = mysqli_query($dbconn,"select * from mst_fasyankes where kd_fasyankes IN ('".$faskesImplode."')");
while($lf=mysqli_fetch_array($fask)){
    $fasname[] = $lf['nama_fasyankes'];
}

$ListFasyankes = implode("<li>",$fasname);
}


/*if($p->kabupaten!=''){
    if(isset($p->faskes)){
        $faskesImplode = implode("','",$p->faskes);
        $sqlSTR.= "WHERE idfaskes in ('".$faskesImplode."') && tahun_lap='$p->tahun'";
    }else{
        $sqlSTR.= "WHERE idkabupaten='$p->kabupaten' && tahun_lap='$p->tahun'";
        
    }
}else{
    $sqlSTR.= "WHERE idpropinsi='$p->propinsi' && tahun_lap='$p->tahun'";
}*/

//echo $sqlSTR;
$qu1 = mysqli_query($dbconn,$sqlSTR);


?>

<table class="tblInput" style="width: 100%">
    <thead>
        <tr>
            <th><h3>REGISTER PASIEN MALARIA</h3></th>
        </tr>
        <tr>
            <th>
                
                
                <table class="tblInput" style="width: 400px">
                    <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>: <?php echo $p->tahun;?></th>
                        </tr>
                        <tr>
                            <th>Propinsi</th>
                            <th>: <?php echo $namaPropinsi;?></th>
                        </tr>
                        
                        <tr>
                            <th>Kabupaten/Kota</th>
                            <th>: <?php echo $namakabs;?></th>
                        </tr>
                        
                        <tr>
                            <th style="vertical-align: top">Fasyankes</th>
                            <th><ul><li><?php echo $ListFasyankes;?></ul></th>
                        </tr>
                    </thead>
                </table>
                
                
                
                
            </th>
            
        </tr>
        <tr>
            <th>
                
                <table class="tblListData" style="width: 3000px;border: solid">
                    <thead>
                        <tr>
                            <th rowspan="3" style="width: 10px;border: solid">No</th>
                            <th rowspan="3" style="width: 150px;border: solid">Nama Faskes</th>
                            <th rowspan="3" style="width: 150px;border: solid">Nik</th>
                            <th rowspan="3" style="width: 150px;border: solid">No. KK</th>
                            <th rowspan="3" style="width: 50px;border: solid">Jenis Penemuan</th>
                            <th rowspan="3" style="width: 150px;border: solid">Nama Pasien</th>
                            <th colspan="2" style="width: 100px;border-bottom: none">Umur</th>
                            <th rowspan="3" style="width: 100px;border: solid">Jenis Kelamin</th>
                            <th  rowspan="3" style="width: 100px;border: solid">Hamil/Tidak</th>
                            <th colspan="2" style="width: 300px;border: solid">alamat</th>
                            <th  rowspan="3" style="width: 50px;border: solid">Bulan Kunjungan</th>
                            <th rowspan="3" style="width: 50px;border: solid">Tgl Kunjungan</th>
                            <th rowspan="3" style="width: 100px;border: solid">Pekerjaan</th>
                            <th rowspan="3" style="width: 100px;border: solid">Konfirmasi Lab</th>
                            <th rowspan="3" style="width: 100px;border: solid">Parasit</th>
                            <th rowspan="3" style="width: 100px;border: solid">Kondisi Pasien</th>
                            <th colspan="8" style="width: 200px;border: solid">Pengobatan (Jumlah Diterima)</th>
                            <th rowspan="3" style="width: 100px;border: solid">Pasien RJ/RI</th>
                            <th rowspan="3" style="width: 100px;border: solid">Rujukan Dari</th>
                            <th rowspan="3" style="width: 100px;border: solid">Dirujuk Ke</th>
                            <th colspan="4" style="width: 300px;border: solid">Penyelidikan Epidemiologi</th>
                            <th colspan="4" style="width: 200px;border: solid">Penambahan Pengobatan</th>
                            <th rowspan="3" style="width: 100px;border: solid">Hasil Pengobatan</th>
                        </tr>
                        <tr>
                            <th rowspan="2" style="width: 50px;border-top: none;border-bottom: solid">&nbsp;</th>
                              <th rowspan="2" style="width: 50px;border-top:none;border-bottom: solid">&nbsp;</th>
                              <th rowspan="2" style="width: 200px;border: solid">Domisili</th>
                              <th rowspan="2" style="width: 100px;border: solid">Wilayah</th>
                               <th rowspan="2" style="width: 25px;border: solid">DHP</th>
                               <th rowspan="2" style="width: 25px;border: solid">Primaquin</th>
                               <th rowspan="2" style="width: 25px;border: solid">Kina</th>
                               <th rowspan="2" style="width: 25px;border: solid">Artesunat</th>
                               <th rowspan="2" style="width: 50px;border: solid">Artemeter (inj)</th>
                               <th rowspan="2" style="width: 25px;border: solid">Kina (inj)</th>
                               <th rowspan="2" style="width: 25px;border: solid">Non ACT</th>
                               <th rowspan="2" style="width: 25px;border: solid">Pengobatan ACT</th>
                                <th rowspan="2" style="width: 50px;border: solid">Ya/Tidak</th>
                               <th rowspan="2" style="width: 50px;border: solid">Klasifikasi Penularan</th>
                               <th colspan="2" style="width: 200px">Tempat Penularan</th>
                               <th rowspan="2" style="width: 25px;border: solid">Hari ke 3</th>
                               <th rowspan="2" style="width: 25px;border: solid">Hari ke 14</th>
                               <th rowspan="2" style="width: 25px;border: solid">Hari Ke 28</th>
                               <th rowspan="2" style="width: 25px;border: solid">Pengobatan Primaquin 14 hari</th>
                               
                        </tr>
                        <tr>
                            <th style="width: 100px;border: solid">Wilayah</th>
                            <th style="width: 100px;border: solid">Nama Dusun/Desa/Kabupaten/Propinsi</th>
                        </tr>
                    </thead>
                    
                    <tbody id="dataregmal">
                        <?php
                        $i=1;
                        while($sm=mysqli_fetch_object($qu1)){
                            ?>
                        <tr style="border: solid">
                        <td><?php echo $i;?></td>
                         <td><?php echo $sm->nama_fasyankes;?></td>
                         <td><?php echo $sm->nik;?></td>
                         <td><?php echo $sm->nkk;?></td>
                         <td><?php echo $sm->j_penemuan;?></td>
                         <td><?php echo $sm->namapasien;?></td>
                         <td><?php echo $sm->umur;?></td>
                         <td><?php echo $sm->th_bl;?></td>
                          <td><?php echo $sm->jnskel;?></td>
                            <td><?php echo $sm->hamil;?></td>
                              <td><?php echo $sm->alamat1;?></td>
                                <td><?php echo $sm->kel_desa;?></td>
                                  <td><?php echo $sm->bulankun;?></td>
                                    <td><?php echo $sm->tglkunj;?></td>
                                      <td><?php echo $sm->pekerjaan;?></td>
                                        <td><?php echo $sm->lab;?></td>
                                      <td><?php echo $sm->parasit;?></td>
                                        <td><?php echo $sm->kondisi;?></td>
                                        
                                          <td><?php echo $sm->dhp;?></td>
                                           <td><?php echo $sm->primaquin;?></td>
                                            <td><?php echo $sm->kinatablet;?></td>
                                             <td><?php echo $sm->artesunat;?></td>
                                             <td><?php echo $sm->artemeter;?></td>
                                              <td><?php echo $sm->kinainj;?></td>
                                               <td><?php echo $sm->non_act;?></td>
                                                <td><?php echo $sm->peng_act;?></td>
                                                
                                                 <td><?php echo $sm->rawat_pend;?></td>
                                                  <td><?php echo $sm->dirujuk;?></td>
                                                   <td><?php echo $sm->dirujuk_ke;?></td>
                                                    <td><?php echo $sm->pex;?></td>
                                                     <td><?php echo $sm->klasifikasi;?></td>
                                                     
                                                     <td><?php echo $sm->kddesa_tmpt_penularan;?></td>
                                                     <td><?php echo $sm->namadesa_tmpt_penularan;?></td>
                                                     
                                                     <td><?php echo $sm->h3;?></td>
                                                     <td><?php echo $sm->h14;?></td>
                                                     <td><?php echo $sm->h28;?></td>
                                                     <td><?php echo $sm->pq14;?></td>
                                                     <td><?php echo $sm->hasilpeng;?></td>
                        </tr>
                          
                            <?php
                        $i++;
                        }
                        ?>
                      
                    </tbody>
                </table>
                
                
            </th>
        </tr>
    </thead>
</table>
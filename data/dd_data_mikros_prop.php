<head>
<style type="text/css">
.auto-style2 {
	text-align: right;
}
</style>
</head>

<script>
/* 
function tampilGambar(){
      document.getElementById('myLoadingGif').style.display = "block";
} */
</script>
<?php
session_start();
require_once("../config/database-connect.php");
$p = (object)$_POST;
$s = (object)$_SESSION;
$prop = "SELECT * FROM mst_propinsi where id_propinsi='$p->propinsi'";
$pray = mysqli_query($dbconn,$prop);
$row = mysqli_fetch_object($pray);

  $kabs = "SELECT * FROM mst_kabupaten where id_kabupaten='$p->kabupaten'";
  $krow = mysqli_query($dbconn,$kabs);
$kray = mysqli_fetch_object($krow);

//echo '<div id="container" ><img src="/esismalv2/loader/ajax-loader.gif" id="myLoadingGif" style= "display: none;"></div>';
$strViewRow4 = "SELECT a.user_log, 
SUM(IF(a.kirimsediaan='Ya',1,IF(a.kirimsediaan='Tidak',0,''))) AS kirimsed,
SUM(IF(a.ksediaan_mikros='1',1,0)) AS adaMikroskopis,
SUM(IF(a.kompt_mikros = '1',1,0)) AS level1,
SUM(IF(a.kompt_mikros = '2',1,0)) AS level2,
SUM(IF(a.kompt_mikros = '3',1,0)) AS level3,
SUM(IF(a.kompt_mikros = '4',1,0)) AS level4,
SUM(IF(a.kompt_mikros = '5',1,0)) AS level5,
SUM(IF(a.kompt_mikros = '6',1,0)) AS level6,
SUM(IF(a.hasil_uji_sil='Baik',1,0)) AS hasil3, 
FORMAT((SUM(IF(a.hasil_uji_sil='Baik',1,0)) / SUM(IF(a.ksediaan_mikros=1,IF(a.kirimsediaan='Ya',1,0),0)) * 100),2) AS persenBaik, ";

if($p->level=='1'){ //nasional    
    $namaPropinsi = "Semua Propinsi";
    $strViewRow = $strViewRow4." b.id_propinsi, b.nama_propinsi AS AREA 
FROM ent_mikroskops a RIGHT JOIN mst_propinsi b 
ON(a.kd_prop=b.id_propinsi && a.tahun='$p->tahun' && a.triwulan='$p->bulan1' AND (RIGHT(a.id_mikros2,2)='/4'))
 GROUP BY b.`id_propinsi`";
 $strTotPersenBaik = "SELECT FORMAT((SUM(IF(a.hasil_uji_sil='Baik',1,0)) / SUM(IF(a.ksediaan_mikros=1,IF(a.kirimsediaan='Ya',1,0),0)) * 100),2) AS persenBaik 
FROM ent_mikroskops a WHERE a.tahun='$p->tahun' && a.triwulan='$p->bulan1' 
AND (RIGHT(a.id_mikros2,2)='/4')";
 
}elseif($p->level=='2'){ //radiobuttn propinsi      
	$namaPropinsi = $row->nama_propinsi;
	$namaKab = "Semua Kabupaten";
	$strViewRow = $strViewRow4." b.id_kabupaten, b.nama_kabupaten AS AREA
FROM ent_mikroskops a RIGHT JOIN mst_kabupaten b 
ON(a.kd_kab=b.id_kabupaten && a.tahun='$p->tahun' && a.triwulan='$p->bulan1' && b.id_propinsi='$p->propinsi' AND (RIGHT(a.id_mikros2,2)='/4')) 
 WHERE b.id_propinsi='$p->propinsi' 
 GROUP BY b.`id_kabupaten`";
 $strTotPersenBaik = "SELECT FORMAT((SUM(IF(a.hasil_uji_sil='Baik',1,0)) / SUM(IF(a.ksediaan_mikros=1,IF(a.kirimsediaan='Ya',1,0),0)) * 100),2) AS persenBaik 
FROM ent_mikroskops a WHERE a.kd_prop='$p->propinsi' AND a.tahun='$p->tahun' && a.triwulan='$p->bulan1' 
AND (RIGHT(a.id_mikros2,2)='/4')";

 	
}elseif ($p->level=='3'){     
$namaPropinsi = $row->nama_propinsi;
    $namaKab = $kray->nama_kabupaten;     
   $strViewRow ="SELECT a.kirimsediaan, IF(a.ksediaan_mikros='1','Tersedia', IF(a.ksediaan_mikros='2','Tidak Tersedia','')) AS sediamikroskop, IF(a.kompt_mikros='1','Level1', IF(a.kompt_mikros='2','Level2', IF(a.kompt_mikros='3','Level3', IF(a.kompt_mikros='4','Level4', IF(a.kompt_mikros='5','Belum Diketahui', ''))))) AS komptmikros2, IF(a.sensitivitas1='0','<70',IF(a.sensitivitas1='1','>=70','')) AS sensi2, IF(a.spesivitas1='0','<70',IF(a.spesivitas1='1','>=70','')) AS spesi2, IF(a.akurasi_spes='0','<70', IF(a.akurasi_spes='1','>=70','')) AS akurasi2, a.hasil_uji_sil AS hasil2, b.kd_fasyankes, b.nama_fasyankes as area FROM ent_mikroskops a right join mst_fasyankes b On(a.kd_faskes=b.kd_fasyankes && tahun='$p->tahun') && triwulan='$p->bulan1' WHERE b.kabupaten='$p->kabupaten' ";
   
	  
}

//echo $strView;
//echo $strViewRow;
?>

<table class="tblInput" style="width: 100%">
    <thead>
        <tr>
		<?php 
		if ($_SESSION['id_group'] ==3){ 
		?>
            <th><h3>Laporan Rekapitulasi Mikroskopis Propinsi</h3></th>
		<?php 
		} elseif ($_SESSION['id_group'] ==1 or $_SESSION['id_group'] ==2){ 		
		?>
		<th><h3>Laporan Rekapitulasi Mikroskopis Nasional</h3></th>
		<?php } ?>
        </tr>
        <tr>
            <th>
                
                
                <table width="500" class="tblInput" style="width: 400px">
                    <thead>
                        <tr>
                            <th width="131" class="auto-style2">Tahun :</th>
                            <th style="width: 300px">&nbsp;<?php echo $p->tahun;
							if($p->periode=='2'){
							?>| Triwulan:<?php echo $p->bulan1;?></th> 
                            <?php } ?>
                            
                            
                        </tr>
                        <tr>
                            <th class="auto-style2" style="height: 23px">Propinsi 
							:</th>
                            <th style="height: 23px; width: 300px;">&nbsp;<?php echo $namaPropinsi;?></th>
                        </tr>
                        
                        <tr>
                            <th class="auto-style2">Kabupaten/Kota :</th>
                            <th style="width: 300px">&nbsp;<?php echo $namaKab;?></th>
                        </tr> 
                        
                        <!-- <tr>
                            <th style="vertical-align: top" class="auto-style2">Fasyankes 
							:</th>
                            <th style="width: 300px">&nbsp;<?php // echo $namaFaskes;?></th>
                        </tr> -->
                    </thead>
                </table>
            </th>
            
        </tr>
        <!-- <tr>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>
               
                
            </th>
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr> -->
        <tr>
            <th>
                
                <table class="tblListData" style="width: 100%">
                    <thead>
					<?php
					if ($p->level=='1' or $p->level=='2'){
					?>
                        <tr>
                            <!-- <th width="132" rowspan="1" style="width: 20px">No</th> -->
                            <th rowspan="2" style="width: 4%">No</th> 
                            <th rowspan="2"  style="width: 20%">Wilayah</th>                            
                            <th rowspan="2"  style="width: 5%">Jumlah Faskes</th>
							<th rowspan="2"  style="width: 5%">Jumlah Faskes Tersedia Mikroskopis</th>
                            <th rowspan="2"  style="width: 5%">Jum Faskes yang mengirimkan uji silang</th>
                            
                        <!--  <th colspan="6" rowspan="1"  style="width: 5%">Jumlah Mikroskopis Berdasarkan Kompetensi</th> -->
							<th rowspan="2"  style="width: 5%">Jum Faskes dg hasil Uji Silang Baik</th>
                            <th rowspan="2"  style="width: 5%">% Mikroskopis dg Hasil  Uji Silang  Baik</th>                   
                        </tr>
                        
                        <!-- <tr>                            
                          <th rowspan="1"  style="width: 5%">Level1</th>
							<th rowspan="1"  style="width: 5%">Level2</th>
							<th rowspan="1"  style="width: 5%">Level3</th>
                            <th rowspan="1"  style="width: 5%">Level4</th>
                            <th rowspan="1"  style="width: 5%">Belum Diketahui</th>
                            <th rowspan="1"  style="width: 5%">Belum Terlatih</th>
                        </tr> -->
					<?php }elseif ($p->level=='3'){ //radiobutton Kab ?>   
						
						<tr>
                            <!-- <th width="132" rowspan="1" style="width: 20px">No</th> -->
                            <th rowspan="1" style="width: 3%">No</th> 
                            <th rowspan="1"  style="width: 25%">Wilayah</th>                            
                            <th colspan="1"  style="width: 3%">Mikroskopis</th>
							<th colspan="1"  style="width: 3%">Kirim Sediaan</th> 
							<!-- <th colspan="1"  style="width: 5%"><span style="width: 5px">Komp. Mikroskopis</span></th> -->
                            <th colspan="1"  style="width: 3%">Sensitivitas</th>
                            <th colspan="1"  style="width: 3%">Spesivitas</th>
                            <th colspan="1"  style="width: 3%">Akurasi Spesiemen</th>
                            <th colspan="1"  style="width: 5%">HASIL</th>
                        </tr>
					<?php } ?>	
                    </thead>
                    <tbody>
                                              

<?php
if ($p->level=='1' or $p->level=='2'){ 
//echo "<br/>strViewRow99= ".$strViewRow;   
						$queView = mysqli_query($dbconn,$strViewRow);
                        $i=1;
						//dadang, tambahkan loader
						//echo '<div id="container" ><img src="/esismalv2/loader/ajax-loader.gif" id="myLoadingGif" style= "display: none;"></div>';
						
						// dadang, khusus radiobutton kab dan faskes.
						while($rowView=mysqli_fetch_object($queView)){ 
							$kodekab2 = $rowView->id_kabupaten;
							$kodeprop2 = $rowView->id_propinsi;
							
                        // dadang, akhir khusus radiobutton kab dan faskes.						
                            
                            ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rowView->AREA;?></td>
                             <td>
							 <?php 
							 if ($p->level=='2') {
									$kab3 = "SELECT count(kd_fasyankes) as jumfaskes FROM mst_fasyankes WHERE kabupaten=$kodekab2";
							 }elseif ($p->level=='1') {
								 $kab3 = "SELECT count(kd_fasyankes) as jumfaskes FROM mst_fasyankes WHERE propinsi=$kodeprop2";
							 }
							 							 							 
							 $quekab3 = mysqli_query($dbconn,$kab3);
							 while($jumfaskes3=mysqli_fetch_object($quekab3)) {
								 $jumfks=$jumfaskes3->jumfaskes;
								 echo $jumfks;
								 }
							 ?>							 
							 </td>
                          <td><?php 
						  
	$mikros3=$rowView->adaMikroskopis;			
	echo $mikros3;
						  ?></td>
						  <td><?php 
						  
	$kirimsed3=$rowView->kirimsed;			
	echo $kirimsed3;
						  ?></td>
                         
                        <!--  <td><?php 		
	$level1=$rowView->level1; 
	echo $level1;    
						  ?></td>
                          <td><?php 	
	$level2=$rowView->level2;    
	echo $level2;    
						  ?></td>
                          <td><?php 
   $level3=$rowView->level3;
   echo $level3;
						  ?></td>
                          <td><?php 	
	$level4=$rowView->level4;
	echo $level4;
						  ?></td>
                          <td><?php 
  $level5=$rowView->level5;
  echo $level5;
						  ?></td>
                          <td><?php 
  $level6=$rowView->level6;
  echo $level6;
						  ?></td> -->
                          <td><?php 	
	$hasil3=$rowView->hasil3; 
	echo $hasil3; 
						  ?></td>						  
                          <td><?php 	
	$persenbaik=$rowView->persenBaik; 
	echo $persenbaik; 
						  ?></td>
                          
                          
                        </tr>
						<?php $i++;
						$post01=$post01+$jumfks;
						$post02=$post02+$mikros3;
						$postsed3=$postsed3+$kirimsed3;
						/* $post03=$post03+$level1;
						$post04=$post04+$level2;
						$post05=$post05+$level3;
						$post06=$post06+$level4;
						$post07=$post07+$level5;
						$post08=$post08+$level6; */
						$post09=$post09+$hasil3;	
						
						//$post10=$post09/$post02*100;
                        } ?>
                        <!-- BARIS TOTAL -->
						<tr style="background: #3EDD6E">
						<td ></td>
						<td >TOTAL</td>
						<td ><?php echo $post01;?></td>
                        <td ><?php echo $post02;?></td>	
						<td ><?php echo $postsed3;?></td>							
                        <!-- <td ><?php echo $post03;?></td>
                        <td ><?php echo $post04;?></td>		
                        <td ><?php echo $post05;?></td>
                        <td ><?php echo $post06;?></td>		
                        <td ><?php echo $post07;?></td>
                        <td ><?php echo $post08;?></td>	-->	
                        <td ><?php echo $post09;?></td>
                        <td ><?php
						//echo "<br/>strTotPersenBaik= ".$strTotPersenBaik;
						$queView = mysqli_query($dbconn,$strTotPersenBaik);
						while($rowView=mysqli_fetch_object($queView)){
						$persenBaik4=$rowView->persenBaik; 
						if ($persenBaik4==0 or $persenBaik4=='' or $persenBaik4==null) {
							$persenBaik4=0;
							echo $persenBaik4;							
						}else {
							echo $persenBaik4;							
						}
						}
												
						/* if ($post02==0){
							echo '';
						}else {
							echo number_format($post09/$post02*100,2);
						} */
						?></td>		
                        
                        					
						</tr>
                    
<?php 
}elseif ($p->level=='3'){ 
//echo "<br/>strViewRow99= ".$strViewRow;  
$queView = mysqli_query($dbconn,$strViewRow);
                        $i=1;
											
						while($rowView=mysqli_fetch_object($queView)){ 							
						    
                            ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rowView->area;?></td>
                            <td><?php echo $rowView->sediamikroskop;	 ?></td>
							<td><?php //echo "strViewRow= ".$strViewRow;
							echo $rowView->kirimsediaan; ?></td>
                          <!-- <td><?php echo $rowView->komptmikros2;  ?></td> -->
                          <td><?php echo $rowView->sensi2; ?></td>
                          <td><?php echo $rowView->spesi2; ?></td>
                          <td><?php echo $rowView->akurasi2; ?></td>						  
                          <td><?php echo $rowView->hasil2; ?></td>                                                    
                        </tr>
						<?php $i++;						
                        } 
} 
 ?>
					
                    </tbody>
                    
                </table>
                
                
            </th>
        </tr>
    </thead>
</table>
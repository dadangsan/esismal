<head>
<style type="text/css">
.auto-style2 {
	text-align: right;
}
</style>
</head>

<?php
//dadang,laporan data harian
session_start();
require_once("../config/database-connect.php");
$p = (object)$_POST;
$s = (object)$_SESSION;

$prop = "SELECT * FROM mst_propinsi where id_propinsi='$p->propinsi'";
$pray = mysqli_query($dbconn,$prop);
$row = mysqli_fetch_object($pray);

  $kabs = "SELECT * FROM mst_kabupaten where id_kabupaten='$p->kabupaten'";
  $krow = mysqli_query($dbconn,$kabs);
$kray = mysqli_fetch_object($krow);
/*if($p->kabupaten!=''){
    $namakabs = $kray->nama_kabupaten;
}else{
    $namakabs = "Semua Kab/Kota";
}
*/

if($p->level=='1'){ //radiobutton nasional
     
    $namaPropinsi = "Semua Propinsi";
    $namaKab = "Semua Kabupaten";
    $namaFaskes = "Semua Faskes";
    $select = "a.*,b.nama_propinsi as area";
     $right = "right join mst_propinsi b on(a.idpropinsi=b.id_propinsi && a.tahun='$p->tahun') ";
    if($p->periode=='2'){
      $dataview = "view_lap_bulanan_nasional a";
      $totalSQL = "SELECT * FROM  view_lap_bulanan_nasional_total where tahun='$p->tahun'  && bulan='$p->bulan1'";
      }else{
    $dataview = "view_lap_tahunan_nasional a";
    $totalSQL = "SELECT * FROM  view_lap_tahunan_nasional_total where tahun='$p->tahun'";
    }
}elseif($p->level=='2'){
      
    $select = "a.*,b.nama_kabupaten as area";
     $right = "right join mst_kabupaten b on(a.idkabupaten=b.id_kabupaten && a.tahun='$p->tahun' && b.id_propinsi='$p->propinsi') ";
     
     $where = " WHERE b.id_propinsi='$p->propinsi'";
    $namaPropinsi = $row->nama_propinsi;
    $namaKab = "Semua Kabupaten";
    $namaFaskes = "Semua Faskes";
   
    if($p->periode=='2'){
      $dataview = "view_lap_bulanan_perprop a";
       $totalSQL = "SELECT * FROM  view_lap_bulanan_perprop_total where idpropinsi='$p->propinsi' && tahun='$p->tahun'  && bulan='$p->bulan1'";
      }else{
    $dataview = "view_lap_tahunan_perprop a";
     $totalSQL = "SELECT * FROM  view_lap_tahunan_perprop_total where idpropinsi='$p->propinsi' && tahun='$p->tahun'";
      }
}elseif($p->level=='3'){ //radiobutton kabupaten
   
   $select = "a.*,b.nama_fasyankes as area";
     $right = "right join mst_fasyankes b on(a.idfaskes=b.kd_fasyankes && a.tahun_lap='$p->tahun') ";
     $where = " WHERE b.kabupaten='$p->kabupaten'";
   
    $namaPropinsi = $row->nama_propinsi;
    $namaKab = $kray->nama_kabupaten;
    $namaFaskes = "Semua Faskes";
    if($p->periode=='2'){
       //$dataview = "view_lap_bulanan_perkab a";
	   $dataview = "dd_dataharian a";
       //$totalSQL = "SELECT * FROM  view_lap_bulanan_perkab_total where idkabupaten='$p->kabupaten' && tahun='$p->tahun' && bulan='$p->bulan1'";	   
	   $totalSQL = "SELECT * FROM  dd_dataharian where idkabupaten='$p->kabupaten' && tahun_lap='$p->tahun' && bulan_lap='$p->bulan1'";	   
      }else{
    $dataview = "view_lap_tahunan_perkab a";
     $totalSQL = "SELECT * FROM  view_lap_tahunan_perkab_total where idkabupaten='$p->kabupaten' && tahun='$p->tahun'";
      }    
      
}elseif($p->level=='4'){
$fks = implode("','",$p->faskes);
//dadang 
$mrQ3=  "SELECT a.juml_pos_mal, a.juml_pe FROM dd_dataharian a  
WHERE a.`tahun_lap` = '$p->tahun' AND a.`bulan_lap` = '$p->bulan1' AND a.idfaskes IN ('".$fks."')";

$select = "*";
 //$where = " WHERE tahun='$p->tahun' && kdfaskes IN ('".$fks."')";
 $where = " WHERE tahun_lap='$p->tahun' && idfaskes IN ('".$fks."')";
$strPKM = "SELECT GROUP_CONCAT('<i class=\"fa fa-plus-square fa-fw\"></i>',upper(nama_fasyankes) SEPARATOR '<br>') AS nama  FROM mst_fasyankes where kd_fasyankes in ('".$fks."')";
    //echo $strPKM;
    if($p->periode=='2'){
      //$dataview = "view_lap_bulanan_perfaskes";
	  $dataview = "dd_dataharian";
      }else{
    $dataview = "view_lap_tahunan_perfaskes"; 
      }
    $qPKM = mysqli_query($dbconn,$strPKM);
   $rPKM=mysqli_fetch_object($qPKM);
   $namaPropinsi = $row->nama_propinsi;
    $namaKab = $kray->nama_kabupaten;
    $namaFaskes = $rPKM->nama;  
}

echo $strView;



?>

<table class="tblInput" style="width: 100%">
    <thead>
        <tr>
            <th><h3>DATA HARIAN MALARIA (PER TANGGAL)</h3></th>
        </tr>
        <tr>
            <th>
                
                
                <table width="500" class="tblInput" style="width: 400px">
                    <thead>
                        <tr>
                            <th width="131" class="auto-style2">Tahun :</th>
                            <th style="width: 300px">&nbsp;<?php echo $p->tahun;
							if($p->periode=='2'){
							?>| Bulan:<?php echo $p->bulan1;?></th> 
                            <?php } ?>
                            
                            
                        </tr>
                        <tr>
                            <th class="auto-style2" style="height: 23px">Propinsi 
							:</th>
                            <th style="height: 23px; width: 300px;">&nbsp;<?php echo $namaPropinsi;?></th>
                        </tr>
                        
                        <tr>
                            <th class="auto-style2">Kabupaten/Kota :</th>
                            <th style="width: 300px">&nbsp;<?php echo $namaKab;?></th>
                        </tr>
                        
                        <tr>
                            <th style="vertical-align: top" class="auto-style2">Fasyankes 
							:</th>
                            <th style="width: 300px">&nbsp;<?php echo $namaFaskes;?></th>
                        </tr>
                    </thead>
                </table>
                
                
                
                
            </th>
            
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>
               
                
            </th>
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>
                
                <table class="tblListData" style="width: 3500px">
                    <thead>
                        <tr>
                            <th rowspan="3" style="width: 20px">No</th>
                            <th rowspan="3"  style="width: 150px">Wilayah</th>

                            <th colspan="2"  style="width: 10px">1</th>
                            <th colspan="2"  style="width: 10px">2</th>
                            <th colspan="2"  style="width: 10px">3</th>
                            <th colspan="2"  style="width: 10px">4</th>
                            <th colspan="2"  style="width: 10px">5</th>
                            <th colspan="2"  style="width: 10px">6</th>
                            <th colspan="2"  style="width: 10px">7</th>
                            <th colspan="2"  style="width: 10px">8</th>
                            <th colspan="2"  style="width: 10px">9</th>
                            <th colspan="2"  style="width: 10px">10</th>
                            <th colspan="2"  style="width: 10px">11</th>
                            <th colspan="2"  style="width: 10px">12</th>
                            <th colspan="2"  style="width: 10px">13</th>
                            <th colspan="2"  style="width: 10px">14</th>
                            <th colspan="2"  style="width: 10px">15</th>
                            <th colspan="2"  style="width: 10px">16</th>
                            <th colspan="2"  style="width: 10px">17</th>
                            <th colspan="2"  style="width: 10px">18</th>
                            <th colspan="2"  style="width: 10px">19</th>
                            <th colspan="2"  style="width: 10px">20</th>
                            <th colspan="2"  style="width: 10px">21</th>
                            <th colspan="2"  style="width: 10px">22</th>
                            <th colspan="2"  style="width: 10px">23</th>
                            <th colspan="2"  style="width: 10px">24</th>
                            <th colspan="2"  style="width: 10px">25</th>
                            <th colspan="2"  style="width: 10px">26</th>
                            <th colspan="2"  style="width: 10px">27</th>
                            <th colspan="2"  style="width: 10px">28</th>
                            <th colspan="2"  style="width: 10px">29</th>
                            <th colspan="2"  style="width: 10px">30</th>
                            <th colspan="2"  style="width: 10px">31</th>
                            
                        </tr>
                        <tr>
                            
                                <th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
								<th rowspan="2"  style="width: 5px">Positif</th>
                                <th rowspan="2"  style="width: 5px">PE</th>
                        </tr>
                        
                        <tr>
                            
                                  
                        </tr>
                       
                    </thead>
                    <tbody>
                             <?
                             





//dari HP
//$strView = "SELECT ".$select." FROM ".$dataview." ".$right." ".$where;
//rev.dadang
$strView2 = "SELECT ".$select." FROM ".$dataview." ".$right;
if($p->periode=='2' ){
            //$strView.=" && bulan='$p->bulan1'";
			//}
			//rev.dadang
				if ($p->level=='4') {
					$strViewLkp =$strView2;			
					$strView = $strViewLkp.$where." && bulan_lap='$p->bulan1'";		
					} else {
		 	$strViewLkp =$strView2." && bulan_lap='$p->bulan1'";			
			$strView = $strViewLkp.$where; 
					}
			} else {
				$strView = $strView2.$where;
			} 
if($p->level=='2'){
      //$strView.="WHERE a.idpropinsi='$p->propinsi'";
     
}elseif($p->level=='3'){
     // $strView.=" && idkabupaten='$p->kabupaten'";
}elseif($p->level=='4'){
    //  $strView .=" && kdfaskes in('".$fks."')";
}
                    if($p->level!=4){
                        $total = mysqli_query($dbconn,$totalSQL);
                       $agregatView = mysqli_fetch_object($total);
                       
                        ?>
                        
                         <tr style="background: #009900">
                            <td></td>
                            <td><?php // echo strtoupper($agregatView->area);?> TOTAL</td>

                            <td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>
							<td><?php echo $agregatView->juml_pos_mal;?></td>
                            <td><?php echo $agregatView->juml_pe;?></td>

                        </tr>
                        
                        <?php
                        
                     }
echo "</br>strView999= ".$strView;   
 
//code_HP                 
//$queView = mysqli_query($dbconn,$strView);
//rev dadang, nama faskes
$mrQ2=  "SELECT a.kd_fasyankes, a.nama_fasyankes, b.nama_propinsi, c.nama_kabupaten FROM  mst_fasyankes a inner join mst_propinsi b ON a.propinsi=b.id_propinsi inner join mst_kabupaten c on a.kabupaten=c.id_kabupaten where a.kd_fasyankes in ('".$fks."')"; 
?>
<!-- dadang, bagian atas tuk TOTAL -->
<tr>
              <td></td>
                            <td> TOTAL</td>

                            <td><?php 
							//$pos = " AND RIGHT(a.`tglupload`,2)='01' and a.idfaskes='".$kodefaskes."'";
							$pos = " AND RIGHT(`tglupload`,2)='01'";
  //$sqlpos1=$mrQ3.$pos;	
  $sqlpos1=$strView.$pos;	
  
  echo "</br>sqlpos1DD3: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos1 = $njk2->juml_pos_mal;	
  echo $pos1; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
							<td>&nbsp;</td>
                            <td>&nbsp;</td>
                      </tr>
<?php                        
//code_HP
//while($rowView=mysqli_fetch_object($queView)){ //kebaris 469
//rev Dadang
echo "</br>mrQ2D9= ".$mrQ2;
$mrQ = mysqli_query($dbconn,$mrQ2);
$i=0;
//while($rowView = mysqli_fetch_object($mrQ)) {
while($rowView=mysqli_fetch_object($queView)){
	$kodefaskes = $rowView->kd_fasyankes;
                            
                            //$i=$i+1;
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                            <td><?php //echo $rowView->area;
							echo $rowView->nama_fasyankes."(".$kodefaskes.")";
							?></td>
                            
                            <td><?php 
							//$pos = " AND RIGHT(a.`tglupload`,2)='01' and a.idfaskes='".$kodefaskes."'";
							$pos = " AND RIGHT(a.`tglupload`,2)='01'";
  //$sqlpos1=$mrQ3.$pos;	
  $sqlpos1=$strView2.$pos;	
  
  echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos1 = $njk2->juml_pos_mal;	
  echo $pos1; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='01' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe11 = $njk2->juml_pe;	
  echo $pe11; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='02' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos2 = $njk2->juml_pos_mal;	
  echo $pos2; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='02' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe12 = $njk2->juml_pe;	
  echo $pe12; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='03' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos3 = $njk2->juml_pos_mal;	
  echo $pos3; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='03' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe13 = $njk2->juml_pe;	
  echo $pe13; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='04' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos4 = $njk2->juml_pos_mal;	
  echo $pos4; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='04' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe14 = $njk2->juml_pe;	
  echo $pe14; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='05' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos5 = $njk2->juml_pos_mal;	
  echo $pos5; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='05' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe15 = $njk2->juml_pe;	
  echo $pe15; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='06' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos6 = $njk2->juml_pos_mal;	
  echo $pos6; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='06' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe16 = $njk2->juml_pe;	
  echo $pe16; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='07' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos7 = $njk2->juml_pos_mal;	
  echo $pos7; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='07' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe17 = $njk2->juml_pe;	
  echo $pe17; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='08' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos8 = $njk2->juml_pos_mal;	
  echo $pos8; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='08' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe18 = $njk2->juml_pe;	
  echo $pe18; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='09' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos9 = $njk2->juml_pos_mal;	
  echo $pos9; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='09' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe19 = $njk2->juml_pe;	
  echo $pe19; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='10' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos10 = $njk2->juml_pos_mal;	
  echo $pos10; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='10' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe110 = $njk2->juml_pe;	
  echo $pe110; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='11' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos11 = $njk2->juml_pos_mal;	
  echo $pos11; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='11' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe111 = $njk2->juml_pe;	
  echo $pe111; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='12' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos12 = $njk2->juml_pos_mal;	
  echo $pos12; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='12' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe112 = $njk2->juml_pe;	
  echo $pe112; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='13' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos13 = $njk2->juml_pos_mal;	
  echo $pos13; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='13' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe113 = $njk2->juml_pe;	
  echo $pe113; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='14' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos14 = $njk2->juml_pos_mal;	
  echo $pos14; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='14' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe114 = $njk2->juml_pe;	
  echo $pe114; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='15' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos15 = $njk2->juml_pos_mal;	
  echo $pos15; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='15' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe115 = $njk2->juml_pe;	
  echo $pe115; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='16' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos16 = $njk2->juml_pos_mal;	
  echo $pos16; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='16' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe116 = $njk2->juml_pe;	
  echo $pe116; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='17' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos17 = $njk2->juml_pos_mal;	
  echo $pos17; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='17' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe117 = $njk2->juml_pe;	
  echo $pe117; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='18' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos18 = $njk2->juml_pos_mal;	
  echo $pos18; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='18' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe118 = $njk2->juml_pe;	
  echo $pe118; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='19' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos19 = $njk2->juml_pos_mal;	
  echo $pos19; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='19' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe119 = $njk2->juml_pe;	
  echo $pe119; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='20' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos20 = $njk2->juml_pos_mal;	
  echo $pos20; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='20' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe120 = $njk2->juml_pe;	
  echo $pe120; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='21' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos21 = $njk2->juml_pos_mal;	
  echo $pos21; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='21' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe121 = $njk2->juml_pe;	
  echo $pe121; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='22' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos22 = $njk2->juml_pos_mal;	
  echo $pos22; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='22' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe122 = $njk2->juml_pe;	
  echo $pe122; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='23' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos23 = $njk2->juml_pos_mal;	
  echo $pos23; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='23' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe123 = $njk2->juml_pe;	
  echo $pe123; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='24' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos24 = $njk2->juml_pos_mal;	
  echo $pos24; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='24' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe124 = $njk2->juml_pe;	
  echo $pe124; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='25' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos25 = $njk2->juml_pos_mal;	
  echo $pos25; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='25' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe125 = $njk2->juml_pe;	
  echo $pe125; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='26' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos26 = $njk2->juml_pos_mal;	
  echo $pos26; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='26' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe126 = $njk2->juml_pe;	
  echo $pe126; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='27' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos27 = $njk2->juml_pos_mal;	
  echo $pos27; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='27' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe127 = $njk2->juml_pe;	
  echo $pe127; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='28' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos28 = $njk2->juml_pos_mal;	
  echo $pos28; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='28' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe128 = $njk2->juml_pe;	
  echo $pe128; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='29' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos29 = $njk2->juml_pos_mal;	
  echo $pos29; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='29' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe129 = $njk2->juml_pe;	
  echo $pe129; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='30' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos30 = $njk2->juml_pos_mal;	
  echo $pos30; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='30' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe130 = $njk2->juml_pe;	
  echo $pe130; }
							//echo $rowView->juml_pos_mal;?></td>
							<td><?php 
							$pos = " AND RIGHT(a.`tglupload`,2)='31' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pos;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pos31 = $njk2->juml_pos_mal;	
  echo $pos31; }
							//echo $rowView->juml_pos_mal;?></td>
                            <td><?php 
							$pex = " AND RIGHT(a.`tglupload`,2)='31' and a.idfaskes='".$kodefaskes."'";
  $sqlpos1=$mrQ3.$pex;	
  //echo "</br>sqlpos1DD: ".$sqlpos1;
  $sqlpos2=mysqli_query($dbconn,$sqlpos1);	
  while($njk2 = mysqli_fetch_object($sqlpos2)){ 
	$pe131 = $njk2->juml_pe;	
  echo $pe131; }
							//echo $rowView->juml_pos_mal;?></td>
							
                        </tr>
                        <?php $i++;
                        } 
						?>
                     
                    
                     
                            
                    </tbody>
                    
                </table>
                
                
            </th>
        </tr>
    </thead>
</table>
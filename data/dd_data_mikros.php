<head>
<style type="text/css">
.auto-style2 {
	text-align: right;
}
</style>
</head>

<script>
/* 
function tampilGambar(){
      document.getElementById('myLoadingGif').style.display = "block";
} */
</script>
<?php
//dadang,laporan dataHarian
session_start();
require_once("../config/database-connect.php");
$p = (object)$_POST;
$s = (object)$_SESSION;

//echo '<div id="container" ><img src="/esismalv2/loader/ajax-loader.gif" id="myLoadingGif" style= "display: none;"></div>';
$Qselect=" a.kirimsediaan, IF(a.ksediaan_mikros='1','Tersedia',
IF(a.ksediaan_mikros='2','Tidak Tersedia','')) AS sediamikroskop, 
IF(a.kompt_mikros='1','Level1', IF(a.kompt_mikros='2','Level2', IF(a.kompt_mikros='3','Level3', IF(a.kompt_mikros='4','Level4', 
IF(a.kompt_mikros='5','Belum Diketahui', ''))))) AS komptmikros2, IF(a.sensitivitas1='0','<70',IF(a.sensitivitas1='1','>=70','')) AS sensi2, 
IF(a.spesivitas1='0','<70',IF(a.spesivitas1='1','>=70','')) AS spesi2, IF(a.akurasi_spes='0','<70', IF(a.akurasi_spes='1','>=70','')) AS akurasi2, 
a.hasil_uji_sil AS hasil2, ";

$prop = "SELECT * FROM mst_propinsi where id_propinsi='$p->propinsi'";
$pray = mysqli_query($dbconn,$prop);
$row = mysqli_fetch_object($pray);

  $kabs = "SELECT * FROM mst_kabupaten where id_kabupaten='$p->kabupaten'";
  $krow = mysqli_query($dbconn,$kabs);
$kray = mysqli_fetch_object($krow);

if($p->level=='1'){ //nasional    
    $namaPropinsi = "Semua Propinsi";
    //$namaKab = "Semua Kabupaten";
    //$namaFaskes = "Semua Faskes";
    
	$select = $Qselect." b.id_propinsi, b.nama_propinsi as area";
     $right = "right join mst_propinsi b on(a.kd_prop=b.id_propinsi && a.tahun='$p->tahun' && a.triwulan='$p->bulan1' ) ";
    if($p->periode=='2'){
      //$dataview = "ddview_dataharian a";
	  $dataview = "ent_mikroskops a";
      $totalSQL = "SELECT * FROM  view_lap_bulanan_nasional_total where tahun='$p->tahun'  && bulan='$p->bulan1'";
      }else{
      $dataview = "ent_mikroskops a";
    $totalSQL = "SELECT * FROM  view_lap_tahunan_nasional_total where tahun='$p->tahun'";
    }
}elseif($p->level=='2'){ //radiobuttn propinsi      
	$select = $Qselect." b.id_kabupaten, b.nama_kabupaten as area ";
	$right = "right join mst_kabupaten b on(a.kd_kab=b.id_kabupaten && a.tahun='$p->tahun' && a.triwulan='$p->bulan1' && b.id_propinsi='$p->propinsi') ";     
     $where = " WHERE b.id_propinsi='$p->propinsi'";
    $namaPropinsi = $row->nama_propinsi;
    $namaKab = "Semua Kabupaten";
    //$namaFaskes = "Semua Faskes";
   
    if($p->periode=='2'){
      $dataview = "ent_mikroskops a";
       $totalSQL = "SELECT * FROM  view_lap_bulanan_perprop_total where kd_prop='$p->propinsi' && tahun='$p->tahun'  && bulan='$p->bulan1'";
      }else{
    $dataview = "ent_mikroskops a";
	$totalSQL = "SELECT * FROM  view_lap_tahunan_perprop_total where kd_prop='$p->propinsi' && tahun='$p->tahun'";
      }
}elseif($p->level=='3'){   
   //$select = " a.tglupload, a.`tahun`, a.`triwulan`, a.`kd_faskes`, a.`kd_prop`, a.`kd_kab`,a.`juml_pos_mal`, a.`juml_pe`, SUM(a.`juml_pos_mal`) AS TotPosMal, b.kd_fasyankes, b.nama_fasyankes as area";
   $select = $Qselect." b.kd_fasyankes, b.nama_fasyankes as area";
     $right = "right join mst_fasyankes b On(a.kd_faskes=b.kd_fasyankes && tahun='$p->tahun')"; 
     $where = " WHERE b.kabupaten='$p->kabupaten'";
   
    $namaPropinsi = $row->nama_propinsi;
    $namaKab = $kray->nama_kabupaten;
    $namaFaskes = "Semua Faskes";
    if($p->periode=='2'){
		$dataview = "ent_mikroskops a";
       $totalSQL = "SELECT * FROM  view_lap_bulanan_perkab_total where kd_kab='$p->kabupaten' && tahun='$p->tahun' && bulan='$p->bulan1'";
      }else{
    $dataview = "ent_mikroskops a";
	$totalSQL = "SELECT * FROM  view_lap_tahunan_perkab_total where kd_kab='$p->kabupaten' && tahun='$p->tahun'";
      }
	  
}elseif($p->level=='4'){
$fks = implode("','",$p->faskes);
//$select = " a.tglupload, a.`tahun`, a.`triwulan`, a.`kd_faskes`, a.`kd_prop`, a.`kd_kab`,a.`juml_pos_mal`, a.`juml_pe`, SUM(a.`juml_pos_mal`) AS TotPosMal, b.kd_fasyankes, b.nama_fasyankes as area";
$select = $Qselect." b.kd_fasyankes, b.nama_fasyankes as area";
$right = "right join mst_fasyankes b ON a.kd_faskes=b.kd_fasyankes"; // && a.tahun='$p->tahun') ";
 $where = " WHERE tahun='$p->tahun' && kd_faskes IN ('".$fks."')";
 $strPKM = "SELECT GROUP_CONCAT('<i class=\"fa fa-plus-square fa-fw\"></i>',upper(nama_fasyankes) SEPARATOR '<br>') AS nama  FROM mst_fasyankes where kd_fasyankes in ('".$fks."')";
    //echo $strPKM;
    if($p->periode=='2'){
		$dataview = "ent_mikroskops a";
      }else{
    $dataview = "ent_mikroskops a";
      }
    $qPKM = mysqli_query($dbconn,$strPKM);
   $rPKM=mysqli_fetch_object($qPKM);
   $namaPropinsi = $row->nama_propinsi;
    $namaKab = $kray->nama_kabupaten;
    $namaFaskes = $rPKM->nama;  	
}

echo $strView;
echo $strViewRow;
?>

<table class="tblInput" style="width: 100%">
    <thead>
        <tr>
            <th><h3>Laporan Data Mikroskopis </h3></th>
        </tr>
        <tr>
            <th>
                
                
                <table width="500" class="tblInput" style="width: 400px">
                    <thead>
                        <tr>
                            <th width="131" class="auto-style2">Tahun :</th>
                            <th style="width: 300px">&nbsp;<?php echo $p->tahun;
							if($p->periode=='2'){
							?>| Triwulan:<?php echo $p->bulan1;?></th> 
                            <?php } ?>
                            
                            
                        </tr>
                        <tr>
                            <th class="auto-style2" style="height: 23px">Propinsi 
							:</th>
                            <th style="height: 23px; width: 300px;">&nbsp;<?php echo $namaPropinsi;?></th>
                        </tr>
                        
                        <tr>
                            <th class="auto-style2">Kabupaten/Kota :</th>
                            <th style="width: 300px">&nbsp;<?php echo $namaKab;?></th>
                        </tr> 
                        
                        <tr>
                            <th style="vertical-align: top" class="auto-style2">Fasyankes 
							:</th>
                            <th style="width: 300px">&nbsp;<?php echo $namaFaskes;?></th>
                        </tr>
                    </thead>
                </table>
            </th>
            
        </tr>
        <!-- <tr>
            <th>&nbsp;</th>
        </tr> -->
        <!-- <tr>
            <th>
               
                
            </th>
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr> -->
        <tr>
            <th>
                
                <table class="tblListData" style="width: 100%">
                    <thead>
                        <tr>
                            <!-- <th width="132" rowspan="1" style="width: 20px">No</th> -->
                            <th rowspan="1" style="width: 3%">No</th> 
                            <th rowspan="1"  style="width: 25%">Wilayah</th>                            
                            <th colspan="1"  style="width: 3%">Mikroskopis</th>							
							<th colspan="1"  style="width: 3%">Mengirim Sediaan</th>  
							<!-- <th colspan="1"  style="width: 5%"><span style="width: 5px">Komp. Mikroskopis</span></th> -->
                            
                            <th colspan="1"  style="width: 3%">Sensitivitas</th>
                            <th colspan="1"  style="width: 3%">Spesivitas</th>
                            <th colspan="1"  style="width: 3%">Akurasi Spesiemen</th>
                            <th colspan="1"  style="width: 8%">HASIL</th>
                            
                            
                            
                        </tr>
                        
						<!-- <tr>
                        </tr>
                        
                        <tr>                            
                        </tr> -->
                       
                    </thead>
                    <tbody>
                             <?
                             

//dari HP
//$strView = "SELECT ".$select." FROM ".$dataview." ".$right." ".$where;
//rev.dadang
$strView2 = "SELECT ".$select." FROM ".$dataview." ".$right;

if($p->periode=='2' ){
            //$strView.=" && bulan='$p->bulan1'";
			//}
			//rev.dadang
				if ($p->level=='4') {
					$strViewLkp =$strView2;			
					$strView = $strViewLkp.$where." && triwulan='$p->bulan1' Group By kd_fasyankes ";	
					$strViewRow = $strViewLkp.$where." && triwulan='$p->bulan1' ";			
					}
					elseif ($p->level=='3') {
						$strViewLkp =$strView2." && triwulan='$p->bulan1'";			
						$strView = $strViewLkp.$where." Group By kd_fasyankes ";								
						$strViewRow = $strViewLkp.$where;			
					}					
					elseif ($p->level=='2') { //propinsi
						$strViewLkp ="SELECT ".$select." FROM ".$dataview." ".$right;
						//$strViewLkp =$strView2." && triwulan='$p->bulan1'";										
						$strView = $strView2.$where." Group By id_kabupaten ";	
						$strViewRow = $strViewLkp.$where;			
					}
					else { //Nasional
						$strViewLkp ="SELECT ".$select." FROM ".$dataview." ".$right;
						//$strViewLkp =$strView2." && triwulan='$p->bulan1'";										
						$strView = $strView2.$where." Group By id_propinsi ";	
						$strViewRow = $strViewLkp.$where;			
					}
			} else {
				$strView = $strView2.$where." Group By kd_fasyankes ";
				$strViewRow = $strView2.$where;
			} 

                        ?>                       
<!-- <tr style="background: #009900"> </tr> -->
                        <?php                        
//echo "</br>strViewRow999= ".$strViewRow;   
                  
$queView = mysqli_query($dbconn,$strViewRow);
                        $i=1;
						//dadang, tambahkan loader
						//echo '<div id="container" ><img src="/esismalv2/loader/ajax-loader.gif" id="myLoadingGif" style= "display: none;"></div>';
						
						// dadang, khusus radiobutton kab dan faskes.						
						while($rowView=mysqli_fetch_object($queView)){ 							
							if ($p->level=='4' || $p->level=='3') {
							$kodefaskes2=$rowView->kd_fasyankes;
							$kodefaskes="'".$kodefaskes2."'";
							$sqland = " AND kd_fasyankes=$kodefaskes " ;
							}elseif ($p->level=='2') {
							$kodefaskes2=$rowView->id_kabupaten;							
							$kodefaskes="'".$kodefaskes2."'";
							$sqland = " AND id_kabupaten=$kodefaskes  ";
							}else {
							$kodefaskes2=$rowView->id_propinsi;							
							$kodefaskes="'".$kodefaskes2."'";
							$sqland = " AND id_propinsi=$kodefaskes  ";
							}
                        // dadang, akhir khusus radiobutton kab dan faskes.						
                            
                            ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rowView->area;?></td>
                             <td><?php 		
	echo $rowView->sediamikroskop;							 
   
						  ?></td>
                         <td><?php 							 
	echo $rowView->kirimsediaan;    
						  ?></td> 
                         <!--  <td><?php 
   echo $rowView->komptmikros2;
						  ?></td> -->
                          <td><?php 	
	 echo $rowView->sensi2;
						  ?></td>
                          <td><?php 
  echo $rowView->spesi2;
						  ?></td>
                          <td><?php 	
	echo $rowView->akurasi2; 
						  ?></td>						  
                          <td><?php 	
	echo $rowView->hasil2; 
						  ?></td>
                          
                          
                        </tr>
						<?php $i++;
						
                        } ?>
						
                            
                    </tbody>
                    
                </table>
                
                
            </th>
        </tr>
    </thead>
</table>
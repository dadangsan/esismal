<?php
session_start();
require_once("../config/database-connect.php");
$p = (object)$_POST;
$s = (object)$_SESSION;

$prop = "SELECT * FROM mst_propinsi where id_propinsi='$p->propinsi'";
$pray = mysqli_query($dbconn,$prop);
$row = mysqli_fetch_object($pray);

  $kabs = "SELECT * FROM mst_kabupaten where id_kabupaten='$p->kabupaten'";
  $krow = mysqli_query($dbconn,$kabs);
$kray = mysqli_fetch_object($krow);
if($p->kabupaten!=''){
    $namakabs = $kray->nama_kabupaten;
}else{
    $namakabs = "Semua Kab/Kota";
}



$sqlSTR = "SELECT kdfaskes,idpropinsi,idkabupaten,tahun,SUM(suspek) AS suspek,SUM(darpositif) AS darpos,SUM(darnegatif) AS darneg, SUM(darah) AS darah,SUM(pos_pcd) AS pcdpos,SUM(neg_pcd) AS pcdneg,SUM(tot_pcd) AS pcdtotal FROM view_rekap_header_bulanan WHERE tahun='$p->tahun' && bulan between '$p->bulan1' and '$p->bulan2'";
if($p->kabupaten!=''){
    if(isset($p->faskes)){
        $fks = implode("','",$p->faskes);
        $sqlSTR .=" && kdfaskes in('".$fks."')";
        $sqlSTR .=" GROUP by tahun,idpropinsi,idkabupaten";
    $strPKM = "SELECT GROUP_CONCAT('<i class=\"fa fa-plus-square fa-fw\"></i>',nama_fasyankes SEPARATOR '<br>') AS nama  FROM mst_fasyankes where kd_fasyankes in ('".$fks."')";
    //echo $strPKM;
    $dataview = "view_lap_tahunan_perfaskes"; 
    
    $qPKM = mysqli_query($dbconn,$strPKM);
   $rPKM=mysqli_fetch_object($qPKM);
    $ListFasyankes = $rPKM->nama;
    }else{
        $sqlSTR .= " && idkabupaten='$p->kabupaten'";
        $sqlSTR .=" GROUP by tahun,idpropinsi,idkabupaten";
    $ListFasyankes = "Semua Faskes";
     $dataview = "view_lap_tahunan_perkab"; 
    
    }
}else{
    $sqlSTR .= " && idpropinsi='$p->propinsi'";
    $sqlSTR .=" GROUP by tahun,idpropinsi";
    $ListFasyankes = "Semua Faskes";
     $dataview = "view_lap_tahunan_perprop"; 
    
}


$sQuery = mysqli_query($dbconn,$sqlSTR);
$resultHeader = mysqli_fetch_object($sQuery);

//echo $strView;
?>
<table class="tblInput" style="width: 100%">
    <thead>
        <tr>
            <th><h3>LAPORAN TAHUNAN PROGRAM MALARIA</h3></th>
        </tr>
        <tr>
            <th>
                
                
                <table class="tblInput" style="width: 300px">
                    <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>: <?php echo $p->tahun;?></th>
                        </tr>
                        <tr>
                            <th>Propinsi</th>
                            <th>: <?php echo $row->nama_propinsi;?></th>
                        </tr>
                        
                        <tr>
                            <th>Kabupaten/Kota</th>
                            <th>: <?php echo $namakabs;?></th>
                        </tr>
                        
                        <tr>
                            <th style="vertical-align: top">Fasyankes</th>
                            <th><?php echo $ListFasyankes;?></th>
                        </tr>
                    </thead>
                </table>
                
                
                
                
            </th>
            
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>
                <table class="tblInput">
                    <thead>
                        <tr>
                            <th>Jumlah Suspek</th>
                            <th>: <?php echo $resultHeader->suspek;?></th>
                        </tr>
                        <tr>
                            <th>Total Sediaan Darah Positif</th>
                            <th>: <?php echo $resultHeader->darpos;?></th>
                        </tr>
                         <tr>
                            <th>Total Sediaan Darah Negatif</th>
                            <th>: <?php echo $resultHeader->darneg;?></th>
                        </tr>
                         
                          <tr>
                            <th>Total Sediaan Darah (Positif+Negatif)</th>
                            <th>: <?php echo $resultHeader->darah;?></th>
                        </tr>
                           <tr>
                            <th>Total Sediaan Darah Positif (PCD)</th>
                            <th>: <?php echo $resultHeader->pcdpos;?></th>
                        </tr>
                           
                           <tr>
                            <th>Total Sediaan Darah Negatif (PCD)</th>
                            <th>: <?php echo $resultHeader->pcdneg;?></th>
                        </tr>
                           <tr>
                            <th>Total Sediaan Darah Positif+Negatif (PCD)</th>
                            <th>: <?php echo $resultHeader->pcdtotal;?></th>
                        </tr>
                           
                    </thead>
                </table>
                
            </th>
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>
                
                <table class="tblListData" style="width: 3500px">
                    <thead>
                        <tr>
                            <th rowspan="3" style="width: 20px">No</th>
                            <th rowspan="3"  style="width: 150px">Wilayah</th>
                            <th rowspan="3"  style="width: 100px">Jumlah Penduduk</th>
                            <th colspan="4"  style="width: 200px">Konfirmasi Laboratorium</th>
                            <th colspan="15"  style="width: 750px">Positif Malaria</th>
                            <th rowspan="3"  style="width: 100px">Kematian Karena Malaria</th>
                            <th rowspan="3"  style="width: 100px">Ibu hami pos Malaria</th>
                            <th colspan="6"  style="width: 300px">Jenis Parasit</th>
                            <th colspan="3"  style="width: 150px">Pengobatan</th>
                            <th colspan="5"  style="width: 500px">Penyelidikan Epidemiologi</th>
                            <th colspan="3"  style="width: 100px">Indikator</th>
                            <th rowspan="3"  style="width: 50px">API</th>
                        </tr>
                        <tr>
                            <th rowspan="2"  style="width: 50px">Mikroskop</th>
                            <th rowspan="2"  style="width: 50px">RDT</th>
                            <th rowspan="2"  style="width: 50px">Lainnya</th>
                            <th rowspan="2"  style="width: 50px">Total</th>
                            
                            <th colspan="2"  style="width: 50px">0-11 bln</th>
                            <th colspan="2"  style="width: 50px">1-4 thn</th>
                            <th colspan="2"  style="width: 50px">5-9 thn</th>
                            <th colspan="2"  style="width: 50px">10-14 thn</th>
                            <th colspan="2"  style="width: 50px">15-64 thn</th>
                            <th colspan="2"  style="width: 50px">>64 thn</th>
                            <th colspan="3"  style="width: 50px">Total Positif</th>
                            
                            
                            <th rowspan="2"  style="width: 50px">Pf</th>
                            <th rowspan="2"  style="width: 50px">Pv</th>
                            <th rowspan="2"  style="width: 50px">Po</th>
                            <th rowspan="2"  style="width: 50px">Pm</th>
                            <th rowspan="2"  style="width: 50px">Pk</th>
                            <th rowspan="2"  style="width: 50px">Mix</th>
                            
                            <th rowspan="2"  style="width: 50px">ACT</th>
                              <th rowspan="2"  style="width: 50px">Non ACT</th>
                                <th rowspan="2"  style="width: 50px">Primaquin 14 hari</th>
                                
                                
                            <th rowspan="2"  style="width: 50px">Kasus di PE</th>
                             <th colspan="4"  style="width: 200px">Klasifikasi Asal Penularan</th>
                             
                              <th rowspan="2"  style="width: 50px">% ACT</th>
                               <th rowspan="2"  style="width: 50px">% Primaquin 14 hari</th>
                                <th rowspan="2"  style="width: 50px">% Kasus Di PE</th>
                        </tr>
                        
                        <tr>
                            <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            <th style="width: 25px">
                                Total
                            </th>
                            
                            <th style="width: 50px">
                                Indigenus
                            </th>
                              <th style="width: 50px">
                                impor
                            </th>
                                <th style="width: 50px">
                                relaps
                            </th>
                                  <th style="width: 50px">
                                Induced
                            </th>
                        </tr>
                       
                    </thead>
                    <tbody>
                             <?
                             
$strView = "SELECT * FROM ".$dataview." WHERE tahun='$p->tahun' && bulan between '$p->bulan1' and '$p->bulan2'";
$queView = mysqli_query($dbconn,$strView);
                        $i=1;
                        while($rowView=mysqli_fetch_object($queView)){?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rowView->kd_desa_kel;?></td>
                             <td><?php echo $rowView->jmlpddk;?></td>
                         <td><?php echo $rowView->mikroskopis;?></td>
                          <td><?php echo $rowView->rdt;?></td>
                           <td><?php echo $rowView->lainnya;?></td>
                            <td><?php echo $rowView->total;?></td>
                            
                            
                            <td><?php echo $rowView->l1;?></td>
                            <td><?php echo $rowView->p1;?></td>
                            
                            <td><?php echo $rowView->l2;?></td>
                            <td><?php echo $rowView->p2;?></td>
                            <td><?php echo $rowView->l3;?></td>
                            <td><?php echo $rowView->p3;?></td>
                            <td><?php echo $rowView->l4;?></td>
                            <td><?php echo $rowView->p4;?></td>
                            <td><?php echo $rowView->l5;?></td>
                            <td><?php echo $rowView->p5;?></td>
                            <td><?php echo $rowView->l6;?></td>
                            <td><?php echo $rowView->p6;?></td>
                            <td><?php echo $rowView->ltotal;?></td>
                            <td><?php echo $rowView->ptotal;?></td>
                            <td><?php echo $rowView->lptotal;?></td>
                            <td><?php echo $rowView->kematian;?></td>
                            <td><?php echo $rowView->bumil;?></td>
                            <td><?php echo $rowView->pf;?></td>
                            <td><?php echo $rowView->pv;?></td>
                            <td><?php echo $rowView->po;?></td>
                            <td><?php echo $rowView->pm;?></td>
                            <td><?php echo $rowView->pk;?></td>
                            <td><?php echo $rowView->mix;?></td>
                            <td><?php echo $rowView->peng_act;?></td>
                            <td><?php echo $rowView->peng_nonact;?></td>
                            <td><?php echo $rowView->peng_primaquin;?></td>
                            <td><?php echo $rowView->kasus_pe;?></td>
                            <td><?php echo $rowView->indigenus;?></td>
                            <td><?php echo $rowView->impor;?></td>
                            <td><?php echo $rowView->relap;?></td>
                            <td><?php echo $rowView->induced;?></td>
                            <td><?php echo $rowView->indik_act;?></td>
                            <td><?php echo $rowView->indik_primaq;?></td>
                            
                            <td><?php echo $rowView->indik_kasus_pe;?></td>
                            <td><?php echo $rowView->api;?></td>
                            
                        </tr>
                        <?php $i++;
                        } ?>
                            
                    </tbody>
                    
                </table>
                
                
            </th>
        </tr>
    </thead>
</table>
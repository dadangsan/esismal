<?php
session_start();
require_once("../config/database-connect.php");
$datetime = date("YmdHis");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("content-disposition: attachment;filename=Laporan_$datetime.xls");
header('Cache-Control: max-age=0'); 
$p = (object)$_GET;
$s = (object)$_SESSION;

$prop = "SELECT * FROM mst_propinsi where id_propinsi='$p->propinsi'";
$pray = mysqli_query($dbconn,$prop);
$row = mysqli_fetch_object($pray);

  $kabs = "SELECT * FROM mst_kabupaten where id_kabupaten='$p->kabupaten'";
  $krow = mysqli_query($dbconn,$kabs);
$kray = mysqli_fetch_object($krow);
/*if($p->kabupaten!=''){
    $namakabs = $kray->nama_kabupaten;
}else{
    $namakabs = "Semua Kab/Kota";
}
*/

if($p->level=='1'){
     
    $namaPropinsi = "Semua Propinsi";
    $namaKab = "Semua Kabupaten";
    $namaFaskes = "Semua Faskes";
    $select = "a.*,b.nama_propinsi as area";
     $right = "right join mst_propinsi b on(a.idpropinsi=b.id_propinsi && a.tahun='$p->tahun') ";
    if($p->periode=='2'){
      $dataview = "view_lap_bulanan_nasional a";
      $totalSQL = "SELECT * FROM  view_lap_bulanan_nasional_total where tahun='$p->tahun'  && bulan='$p->bulan1'";
      }else{
    $dataview = "view_lap_tahunan_nasional a";
    $totalSQL = "SELECT * FROM  view_lap_tahunan_nasional_total where tahun='$p->tahun'";
    }
}elseif($p->level=='2'){
      
    $select = "a.*,b.nama_kabupaten as area";
     $right = "right join mst_kabupaten b on(a.idkabupaten=b.id_kabupaten && a.tahun='$p->tahun' && b.id_propinsi='$p->propinsi') ";
     
     $where = " WHERE b.id_propinsi='$p->propinsi'";
    $namaPropinsi = $row->nama_propinsi;
    $namaKab = "Semua Kabupaten";
    $namaFaskes = "Semua Faskes";
   
    if($p->periode=='2'){
      $dataview = "view_lap_bulanan_perprop a";
       $totalSQL = "SELECT * FROM  view_lap_bulanan_perprop_total where idpropinsi='$p->propinsi' && tahun='$p->tahun'  && bulan='$p->bulan1'";
      }else{
    $dataview = "view_lap_tahunan_perprop a";
     $totalSQL = "SELECT * FROM  view_lap_tahunan_perprop_total where idpropinsi='$p->propinsi' && tahun='$p->tahun'";
      }
}elseif($p->level=='3'){
   
   $select = "a.*,b.nama_fasyankes as area";
     $right = "right join mst_fasyankes b on(a.kdfaskes=b.kd_fasyankes && a.tahun='$p->tahun') ";
     $where = " WHERE b.kabupaten='$p->kabupaten'";
   
    $namaPropinsi = $row->nama_propinsi;
    $namaKab = $kray->nama_kabupaten;
    $namaFaskes = "Semua Faskes";
    if($p->periode=='2'){
      $dataview = "view_lap_bulanan_perkab a";
       $totalSQL = "SELECT * FROM  view_lap_bulanan_perkab_total where idkabupaten='$p->kabupaten' && tahun='$p->tahun' && bulan='$p->bulan1'";
      }else{
    $dataview = "view_lap_tahunan_perkab a";
     $totalSQL = "SELECT * FROM  view_lap_tahunan_perkab_total where idkabupaten='$p->kabupaten' && tahun='$p->tahun'";
      }
      
     
      
}elseif($p->level=='4'){
$fks = implode("','",$p->faskes);
$select = "*";
 $where = " WHERE tahun='$p->tahun' && kdfaskes IN ('".$fks."')";
$strPKM = "SELECT GROUP_CONCAT('<i class=\"fa fa-plus-square fa-fw\"></i>',upper(nama_fasyankes) SEPARATOR '<br>') AS nama  FROM mst_fasyankes where kd_fasyankes in ('".$fks."')";
    //echo $strPKM;
    if($p->periode=='2'){
      $dataview = "view_lap_bulanan_perfaskes";
      }else{
    $dataview = "view_lap_tahunan_perfaskes"; 
      }
    $qPKM = mysqli_query($dbconn,$strPKM);
   $rPKM=mysqli_fetch_object($qPKM);
   $namaPropinsi = $row->nama_propinsi;
    $namaKab = $kray->nama_kabupaten;
    $namaFaskes = $rPKM->nama;  
}









echo $strView;
?>
<table class="tblInput" style="width: 100%">
    <thead>
        <tr>
            <th><h3>LAPORAN TAHUNAN PROGRAM MALARIA</h3></th>
        </tr>
        <tr>
            <th>
                
                
                <table class="tblInput" style="width: 300px">
                    <thead>
                        <tr>
                            <th>Tahun</th>
                            <th>: <?php echo $p->tahun;?></th>
                        </tr>
                        <tr>
                            <th>Propinsi</th>
                            <th>: <?php echo $namaPropinsi;?></th>
                        </tr>
                        
                        <tr>
                            <th>Kabupaten/Kota</th>
                            <th>: <?php echo $namaKab;?></th>
                        </tr>
                        
                        <tr>
                            <th style="vertical-align: top">Fasyankes</th>
                            <th><?php echo $namaFaskes;?></th>
                        </tr>
                    </thead>
                </table>
                
                
                
                
            </th>
            
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>
               
                
            </th>
        </tr>
        <tr>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <th>
                
                <table class="tblListData" style="width: 3500px">
                    <thead>
                        <tr>
                            <th rowspan="3" style="width: 20px">No</th>
                            <th rowspan="3"  style="width: 150px">Wilayah</th>
                            <th rowspan="3"  style="width: 100px">Jumlah Penduduk</th>
                            <th colspan="4"  style="width: 200px">Konfirmasi Laboratorium</th>
                            <th colspan="15"  style="width: 750px">Positif Malaria</th>
                            <th rowspan="3"  style="width: 100px">Kematian Karena Malaria</th>
                            <th rowspan="3"  style="width: 100px">Ibu hami pos Malaria</th>
                            <th colspan="6"  style="width: 300px">Jenis Parasit</th>
                            <th colspan="3"  style="width: 150px">Pengobatan</th>
                            <th colspan="5"  style="width: 500px">Penyelidikan Epidemiologi</th>
                            <th colspan="3"  style="width: 100px">Indikator</th>
                            <th rowspan="3"  style="width: 50px">API</th>
                        </tr>
                        <tr>
                            <th rowspan="2"  style="width: 50px">Mikroskop</th>
                            <th rowspan="2"  style="width: 50px">RDT</th>
                            <th rowspan="2"  style="width: 50px">Lainnya</th>
                            <th rowspan="2"  style="width: 50px">Total</th>
                            
                            <th colspan="2"  style="width: 50px">0-11 bln</th>
                            <th colspan="2"  style="width: 50px">1-4 thn</th>
                            <th colspan="2"  style="width: 50px">5-9 thn</th>
                            <th colspan="2"  style="width: 50px">10-14 thn</th>
                            <th colspan="2"  style="width: 50px">15-64 thn</th>
                            <th colspan="2"  style="width: 50px">>64 thn</th>
                            <th colspan="3"  style="width: 50px">Total Positif</th>
                            
                            
                            <th rowspan="2"  style="width: 50px">Pf</th>
                            <th rowspan="2"  style="width: 50px">Pv</th>
                            <th rowspan="2"  style="width: 50px">Po</th>
                            <th rowspan="2"  style="width: 50px">Pm</th>
                            <th rowspan="2"  style="width: 50px">Pk</th>
                            <th rowspan="2"  style="width: 50px">Mix</th>
                            
                            <th rowspan="2"  style="width: 50px">ACT</th>
                              <th rowspan="2"  style="width: 50px">Non ACT</th>
                                <th rowspan="2"  style="width: 50px">Primaquin 14 hari</th>
                                
                                
                            <th rowspan="2"  style="width: 50px">Kasus di PE</th>
                             <th colspan="4"  style="width: 200px">Klasifikasi Asal Penularan</th>
                             
                              <th rowspan="2"  style="width: 50px">% ACT</th>
                               <th rowspan="2"  style="width: 50px">% Primaquin 14 hari</th>
                                <th rowspan="2"  style="width: 50px">% Kasus Di PE</th>
                        </tr>
                        
                        <tr>
                            <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                             <th style="width: 25px">
                                L
                            </th>
                            <th style="width: 25px">
                                P
                            </th>
                            <th style="width: 25px">
                                Total
                            </th>
                            
                            <th style="width: 50px">
                                Indigenus
                            </th>
                              <th style="width: 50px">
                                impor
                            </th>
                                <th style="width: 50px">
                                relaps
                            </th>
                                  <th style="width: 50px">
                                Induced
                            </th>
                        </tr>
                       
                    </thead>
                    <tbody>
                             <?
                             






$strView = "SELECT ".$select." FROM ".$dataview." ".$right." ".$where;


 if($p->periode=='2'){
            $strView.=" && bulan='$p->bulan1'";
      }
if($p->level=='2'){
      //$strView.="WHERE a.idpropinsi='$p->propinsi'";
     
}elseif($p->level=='3'){
     // $strView.=" && idkabupaten='$p->kabupaten'";
}elseif($p->level=='4'){
    //  $strView .=" && kdfaskes in('".$fks."')";
}

                    if($p->level!=4){
                        $total = mysqli_query($dbconn,$totalSQL);
                       $agregatView = mysqli_fetch_object($total);
                       
                        ?>
                        
                         <tr style="background: #009900">
                            <td></td>
                            <td><?php echo strtoupper($agregatView->area);?></td>
                             <td><?php echo $agregatView->jmlpddk;?></td>
                         <td><?php echo $agregatView->mikroskopis;?></td>
                          <td><?php echo $agregatView->rdt;?></td>
                           <td><?php echo $agregatView->lainnya;?></td>
                            <td><?php echo $agregatView->total;?></td>
                            
                            
                            <td><?php echo $agregatView->l1;?></td>
                            <td><?php echo $agregatView->p1;?></td>
                            
                            <td><?php echo $agregatView->l2;?></td>
                            <td><?php echo $agregatView->p2;?></td>
                            <td><?php echo $agregatView->l3;?></td>
                            <td><?php echo $agregatView->p3;?></td>
                            <td><?php echo $agregatView->l4;?></td>
                            <td><?php echo $agregatView->p4;?></td>
                            <td><?php echo $agregatView->l5;?></td>
                            <td><?php echo $agregatView->p5;?></td>
                            <td><?php echo $agregatView->l6;?></td>
                            <td><?php echo $agregatView->p6;?></td>
                            <td><?php echo $agregatView->ltotal;?></td>
                            <td><?php echo $agregatView->ptotal;?></td>
                            <td><?php echo $agregatView->lptotal;?></td>
                            <td><?php echo $agregatView->kematian;?></td>
                            <td><?php echo $agregatView->bumil;?></td>
                            <td><?php echo $agregatView->pf;?></td>
                            <td><?php echo $agregatView->pv;?></td>
                            <td><?php echo $agregatView->po;?></td>
                            <td><?php echo $agregatView->pm;?></td>
                            <td><?php echo $agregatView->pk;?></td>
                            <td><?php echo $agregatView->mix;?></td>
                            <td><?php echo $agregatView->peng_act;?></td>
                            <td><?php echo $agregatView->peng_nonact;?></td>
                            <td><?php echo $agregatView->peng_primaquin;?></td>
                            <td><?php echo $agregatView->kasus_pe;?></td>
                            <td><?php echo $agregatView->indigenus;?></td>
                            <td><?php echo $agregatView->impor;?></td>
                            <td><?php echo $agregatView->relap;?></td>
                            <td><?php echo $agregatView->induced;?></td>
                            <td><?php echo $agregatView->indik_act;?></td>
                            <td><?php echo $agregatView->indik_primaq;?></td>
                            
                            <td><?php echo $agregatView->indik_kasus_pe;?></td>
                            <td><?php echo $agregatView->api;?></td>
                            
                        </tr>
                        
                        <?php
                        
                     }
                    
$queView = mysqli_query($dbconn,$strView);
                        $i=1;
                        while($rowView=mysqli_fetch_object($queView)){
                            
                            
                            ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $rowView->area;?></td>
                             <td><?php echo $rowView->jmlpddk;?></td>
                         <td><?php echo $rowView->mikroskopis;?></td>
                          <td><?php echo $rowView->rdt;?></td>
                           <td><?php echo $rowView->lainnya;?></td>
                            <td><?php echo $rowView->total;?></td>
                            
                            
                            <td><?php echo $rowView->l1;?></td>
                            <td><?php echo $rowView->p1;?></td>
                            
                            <td><?php echo $rowView->l2;?></td>
                            <td><?php echo $rowView->p2;?></td>
                            <td><?php echo $rowView->l3;?></td>
                            <td><?php echo $rowView->p3;?></td>
                            <td><?php echo $rowView->l4;?></td>
                            <td><?php echo $rowView->p4;?></td>
                            <td><?php echo $rowView->l5;?></td>
                            <td><?php echo $rowView->p5;?></td>
                            <td><?php echo $rowView->l6;?></td>
                            <td><?php echo $rowView->p6;?></td>
                            <td><?php echo $rowView->ltotal;?></td>
                            <td><?php echo $rowView->ptotal;?></td>
                            <td><?php echo $rowView->lptotal;?></td>
                            <td><?php echo $rowView->kematian;?></td>
                            <td><?php echo $rowView->bumil;?></td>
                            <td><?php echo $rowView->pf;?></td>
                            <td><?php echo $rowView->pv;?></td>
                            <td><?php echo $rowView->po;?></td>
                            <td><?php echo $rowView->pm;?></td>
                            <td><?php echo $rowView->pk;?></td>
                            <td><?php echo $rowView->mix;?></td>
                            <td><?php echo $rowView->peng_act;?></td>
                            <td><?php echo $rowView->peng_nonact;?></td>
                            <td><?php echo $rowView->peng_primaquin;?></td>
                            <td><?php echo $rowView->kasus_pe;?></td>
                            <td><?php echo $rowView->indigenus;?></td>
                            <td><?php echo $rowView->impor;?></td>
                            <td><?php echo $rowView->relap;?></td>
                            <td><?php echo $rowView->induced;?></td>
                            <td><?php echo $rowView->indik_act;?></td>
                            <td><?php echo $rowView->indik_primaq;?></td>
                            
                            <td><?php echo $rowView->indik_kasus_pe;?></td>
                            <td><?php echo $rowView->api;?></td>
                            
                        </tr>
                        <?php $i++;
                        } ?>
                     
                    
                     
                            
                    </tbody>
                    
                </table>
                
                
            </th>
        </tr>
    </thead>
</table>
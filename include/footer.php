<!-- dadang, dadang, akhir lap regmal1, logistik -->
<script src="assets/external/jquery/jquery.min.js"></script>
<script src="media/libs/jquery.multiple.select.js"></script>
	<link rel="stylesheet" type="text/css" href="media/libs/multiple-select.css"/> 
<!-- dadang, akhir lap regmal1, logistik -->
 
<script src="assets/external/jquery/jquery.validate.js"></script>
<script src="assets/external/Charts/fusioncharts.js"></script>
<script src="assets/external/Charts/fusioncharts-jquery-plugin.js"></script>
	 <script src="assets/internal/js/validation_form.js"></script>
	 <script src="assets/internal/js/hp_validasi_laporan.js"></script>
	 <script src="assets/internal/js/hp_laporan.js"></script> <!-- dadang, ini untuk memanggil filter laporan -->
<script>
    $('document').ready(function(){
		//filterValidation('#filterForm');
		$('#kabupaten').change(function(){
			getComboKec('kecamatan',$(this).val());
		});
		$('#kecamatan').change(function(){
			getComboKel('kelurahan',$(this).val());
		});
		userFormValidation('#userForm');
		$('#sidebar').load('include/menu.php');
		 $('#btnsubmit').click(function(){
                login();
            })
        /* $('#repRegmal1').click(function(){
            
            if ($('#blokmenu').is(':visible')) {
                //code
                $('#blokmenu').slideUp('slow');
                $('#blokfilter').slideDown('slow');
                //alert('nongol');
            }else{
                $('#blokfilter').slideUp('slow');
            }
        }) */
       // $('#page-wrapper').load('dashboard.php');
       $('#g_wilayah').insertFusionCharts({
        type: "column2d",
			width:"400",
			height:"300",
			dataFormat:'jsonurl',
			dataSource:"json_chart.php?chart=prop"
        }); 
        
        
        $('#btnfilter').click(function(){
            $('#blokdata').load('data/datatable_regmal1.php',{
                'tahun':$('#tahun').val(),
                'bulan':$('#bulan').val()
            });
        });
        
        $('#datapropinsi').load('data/datatable_propinsi.php');
		 $('#datakabupaten').load('data/datatable_kabupaten.php');
		 $('#datafasyankes').load('data/datatable_fasyankes.php');
		 
		 //dadang-logistik
		 $('#datalogistik').load('data/datatable_logistik.php');
		 
		 $('#datamikrokopiskab').load('data/dd_datatable_mikroskopis.php');
		 
        /*$('#btnAddProp').click(function(){
            $('#inputForm').slideDown('slow');
            });
        */
       
        });
</script>
	
    <!-- Bootstrap Core JavaScript -->
    <script src="assets/external/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/external/bootstrap/js/bootstrap-multiselect.js"></script>	
	<link rel="stylesheet" type="text/css" href="assets/external/bootstrap/css/bootstrap.min.css"/> 
	<link rel="stylesheet" type="text/css" href="assets/external/bootstrap/css/bootstrap-multiselect.css"/> 
	

    <!-- Metis Menu Plugin JavaScript -->
    <script src="assets/external/metisMenu/metisMenu.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="assets/internal/js/sb-admin-2.js"></script>
     <script src="assets/internal/js/submit-wilayah.js"></script>
	 <script src="assets/internal/js/validation_form.js"></script>
     <script src="assets/internal/js/validation_form_logistik.js"></script> 
	 <script src="assets/internal/js/dd_validation_form_mikrokopis_kab.js"></script>
     
	 <script src="assets/internal/js/pengaturan.js"></script>
	 <script src="assets/internal/js/pengaturan_menu.js"></script>
	  <script src="assets/internal/js/pengaturan_user.js"></script>
	  <!-- <script src="assets/internal/js/dd_pengaturan_mikros.js"></script> -->
	  <script src="assets/internal/js/unit_administrasi.js"></script>
	   <script src="assets/internal/js/pengaturan_group.js"></script>
 <script src="assets/internal/js/pengaturan_akses.js"></script>
 
 
 <script src="assets/internal/js/unit_administrasi_report.js"></script>
 <script src="assets/internal/js/unit_logistik.js"></script>
 <!-- <script src="assets/internal/js/dd_unit_mikroskopis_kab.js"></script> -->
 
 <!-- dadang, logistik prov -->
<!--	<script type="text/javascript" src="logistikpusat/js/jquery-1.8.0.min.js"></script>    
	<script type="text/javascript" src="logistikpusat/js/jquery.easyui.min.js"></script> 
     <link rel="stylesheet" type="text/css" href="logistikpusat/css/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="logistikpusat/css/icon.css"> -->
	<!-- dadang, akhir logistik prov -->
 <!-- <script src="media/libs/jquery.min.js"></script> -->

	